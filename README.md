## Safrano
Safrano is an OData server library based on Ruby, Rack and Sequel.

## Installation

Safrano is available as a [Rubygem](https://rubygems.org/gems/safrano)

```bash
gem install safrano
```

## Usage

To create an OData endpoint follow these steps:

1. [Setup  Models and their associations](docs/01_Sequel_Model.md#safrano-documentation) by using the Sequel functionality.
2. [Publish models and their associations](docs/02_publish.md#safrano-documentation) as an OData app and [Publish media entity models and associations](docs/02_publish_media.md#safrano-documentation)
3. [Configure OData Rack app](docs/03_rack_config.md#safrano-documentation) (port, additional middlewares, logging etc) 

Only the second step is Safrano/OData specific.

After that you are ready to start your server app 
with `rackup config.ru`

### Supported OData versions and features

OData v1 and v2 are provided with the following functionality

* format: only json, no xml/atom yet
* reading data (entities and collections) with
  + $expand, $select, $count, $value
  + $links
  + $filter
      - logical filter expressions with functions
          - length
          - trim
          - tolower / toupper
          - concat
      - substringof
      - startswith / endswith
      - math and arithmetic functions add,sub,mul,div,mod,round,floor,ceiling
      - datetime functions year, month, day, hour, minute, second 
  + $orderby
  + $top and $skip
  + $inlinecount
* all CRUD operations
* fully automated  support for navigation properties based on model associations, 
themselves based on database relationships 
* $batch requests supporting
  + changesets
  + content-ID referencing in changesets
* media entities and media ressources: upload/update, download, delete, associate

## Supported database backends
Any database supported by Sequel should work in theory. Currently the testsuite (200+ testcases)
runs agains Sqlite and Postgresql test-databases.

## Demo client application

* [Fiveapples](https://gitlab.com/dm0da/fiveapples) is a fully opensource and standalone Safrano based OData + openui5 demo app


