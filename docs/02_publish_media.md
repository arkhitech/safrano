## Safrano Documentation

<  | [up](02_publish.md#safrano-documentation) | [next](03_rack_config.md#safrano-documentation) >

### 2. Publishing media entity models and associations

#### Media entities and ressources in Safrano

Media entities are specialised entities that allow to link normal entities to
media ressource files (photos, audio, video files).
The media entities only hold limited metadata information about the media ressource.
The media entities are stored like any other entities in database tables.
The media ressource themselves are stored outside of the database.
To handle the connection between a media entity record in the database and
the real media ressource, Safrano is using configurable Media Handler classes
 (see below). 
The slug/filename provided in media ressource uploads can be stored in the media entity table. The real filename used on the server is defined in the media handler implementations. In Safrano we decided to not export this
real file names eg. in the `media_src` metadata of media entities, but we just
use a generic implicit `media_src`. 


Once you have published a media model and associations, Safrano will handle
all supported operations -- upload, update, download, delete -- 
fully automatically.
Furthermore, when creating and uploading media entities and ressources by
using navigation properties URL, Safrano automatically creates the link to the 
parent entity (assuming your datamodel supports it)

#### Requirements for the media entity table 

Media entities store metadata information about the media ressource. Currently
there is one mandatory field for the media entity table: `content_type` to store 
the media ressource content type.
Optionally you can have a field to store a media ressource filename (SLUG). You can
use the `slug` statement for this purpose (see below).

#### Publishing media entity models and associations

`publish_media_model` is a specialised `publish_model` for media entity sets.
  Within the `publish_media_model` block you can use
* `slug` to specify the field that will be mapped to the Slug when media files 
   are uploaded 
* `use <media_class>, :root => <media_root>` to specify the media file handler 
     that should be used for this media entity.
  + <media_class> should be one of the two provided media class handlers:
    - OData::Media::Static for a simple flat directory handler
    - OData::Media::StaticTree for a tree directory handler, where media files
      are distributed automaticall in a directory tree structure
  + <media_root> is the root directory for the media handler.

#### Reusability of media entities and media handlers

* A media entity eg. Photo can be associated to multiple parent entities.
  For example in the fiveapple demo app, we have 
  + Breeder with a many_to_one `avatar` association, and
  + Cultivar with a one_to_many `photos` association, both `avatar` and `photos`
  share the same Photo model class. On database level this means we have multiple 
  FK relations from/to the same photo table.
* You can have multiple media handlers in use that share the same media root
  directory. This is possible because the media ressource will be stored in a media-entity specific subdirectory. Assuming '/Server/medias' is the 
  shared media root, then <media_entity> ressources will be stored 
  in sub-directory '/Server/medias/<media_entity>'

#### Example

A typical use case would look like that:

 ```ruby
### Sequel Models (Step 1)

class Photo < Sequel::Model(:photos_table)
end
class Audio < Sequel::Model(:audios_table)
end
class Track < Sequel::Model(:tracks_table)
  one_to_one :audio, class: Audio
end

class Album < Sequel::Model(:albums_table)
  one_to_many :tracks, class: :TrackModel, key: ...
  one_to_many :album_photos, class: :Photo, key: ...
end

### OData server (Step 2) 
class YourODataApp < OData::ServerApp
  publish_service do

    # store photos in flat directory /server/medias/Photo
    publish_media_model Photo do 
      slug :filename 
      use OData::Media::Static, :root => '/server/medias'
    end
    
    # store audio files in deep tree directory structure
    # under  /server/medias/Audio
    publish_media_model Audio do 
      slug :filename 
      use OData::Media::StaticTree, :root => '/server/medias'
    end
    
    # Publish Track model with association to the audio file entity Audio
    publish_model Track do
      add_nav_prop_single :audio 
    end

    # publish Album model with association to tracks (normal entities) and
    # Album photos (media entities) 
    
    publish_model Album do
    
      add_nav_prop_collection :tracks
      add_nav_prop_collection :photos 

    end

  end
end
```