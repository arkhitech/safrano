## Safrano Documentation

< [previous](02_publish.md#safrano-documentation) | [Up](README.md) | >

### 3. Configure your OData Rack app 

Safrano is a [Rack](https://github.com/rack/rack) application, and as such you can benefit from the already existing Rack middlewares and plugins.

Safrano does **not** come out of the box with auto-magical security protections.

**Please consider using Rack::Cors and/or Rack::Protection for security reasons.**

### Safrano app configuration

Rack apps are usually configured with an Rack::Builder block.

For configuring your Safrano app, please use Rack::OData::Builder instead of Rack::Builder. Rack::OData::Builder is a simple wrapper that pulls automatically
a minimal set of mandatory middlewares .


### Safrano specific Rack middlewares

Currently there is only one: `Rack::ODataCommonLogger` is like 
Rack::CommonLogger but additionally logs sub-requests of $batch requests.
Example output:

```
- - - [26/Apr/2020:10:59:20 +0200] "GET /cultivar/$count " 200 2 0.0086
- - - [26/Apr/2020:10:59:20 +0200] "GET /cultivar?$skip=0&$top=120&$expand=breeder " 200 10794 0.0793
127.0.0.1 - - [26/Apr/2020:10:59:20 +0200] "POST /odata/$batch HTTP/1.1" 202 11330 0.1509
```

The two first lines are the sub-requests of a $batch request which is logged
in the third line.

### Example configuration

```ruby
###########
# config.ru
#

# model.rb contains the Sequel models and OData app class definition 
require './model.rb'


final_odata_app = Rack::OData::Builder.new do


# CORS handling with rack-cors
#  use Rack::Cors do
#    allow do
#      Please put something meaningfull here after having read the CORS
#      and security related informations
#    end
#  end

# use Rack::CommonLogger
# or
# use Rack::ODataCommonLogger

# provide additional error informations
#  use Rack::ShowExceptions

# and finaly you want to run your app
  run YourODataApp.new

end

# run with WEBrick at port 9595
Rack::Handler::WEBrick.run final_odata_app, Port: 9595

# or
# run with Thin at port 9494
# Thin::Server.start('0.0.0.0', 9494, final_odata_app)
```