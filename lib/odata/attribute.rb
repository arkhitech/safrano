require 'json'
require_relative '../safrano/core.rb'
require_relative './entity.rb'

module OData
  # Represents a named and valued attribute of an Entity
  class Attribute
    attr_reader :name
    attr_reader :entity
    def initialize(entity, name)
      @entity = entity
      @name = name
    end

    def value
      # WARNING ... this require more work to handle the timezones topci
      # currently it is just set to make some minimal testcase work
      case (v = @entity.values[@name.to_sym])
      when Time
        # try to get back the database time zone and value
        #        (v + v.gmt_offset).utc.to_datetime
        v.iso8601

      else
        v
      end
    end

    def odata_get(req)
      if req.walker.raw_value
        [200, CT_TEXT, value.to_s]
      elsif req.accept?(APPJSON)
        [200, CT_JSON, to_odata_json(service: req.service)]
      else # TODO: other formats
        406
      end
    end

    # output as OData json (v2)
    def to_odata_json(*)
      { 'd' => { @name => value } }.to_json
    end

    # for testing purpose (assert_equal ...)
    def ==(other)
      (@entity == other.entity) && (@name == other.name)
    end

    # methods related to transitions to next state (cf. walker)
    module Transitions
      def transition_end(_match_result)
        [nil, :end]
      end

      def transition_value(_match_result)
        [self, :end_with_value]
      end

      ALLOWED_TRANSITIONS = [Safrano::TransitionEnd,
                             Safrano::TransitionValue].freeze

      def allowed_transitions
        ALLOWED_TRANSITIONS
      end
    end
    include Transitions
  end
end
