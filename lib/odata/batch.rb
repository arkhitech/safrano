require_relative '../safrano/rack_app.rb'
require_relative '../safrano/core.rb'
require 'rack/body_proxy'
require_relative './common_logger.rb'

module OData
  # Support for OData multipart $batch Requests
  class Request
    def create_batch_app
      Batch::MyOApp.new(self)
    end

    def parse_multipart
      @mimep = MIME::Media::Parser.new
      @boundary = media_type_params['boundary']
      @mimep.hook_multipart(media_type, @boundary)
      @mimep.parse_str(body)
    end
  end

  module Batch
    # Mayonaise
    class MyOApp < OData::ServerApp
      attr_reader :full_req
      attr_reader :response
      attr_reader :db

      def initialize(full_req)
        @full_req = full_req
        @db = full_req.service.collections.first.db
      end

      # redefined for $batch
      def before
        headers 'Cache-Control' => 'no-cache'
        @request.service = @full_req.service
        headers 'DataServiceVersion' => @request.service.data_service_version
      end

      def batch_call(part_req)
        env = batch_env(part_req)
        env['HTTP_HOST'] = @full_req.env['HTTP_HOST']
        began_at = Rack::Utils.clock_time
        @request = OData::Request.new(env)
        @response = OData::Response.new

        if part_req.level == 2
          @request.in_changeset = true
          @request.content_id = part_req.content_id
          @request.content_id_references = part_req.content_id_references
        end

        before
        dispatch

        status, header, body = @response.finish
        # Logging of sub-requests with ODataCommonLogger.
        # A bit hacky but working
        # TODO: test ?
        if (logga = @full_req.env['safrano.logger_mw'])
          logga.batch_log(env, status, header, began_at)
          # TODO: check why/if we need Rack::Utils::HeaderHash.new(header)
          # and Rack::BodyProxy.new(body) ?
        end
        [status, header, body]
      end

      # shamelessely copied from Rack::TEST:Session
      def headers_for_env(headers)
        converted_headers = {}

        headers.each do |name, value|
          env_key = name.upcase.tr('-', '_')
          env_key = "HTTP_#{env_key}" unless env_key == 'CONTENT_TYPE'
          converted_headers[env_key] = value
        end

        converted_headers
      end

      def batch_env(mime_req)
        @env = ::Rack::MockRequest.env_for(mime_req.uri,
                                           method: mime_req.http_method,
                                           input: mime_req.content)
        # Logging of sub-requests
        @env[Rack::RACK_ERRORS] = @full_req.env[Rack::RACK_ERRORS]
        @env.merge! headers_for_env(mime_req.hd)

        @env
      end
    end

    # Huile d'olive extra
    class HandlerBase
      TREND = Safrano::Transition.new('', trans: 'transition_end')
      def allowed_transitions
        @allowed_transitions = [TREND]
      end

      def transition_end(_match_result)
        [nil, :end]
      end
    end
    # jaune d'oeuf
    class DisabledHandler < HandlerBase
      def odata_post(_req)
        [404, EMPTY_HASH, '$batch is not enabled ']
      end

      def odata_get(_req)
        [404, EMPTY_HASH, '$batch is not enabled ']
      end
    end
    # battre le tout
    class EnabledHandler < HandlerBase
      attr_accessor :boundary
      attr_accessor :mmboundary
      attr_accessor :body_str
      attr_accessor :parts
      attr_accessor :request

      def initialize; end

      # here we are in the Batch handler object, and this POST should
      # normally handle a $batch request
      def odata_post(req)
        @request = req

        if @request.media_type == OData::MP_MIXED

          batcha = @request.create_batch_app
          @mult_request = @request.parse_multipart

          @mult_request.prepare_content_id_refs
          @mult_response = OData::Response.new

          resp_hdrs, @mult_response.body = @mult_request.get_http_resp(batcha)

          [202, resp_hdrs, @mult_response.body[0]]
        else
          [415, EMPTY_HASH, 'Unsupported Media Type']
        end
      end

      def odata_get(_req)
        [405, EMPTY_HASH, 'You cant GET $batch, POST it ']
      end
    end
  end
end
