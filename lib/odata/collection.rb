# Design: Collections are nothing more as Sequel based model classes that have
# somehow the character of an array (Enumerable)
# Thus Below we have called that "EntityClass". It's meant as "Collection"

require 'json'
require 'rexml/document'
require_relative '../safrano/core.rb'
require_relative 'error.rb'
require_relative 'collection_filter.rb'
require_relative 'collection_order.rb'
require_relative 'expand.rb'
require_relative 'select.rb'
require_relative 'url_parameters.rb'
require_relative 'collection_media.rb'

# small helper method
# http://stackoverflow.com/
#        questions/24980295/strictly-convert-string-to-integer-or-nil
def number_or_nil(str)
  num = str.to_i
  num if num.to_s == str
end

# another helper
# thanks https://stackoverflow.com/questions/1448670/ruby-stringto-class
class String
  def constantize
    names = split('::')
    names.shift if names.empty? || names.first.empty?

    const = Object
    names.each do |name|
      const = if const.const_defined?(name)
                const.const_get(name)
              else
                const.const_missing(name)
              end
    end
    const
  end
end

module OData
  # class methods. They Make heavy use of Sequel::Model functionality
  # we will add this to our Model classes with "extend" --> self is the Class
  module EntityClassBase
    SINGLE_PK_URL_REGEXP = /\A\(\s*'?([\w\s]+)'?\s*\)(.*)/.freeze
    ONLY_INTEGER_RGX = /\A[+-]?\d+\z/.freeze

    attr_reader :nav_collection_url_regexp
    attr_reader :nav_entity_url_regexp
    attr_reader :entity_id_url_regexp
    attr_reader :nav_collection_attribs
    attr_reader :nav_entity_attribs
    attr_reader :data_fields
    attr_reader :inlinecount
    attr_reader :default_template
    attr_reader :uri
    attr_reader :time_cols

    # Sequel associations pointing to this model. Sequel provides association
    # reflection information on the "from" side. But in some cases
    # we will need the reverted way
    # finally not needed and not used yet
    # attr_accessor :assocs_to

    # set to parent entity in case the collection is a nav.collection
    # nil otherwise
    attr_reader :nav_parent

    attr_accessor :namespace

    # dataset
    attr_accessor :cx

    # url params
    attr_reader :params

    # url parameters processing object (mostly covert to sequel exprs).
    # exposed for testing only
    attr_reader :uparms

    # initialising block of code to be executed at end of
    # ServerApp.publish_service after all model classes have been registered
    # (without the associations/relationships)
    # typically the block should contain the publication of the associations
    attr_accessor :deferred_iblock

    # convention: entityType is the Ruby Model class --> name is just to_s
    # Warning: for handling Navigation relations, we use anonymous collection classes
    # dynamically subtyped from a Model class, and in such an anonymous class
    # the class-name is not the OData Type. In these subclass we redefine "type_name"
    # thus when we need the Odata type name, we shall use this method instead of just the collection class name
    def type_name
      @type_name
    end

    def build_type_name
      @type_name = to_s
    end

    # convention: default for entity_set_name is the type name
    def entity_set_name
      @entity_set_name = (@entity_set_name || type_name)
    end

    def reset
      # TODO: automatically reset all attributes?
      @deferred_iblock = nil
      @entity_set_name = nil
      @uri = nil
      @uparms = nil
      @params = nil
      @cx = nil
      @@time_cols = nil
    end

    def build_uri(uribase)
      @uri = "#{uribase}/#{entity_set_name}"
    end

    def execute_deferred_iblock
      instance_eval { @deferred_iblock.call } if @deferred_iblock
    end

    # Factory json-> Model Object instance
    def new_from_hson_h(hash)
      enty = new
      enty.set_fields(hash, @data_fields, missing: :skip)
      enty
    end

    CREATE_AND_SAVE_ENTY_AND_REL = lambda do |new_entity, assoc, parent|
      # in-changeset requests get their own transaction
      case assoc[:type]
      when :one_to_many, :one_to_one
        OData.create_nav_relation(new_entity, assoc, parent)
        new_entity.save(transaction: false)
      when :many_to_one
        new_entity.save(transaction: false)
        OData.create_nav_relation(new_entity, assoc, parent)
        parent.save(transaction: false)
        # else # not supported
      end
    end
    def odata_create_save_entity_and_rel(req, new_entity, assoc, parent)
      if req.in_changeset
        # in-changeset requests get their own transaction
        CREATE_AND_SAVE_ENTY_AND_REL.call(new_entity, assoc, parent)
      else
        db.transaction do
          CREATE_AND_SAVE_ENTY_AND_REL.call(new_entity, assoc, parent)
        end
      end
    end

    def odata_get_inlinecount_w_sequel
      return unless (icp = @params['$inlinecount'])

      @inlinecount = if icp == 'allpages'
                       if is_a? Sequel::Model::ClassMethods
                         @cx.count
                       else
                         @cx.dataset.count
                       end
                     end
    end

    def attrib_path_valid?(path)
      @attribute_path_list.include? path
    end

    def odata_get_apply_params
      @cx = @uparms.apply_to_dataset(@cx)
      odata_get_inlinecount_w_sequel

      @cx = @cx.offset(@params['$skip']) if @params['$skip']
      @cx = @cx.limit(@params['$top']) if @params['$top']
      @cx
    end

    # url params validation methods.
    # nil is the expected return for no errors
    # an error class is returned in case of errors
    # this way we can combine multiple validation calls with logical ||
    def check_u_p_top
      return unless @params['$top']

      itop = number_or_nil(@params['$top'])
      return BadRequestError if itop.nil? || itop.zero?
    end

    def check_u_p_skip
      return unless @params['$skip']

      iskip = number_or_nil(@params['$skip'])
      return BadRequestError if iskip.nil? || iskip.negative?
    end

    def check_u_p_inlinecount
      return unless (icp = @params['$inlinecount'])

      return BadRequestInlineCountParamError unless (icp == 'allpages') || (icp == 'none')

      nil
    end

    def check_u_p_filter
      @uparms.check_filter
    end

    def check_u_p_orderby
      @uparms.check_order
    end

    def check_u_p_expand
      @uparms.check_expand
    end

    def build_attribute_path_list
      @attribute_path_list = attribute_path_list
    end

    MAX_DEPTH = 6
    def attribute_path_list(depth = 0)
      ret = @columns.map(&:to_s)
      # break circles
      return ret if depth > MAX_DEPTH

      depth += 1

      @nav_entity_attribs&.each do |a, k|
        ret.concat(k.attribute_path_list(depth).map { |kc| "#{a}/#{kc}" })
      end
      @nav_collection_attribs&.each do |a, k|
        ret.concat(k.attribute_path_list(depth).map { |kc| "#{a}/#{kc}" })
      end
      ret
    end

    def check_url_params
      return nil unless @params

      check_u_p_top || check_u_p_skip || check_u_p_orderby ||
        check_u_p_filter || check_u_p_expand || check_u_p_inlinecount
    end

    def initialize_dataset
      @cx = self
      @cx = navigated_dataset if @cx.nav_parent
      @model = if @cx.respond_to? :model
                 @cx.model
               else
                 @cx
               end
      @uparms = UrlParameters4Coll.new(@model, @params)
    end

    # finally return the requested output according to format, options etc
    def odata_get_output(req)
      return @error.odata_get(req) if @error

      if req.walker.do_count
        [200, CT_TEXT, @cx.count.to_s]
      elsif req.accept?(APPJSON)
        if req.walker.do_links
          [200, CT_JSON, [to_odata_links_json(service: req.service)]]
        else
          [200, CT_JSON, [to_odata_json(service: req.service)]]
        end
      else # TODO: other formats
        406
      end
    end

    # validation/error handling methods.
    # normal processing is done in the passed proc

    def with_validated_get(req)
      begin
        initialize_dataset
        return yield unless (@error = check_url_params)
      rescue OData::Filter::Parser::ErrorWrongColumnName
        @error = BadRequestFilterParseError
      rescue OData::Filter::Parser::ErrorFunctionArgumentType
        @error = BadRequestFilterParseError
      rescue OData::Filter::FunctionNotImplemented => e
        @error = e.odata_error
      rescue OData::Filter::Parser::ErrorInvalidFunction => e
        @error = e.odata_error
      end

      @error.odata_get(req) if @error
    end

    # on model class level we return the collection
    def odata_get(req)
      @params = req.params

      with_validated_get(req) do
        odata_get_apply_params
        odata_get_output(req)
      end
    end

    def odata_post(req)
      odata_create_entity_and_relation(req, @navattr_reflection, @nav_parent)
    end

    # add metadata xml to the passed REXML schema object
    def add_metadata_rexml(schema)
      enty = if @media_handler
               schema.add_element('EntityType', 'Name' => to_s, 'HasStream' => 'true')
             else
               schema.add_element('EntityType', 'Name' => to_s)
             end
      # with their properties
      db_schema.each do |pnam, prop|
        if prop[:primary_key] == true
          enty.add_element('Key').add_element('PropertyRef',
                                              'Name' => pnam.to_s)
        end
        # puts "Name: #{pnam}: db_type: #{prop[:db_type]} type: #{OData.get_edm_type(db_type: prop[:db_type])}" unless OData.get_edm_type(db_type: prop[:db_type])
        attrs = { 'Name' => pnam.to_s,
                  'Type' => OData.get_edm_type(db_type: prop[:db_type]) }
        attrs['Nullable'] = 'false' if prop[:allow_null] == false
        enty.add_element('Property', attrs)
      end
      enty
    end

    # metadata REXML data  for a single Nav attribute
    def metadata_nav_rexml_attribs(assoc, to_klass, relman, xnamespace)
      from = type_name
      to = to_klass.type_name
      relman.get_metadata_xml_attribs(from,
                                      to,
                                      association_reflection(assoc.to_sym)[:type],
                                      xnamespace,
                                      assoc)
    end

    # and their Nav attributes == Sequel Model association
    def add_metadata_navs_rexml(schema_enty, relman, xnamespace)
      @nav_entity_attribs&.each do |ne, klass|
        nattr = metadata_nav_rexml_attribs(ne,
                                           klass,
                                           relman,
                                           xnamespace)
        schema_enty.add_element('NavigationProperty', nattr)
      end

      @nav_collection_attribs&.each do |nc, klass|
        nattr = metadata_nav_rexml_attribs(nc,
                                           klass,
                                           relman,
                                           xnamespace)
        schema_enty.add_element('NavigationProperty', nattr)
      end
    end

    D = 'd'.freeze
    DJ_OPEN = '{"d":'.freeze
    DJ_CLOSE = '}'.freeze
    def to_odata_links_json(service:)
      innerj = service.get_coll_odata_links_h(array: @cx.all,
                                              icount: @inlinecount).to_json
      "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
    end

    #    def output_template(expand: nil, select: nil)
    def output_template(uparms)
      #      output_template_deep(expand_list: expand_list, select: select)
      output_template_deep(expand_list: uparms.expand.template, select: uparms.select)
    end

    # Recursive
    def output_template_deep(expand_list:, select: OData::SelectBase::ALL)
      return default_template if expand_list.empty? && select.all_props?

      template = {}
      expand_e = {}
      expand_c = {}
      deferr = []

      # 1. handle non-navigation properties, only consider $select
      # 2. handle navigations properties, need to check $select and $expand
      if select.all_props?
        template[:all_values] = EMPTYH
        # include all nav attributes -->
        @nav_entity_attribs&.each do |attr, klass|
          if expand_list.key?(attr)
            expand_e[attr] = klass.output_template_deep(expand_list: expand_list[attr])
          else
            deferr << attr
          end
        end

        @nav_collection_attribs&.each do |attr, klass|
          if expand_list.key?(attr)
            expand_c[attr] = klass.output_template_deep(expand_list: expand_list[attr])
          else
            deferr << attr
          end
        end

      else
        template[:selected_vals] = @columns.map(&:to_s) & select.props
        # include only selected nav attribs-->need additional intersection step
        if @nav_entity_attribs
          selected_nav_e = @nav_entity_attribs.keys & select.props

          selected_nav_e&.each do |attr|
            if expand_list.key?(attr)
              klass = @nav_entity_attribs[attr]
              expand_e[attr] = klass.output_template_deep(expand_list: expand_list[attr])
            else
              deferr << attr
            end
          end
        end
        if @nav_collection_attribs
          selected_nav_c = @nav_collection_attribs.keys & select.props
          selected_nav_c&.each do |attr|
            if expand_list.key?(attr)
              klass = @nav_collection_attribs[attr]
              expand_c[attr] = klass.output_template_deep(expand_list: expand_list[attr])
            else
              deferr << attr
            end
          end
        end
      end
      template[:expand_e] = expand_e if expand_e
      template[:expand_c] = expand_c if expand_c
      template[:deferr] = deferr if deferr
      template
    end

    def to_odata_json(service:)
      template = output_template(@uparms)
      innerj = service.get_coll_odata_h(array: @cx.all,
                                        template: template,
                                        icount: @inlinecount).to_json
      "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
    end

    # this functionally similar to the Sequel Rels (many_to_one etc)
    # We need to base this on the Sequel rels, or extend them
    def add_nav_prop_collection(assoc_symb, attr_name_str = nil)
      @nav_collection_attribs = (@nav_collection_attribs || {})
      # DONE: Error handling. This requires that associations
      # have been properly defined with Sequel before
      assoc = all_association_reflections.find do |a|
        a[:name] == assoc_symb && a[:model] == self
      end

      raise OData::API::ModelAssociationNameError.new(self, assoc_symb) unless assoc

      attr_class = assoc[:class_name].constantize
      lattr_name_str = (attr_name_str || assoc_symb.to_s)
      @nav_collection_attribs[lattr_name_str] = attr_class
      @nav_collection_url_regexp = @nav_collection_attribs.keys.join('|')
    end

    def add_nav_prop_single(assoc_symb, attr_name_str = nil)
      @nav_entity_attribs = (@nav_entity_attribs || {})
      # DONE: Error handling. This requires that associations
      # have been properly defined with Sequel before
      assoc = all_association_reflections.find do |a|
        a[:name] == assoc_symb && a[:model] == self
      end

      raise OData::API::ModelAssociationNameError.new(self, assoc_symb) unless assoc

      attr_class = assoc[:class_name].constantize
      lattr_name_str = (attr_name_str || assoc_symb.to_s)
      @nav_entity_attribs[lattr_name_str] = attr_class
      @nav_entity_url_regexp = @nav_entity_attribs.keys.join('|')
    end

    EMPTYH = {}.freeze

    def build_default_template
      template = { all_values: EMPTYH }
      if @nav_entity_attribs || @nav_collection_attribs
        template[:deferr] = (@nav_entity_attribs&.keys || []) + (@nav_collection_attribs&.keys || EMPTY_ARRAY)
      end
      template
    end
    # old names...
    #    alias_method :add_nav_prop_collection, :addNavCollectionAttrib
    #    alias_method :add_nav_prop_single, :addNavEntityAttrib

    def finalize_publishing
      build_type_name

      # finalize media handler
      @media_handler.register(self) if @media_handler

      # build default output template structure
      @default_template = build_default_template

      # Time columns
      @time_cols = db_schema.select { |c, v| v[:type] == :datetime }.map { |c, v| c }

      # and finally build the path list and allowed tr's
      build_attribute_path_list

      build_allowed_transitions
      build_entity_allowed_transitions
    end

    def prepare_pk
      if primary_key.is_a? Array
        @pk_names = []
        primary_key.each { |pk| @pk_names << pk.to_s }
        # TODO: better handle quotes based on type
        # (stringlike--> quote, int-like --> no quotes)

        iuk = @pk_names.map { |pk| "#{pk}='?(\\w+)'?" }
        @iuk_rgx = /\A#{iuk.join(',\s*')}\z/

        iuk = @pk_names.map { |pk| "#{pk}='?\\w+'?" }
        @entity_id_url_regexp = /\A\(\s*(#{iuk.join(',\s*')})\s*\)(.*)/
      else
        @pk_names = [primary_key.to_s]
        @entity_id_url_regexp = SINGLE_PK_URL_REGEXP
      end
    end

    def prepare_fields
      @data_fields = db_schema.map do |col, cattr|
        cattr[:primary_key] ? nil : col
      end.select { |col| col }
    end

    def invalid_hash_data?(data)
      data.keys.map(&:to_sym).find { |ksym| !(@columns.include? ksym) }
    end

    ## A regexp matching all allowed attributes of the Entity
    ## (eg ID|name|size etc... ) at start position and returning the rest
    def transition_attribute_regexp
      #      db_schema.map { |sch| sch[0] }.join('|')
      # @columns is from Sequel Model
      %r{\A/(#{@columns.join('|')})(.*)\z}
    end

    # pkid can be a single value for single-pk models,  or an array.
    # type checking/convertion is done in check_odata_key_type
    def find_by_odata_key(pkid)
      # amazingly this works as expected from an Entity.get_related(...) anonymous class
      # without need to redefine primary_key_lookup (returns nil for valid but unrelated keys)
      primary_key_lookup(pkid)
    end

    # super-minimal type check, but better as nothing
    def check_odata_val_type(val, type)
      case type
      when :integer
        val =~ ONLY_INTEGER_RGX ? [true, Integer(val)] : [false, val]
      when :string
        [true, val]
      else
        [true, val] # todo...
      end
    rescue StandardError
      [false, val]
    end

    # methods related to transitions to next state (cf. walker)
    module Transitions
      def transition_end(_match_result)
        [nil, :end]
      end

      def transition_count(_match_result)
        [self, :end_with_count]
      end

      def transition_id(match_result)
        if (id = match_result[1])

          ck, casted_id = check_odata_key(id)

          if ck
            if (y = find_by_odata_key(casted_id))
              [y, :run]
            else
              [nil, :error, ErrorNotFound]
            end
          else
            [nil, :error, BadRequestError]
          end
        else
          [nil, :error, ServerTransitionError]
        end
      end

      def allowed_transitions
        @allowed_transitions
      end

      def entity_allowed_transitions
        @entity_allowed_transitions
      end

      def build_allowed_transitions
        @allowed_transitions = [Safrano::TransitionEnd,
                                Safrano::TransitionCount,
                                Safrano::Transition.new(entity_id_url_regexp,
                                                        trans: 'transition_id')].freeze
      end

      def build_entity_allowed_transitions
        @entity_allowed_transitions = [
          Safrano::TransitionEnd,
          Safrano::TransitionCount,
          Safrano::TransitionLinks,
          Safrano::TransitionValue,
          Safrano::Transition.new(transition_attribute_regexp, trans: 'transition_attribute')
        ]
        if (ncurgx = @nav_collection_url_regexp)
          @entity_allowed_transitions <<
            Safrano::Transition.new(%r{\A/(#{ncurgx})(.*)\z}, trans: 'transition_nav_collection')
        end
        if (neurgx = @nav_entity_url_regexp)
          @entity_allowed_transitions <<
            Safrano::Transition.new(%r{\A/(#{neurgx})(.*)\z}, trans: 'transition_nav_entity')
        end
        @entity_allowed_transitions.freeze
        @entity_allowed_transitions
      end
    end
    include Transitions
  end

  # special handling for composite key
  module EntityClassMultiPK
    include EntityClassBase
    # input  fx='aas',fy_w='0001'
    # output true, ['aas', '0001'] ... or false when typ-error
    def check_odata_key(mid)
      # @iuk_rgx is (needs to be) built on start with
      # collklass.prepare_pk
      md = @iuk_rgx.match(mid).to_a
      md.shift # remove first element which is the whole match
      mdc = []
      error = false
      primary_key.each_with_index do |pk, i|
        ck, casted = check_odata_val_type(md[i], db_schema[pk][:type])
        if ck
          mdc << casted
        else
          error = true
          break
        end
      end
      if error
        [false, md]
      else
        [true, mdc]
      end
    end
  end

  # special handling for single key
  module EntityClassSinglePK
    include EntityClassBase

    def check_odata_key(id)
      check_odata_val_type(id, db_schema[primary_key][:type])
    end
  end

  # normal handling for non-media entity
  module EntityClassNonMedia
    # POST for non-media entity collection -->
    # 1. Create and add entity from payload
    # 2. Create relationship if needed
    def odata_create_entity_and_relation(req, assoc, parent)
      # TODO: this is for v2 only...
      req.with_parsed_data do |data|
        data.delete('__metadata')
        # validate payload column names
        if (invalid = invalid_hash_data?(data))
          ::OData::Request::ON_CGST_ERROR.call(req)
          return [422, EMPTY_HASH, ['Invalid attribute name: ', invalid.to_s]]
        end

        if req.accept?(APPJSON)
          new_entity = new_from_hson_h(data)
          if parent
            odata_create_save_entity_and_rel(req, new_entity, assoc, parent)
          else
            # in-changeset requests get their own transaction
            new_entity.save(transaction: !req.in_changeset)
          end
          req.register_content_id_ref(new_entity)
          new_entity.copy_request_infos(req)

          [201, CT_JSON, new_entity.to_odata_post_json(service: req.service)]
        else # TODO: other formats
          415
        end
      end
    end
  end
end
