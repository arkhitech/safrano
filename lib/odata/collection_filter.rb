require 'odata/error.rb'

require_relative 'filter/parse.rb'
require_relative 'filter/sequel.rb'

# a few helper method
class String
  MASK_RGX = /'((?:[^']|(?:\'{2}))*)'/.freeze
  UNMASK_RGX = /'(%?)(\$\d+)(%?)'/.freeze
  def with_mask_quoted_substrings!
    cnt = 0
    repl = {}
    gsub!(MASK_RGX) do |_m|
      cnt += 1
      repl["$#{cnt}"] = Regexp.last_match(1)
      "'$#{cnt}'"
    end
    yield self

    gsub!(UNMASK_RGX) do |_m|
      k = Regexp.last_match(2).to_s
      "'#{Regexp.last_match(1)}#{repl[k]}#{Regexp.last_match(3)}'"
    end
  end

  def with_mask_quoted_substrings
    cnt = 0
    repl = {}
    tmpstr = gsub(MASK_RGX) do |_m|
      cnt += 1
      repl["$#{cnt}"] = Regexp.last_match(1)
      "'$#{cnt}'"
    end
    yield tmpstr
  end
end

# filter base class and subclass in our OData namespace
module OData
  class FilterBase
    # re-useable empty filtering (idempotent)
    EmptyFilter = new

    def self.factory(filterstr)
      filterstr.nil? ? EmptyFilter : FilterByParse.new(filterstr)
    end

    def apply_to_dataset(dtcx)
      dtcx
    end

    # finalize
    def finalize(jh) end

    def empty?
      true
    end

    def parse_error?
      false
    end
  end
  # should handle everything by parsing
  class FilterByParse < FilterBase
    attr_reader :filterstr
    def initialize(filterstr)
      @filterstr = filterstr.dup
      @ast = OData::Filter::Parser.new(@filterstr).build
    end

    # this build's up the Sequel Filter Expression, and as a side effect,
    # it also finalizes the join helper that we need for the start dataset join
    # the join-helper is shared by the order-by object and was potentially already
    # partly built on order-by object creation.
    def finalize(jh)
      @filtexpr = @ast.sequel_expr(jh)
    end

    def apply_to_dataset(dtcx)
      #     normally finalize is called before, and thus @filtexpr is set
      dtcx.where(@filtexpr)
    end

    def parse_error?
      @ast.is_a? StandardError
    end

    def empty?
      false
    end
  end
end
