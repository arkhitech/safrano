require 'rack'
require 'fileutils'
require_relative './navigation_attribute.rb'

module OData
  module Media
    # base class for Media Handler
    class Handler
    end

    # Simple static File/Directory based media store handler
    # similar to Rack::Static
    # with a flat directory structure
    class Static < Handler
      def initialize(root: nil)
        @root = File.absolute_path(root || Dir.pwd)
        @file_server = ::Rack::File.new(@root)
      end

      # TODO: testcase and better abs_klass_dir design
      def register(klass)
        abs_klass_dir = File.absolute_path(klass.type_name, @root)
        FileUtils.makedirs abs_klass_dir unless Dir.exist?(abs_klass_dir)
      end

      # minimal working implementation...
      #  Note: @file_server works relative to @root directory
      def odata_get(request:, entity:)
        media_env = request.env.dup
        media_env['PATH_INFO'] = filename(entity)
        fsret = @file_server.call(media_env)
        if fsret.first == 200
          # provide own content type as we keep it in the media entity
          fsret[1]['Content-Type'] = entity.content_type
        end
        fsret
      end

      # TODO: [perf] this can be precalculated and cached on MediaModelKlass level
      #            and passed as argument to save_file
      # eg. /@root/Photo
      def abs_klass_dir(entity)
        File.absolute_path(entity.klass_dir, @root)
      end

      # this is relative to @root
      # eg. Photo/1
      def media_path(entity)
        File.join(entity.klass_dir, media_directory(entity))
      end

      # relative to @root
      # eg Photo/1/pommes-topaz.jpg
      def filename(entity)
        Dir.chdir(abs_path(entity)) do
          # simple design: one file per directory, and the directory
          # contains the media entity-id --> implicit link between the media
          # entity
          File.join(media_path(entity), Dir.glob('*').first)
        end
      end

      # /@root/Photo/1
      def abs_path(entity)
        File.absolute_path(media_path(entity), @root)
      end

      # this is relative to abs_klass_dir(entity) eg to /@root/Photo
      # simplest implementation is media_directory = entity.media_path_id
      # --> we get a 1 level depth flat directory structure
      def media_directory(entity)
        entity.media_path_id
      end

      def in_media_directory(entity)
        mpi = media_directory(entity)
        Dir.mkdir mpi unless Dir.exist?(mpi)
        Dir.chdir mpi do
          yield
        end
      end

      def odata_delete(entity:)
        Dir.chdir(abs_klass_dir(entity)) do
          in_media_directory(entity) do
            Dir.glob('*').each { |oldf| File.delete(oldf) }
          end
        end
      end

      # Here as well, MVP implementation
      def save_file(data:, filename:, entity:)
        Dir.chdir(abs_klass_dir(entity)) do
          in_media_directory(entity) do
            filename = '1'
            File.open(filename, 'wb') { |f| IO.copy_stream(data, f) }
          end
        end
      end

      # needed for having a changing media ressource "source" metadata
      # after each upload, so that clients get informed about new versions
      # of the same media ressource
      def ressource_version(entity)
        Dir.chdir(abs_klass_dir(entity)) do
          in_media_directory(entity) do
            Dir.glob('*').last
          end
        end
      end

      # Here as well, MVP implementation
      def replace_file(data:, filename:, entity:)
        Dir.chdir(abs_klass_dir(entity)) do
          in_media_directory(entity) do
            version = nil
            Dir.glob('*').each do |oldf|
              version = oldf
              File.delete(oldf)
            end
            filename = (version.to_i + 1).to_s
            File.open(filename, 'wb') { |f| IO.copy_stream(data, f) }
          end
        end
      end
    end
    # Simple static File/Directory based media store handler
    # similar to Rack::Static
    # with directory Tree structure

    class StaticTree < Static
      SEP = '/00/'.freeze
      VERS = '/v'.freeze

      def self.path_builder(ids)
        ids.map { |id| id.to_s.chars.join('/') }.join(SEP) << VERS
      end

      # this is relative to abs_klass_dir(entity) eg to /@root/Photo
      # tree-structure
      #  media_path_ids = 1  -->  1
      #  media_path_ids = 15  -->  1/5
      #  media_path_ids = 555  -->  5/5/5
      #  media_path_ids = 5,5,5  -->  5/00/5/00/5
      #  media_path_ids = 5,00,5  -->  5/00/0/0/00/5
      #  media_path_ids = 5,xyz,5  -->  5/00/x/y/z/00/5
      def media_directory(entity)
        StaticTree.path_builder(entity.media_path_ids)
        #        entity.media_path_ids.map{|id| id.to_s.chars.join('/')}.join(@sep)
      end

      def in_media_directory(entity)
        mpi = media_directory(entity)
        FileUtils.makedirs mpi unless Dir.exist?(mpi)
        Dir.chdir(mpi) { yield }
      end

      def odata_delete(entity:)
        Dir.chdir(abs_klass_dir(entity)) do
          in_media_directory(entity) do
            Dir.glob('*').each { |oldf| File.delete(oldf) if File.file?(oldf) }
          end
        end
      end

      # Here as well, MVP implementation
      #      def replace_file(data:, filename:, entity:)
      #        Dir.chdir(abs_klass_dir(entity)) do
      #          in_media_directory(entity) do
      #            Dir.glob('*').each { |oldf| File.delete(oldf) if File.file?(oldf) }
      #            File.open(filename, 'wb') { |f| IO.copy_stream(data, f) }
      #          end
      #        end
      #      end
      # Here as well, MVP implementation
      def replace_file(data:, filename:, entity:)
        Dir.chdir(abs_klass_dir(entity)) do
          in_media_directory(entity) do
            version = nil
            Dir.glob('*').each do |oldf|
              version = oldf
              File.delete(oldf)
            end
            filename = (version.to_i + 1).to_s
            File.open(filename, 'wb') { |f| IO.copy_stream(data, f) }
          end
        end
      end
    end
  end

  # special handling for media entity
  module EntityClassMedia
    attr_reader :media_handler
    attr_reader :slug_field

    # API method for defining the media handler
    # eg.
    # publish_media_model photos do
    #   use OData::Media::Static, :root => '/media_root'
    # end

    def set_default_media_handler
      @media_handler = OData::Media::Static.new
    end

    def use(klass, args)
      @media_handler = klass.new(**args)
    end

    # API method for setting the model field mapped to SLUG on upload
    def slug(inp)
      @slug_field = inp
    end

    def api_check_media_fields
      raise(OData::API::MediaModelError, self) unless db_schema.key?(:content_type)

      #      unless self.db_schema.has_key?(:media_src)
      #        raise OData::API::MediaModelError, self
      #      end
    end

    #  END API methods

    def new_media_entity(mimetype:)
      nh = { 'content_type' => mimetype }
      new_from_hson_h(nh)
    end

    # POST for media entity collection --> Create media-entity linked to
    # uploaded file from payload
    # 1. create new media entity
    # 2. get the pk/id from the new media entity
    # 3. Upload the file and use the pk/id to get an unique Directory/filename
    #    assignment to the media entity record
    # NOTE: we will implement this first in a MVP way. There will be plenty of
    #       potential future enhancements (performance, scallability, flexibility... with added complexity)
    # 4. create relation to parent if needed
    def odata_create_entity_and_relation(req, assoc, parent)
      req.with_media_data do |data, mimetype, filename|
        ## future enhancement: validate allowed mimetypes ?
        # if (invalid = invalid_media_mimetype(mimetype))
        #  ::OData::Request::ON_CGST_ERROR.call(req)
        #  return [422, {}, ['Invalid mime type: ', invalid.to_s]]
        # end

        if req.accept?(APPJSON)

          new_entity = new_media_entity(mimetype: mimetype)

          if slug_field

            new_entity.set_fields({ slug_field => filename },
                                  data_fields,
                                  missing: :skip)
          end

          # to_one rels are create with FK data set on the parent entity
          if parent
            odata_create_save_entity_and_rel(req, new_entity, assoc, parent)
          else
            # in-changeset requests get their own transaction
            new_entity.save(transaction: !req.in_changeset)
          end

          req.register_content_id_ref(new_entity)
          new_entity.copy_request_infos(req)

          media_handler.save_file(data: data, entity: new_entity, filename: filename)

          [201, CT_JSON, new_entity.to_odata_post_json(service: req.service)]
        else # TODO: other formats
          415
        end
      end
    end
  end
end
