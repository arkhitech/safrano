module Rack
  class ODataCommonLogger < CommonLogger
    def call(env)
      env['safrano.logger_mw'] = self
      super
    end

    # Handle https://github.com/rack/rack/pull/1526
    # new in Rack 2.2.2 : Format has now 11 placeholders instead of 10

    MSG_FUNC = if FORMAT.count('%') == 10
                 lambda { |env, length, status, began_at|
                   FORMAT % [
                     env['HTTP_X_FORWARDED_FOR'] || env['REMOTE_ADDR'] || '-',
                     env['REMOTE_USER'] || '-',
                     Time.now.strftime('%d/%b/%Y:%H:%M:%S %z'),
                     env[REQUEST_METHOD],
                     env[SCRIPT_NAME] + env[PATH_INFO],
                     env[QUERY_STRING].empty? ? '' : "?#{env[QUERY_STRING]}",
                     env[SERVER_PROTOCOL],
                     status.to_s[0..3],
                     length,
                     Utils.clock_time - began_at
                   ]
                 }
               elsif FORMAT.count('%') == 11
                 lambda { |env, length, status, began_at|
                   FORMAT % [
                     env['HTTP_X_FORWARDED_FOR'] || env['REMOTE_ADDR'] || '-',
                     env['REMOTE_USER'] || '-',
                     Time.now.strftime('%d/%b/%Y:%H:%M:%S %z'),
                     env[REQUEST_METHOD],
                     env[SCRIPT_NAME],
                     env[PATH_INFO],
                     env[QUERY_STRING].empty? ? '' : "?#{env[QUERY_STRING]}",
                     env[SERVER_PROTOCOL],
                     status.to_s[0..3],
                     length,
                     Utils.clock_time - began_at
                   ]
                 }
               end

    def batch_log(env, status, header, began_at)
      length = extract_content_length(header)

      msg = MSG_FUNC.call(env, length, status, began_at)

      logger = @logger || env[RACK_ERRORS]
      # Standard library logger doesn't support write but it supports << which actually
      # calls to write on the log device without formatting
      if logger.respond_to?(:write)
        logger.write(msg)
      else
        logger << msg
      end
    end
  end
end
