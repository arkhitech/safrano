require 'json'
require 'rexml/document'
require 'safrano.rb'
require 'odata/collection.rb' # required for  self.class.entity_type_name ??
require_relative 'navigation_attribute'

module OData
  # this will be mixed in the  Model classes (subclasses of Sequel Model)
  module EntityBase
    attr_reader :params

    include EntityBase::NavigationInfo

    # methods related to transitions to next state (cf. walker)
    module Transitions
      def allowed_transitions
        self.class.entity_allowed_transitions
      end

      def transition_end(_match_result)
        [nil, :end]
      end

      def transition_count(_match_result)
        [self, :end]
      end

      def transition_value(_match_result)
        # $value is only allowd for media entities (or attributes)
        [self, :end_with_media_value]
      end

      def transition_links(_match_result)
        [self, :run_with_links]
      end

      def transition_attribute(match_result)
        attrib = match_result[1]
        #        [values[attrib.to_sym], :run]
        [OData::Attribute.new(self, attrib), :run]
      end

      def transition_nav_collection(match_result)
        attrib = match_result[1]
        [get_related(attrib), :run]
      end

      def transition_nav_entity(match_result)
        attrib = match_result[1]
        [get_related_entity(attrib), :run]
      end
    end

    include Transitions

    def nav_values
      @nav_values = {}

      self.class.nav_entity_attribs&.each_key do |na_str|
        @nav_values[na_str.to_sym] = send(na_str)
      end
      @nav_values
    end

    def nav_coll
      @nav_coll = {}
      self.class.nav_collection_attribs&.each_key do |nc_str|
        @nav_coll[nc_str.to_sym] = send(nc_str)
      end
      @nav_coll
    end

    def uri
      @odata_pk ||= "(#{pk_uri})"
      "#{self.class.uri}#{@odata_pk}"
    end

    D = 'd'.freeze
    DJ_OPEN = '{"d":'.freeze
    DJ_CLOSE = '}'.freeze

    # Json formatter for a single entity (probably OData V1/V2 like)
    def to_odata_json(service:)
      template = self.class.output_template(@uparms)
      innerj = service.get_entity_odata_h(entity: self,
                                          template: template).to_json
      "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
    end

    # Json formatter for a single entity reached by navigation $links
    def to_odata_onelink_json(service:)
      innerj = service.get_entity_odata_link_h(entity: self).to_json
      "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
    end

    def selected_values_for_odata(cols)
      allvals = values_for_odata
      selvals = {}
      cols.map(&:to_sym).each { |k| selvals[k] = allvals[k] if allvals.key?(k) }
      selvals
    end

    # post paylod expects the new entity in an array
    def to_odata_post_json(service:)
      innerj = service.get_coll_odata_h(array: [self],
                                        template: self.class.default_template).to_json
      "#{DJ_OPEN}#{innerj}#{DJ_CLOSE}"
    end

    def type_name
      self.class.type_name
    end

    def copy_request_infos(req)
      @params = req.params
      @do_links = req.walker.do_links
      @uparms = UrlParameters4Single.new(@params)
    end

    # Finally Process REST verbs...
    def odata_get(req)
      copy_request_infos(req)
      if req.walker.media_value
        odata_media_value_get(req)
      elsif req.accept?(APPJSON)
        if req.walker.do_links
          [200, CT_JSON, [to_odata_onelink_json(service: req.service)]]
        else
          [200, CT_JSON, [to_odata_json(service: req.service)]]
        end
      else # TODO: other formats
        415
      end
    end

    DELETE_REL_AND_ENTY = lambda do |entity, assoc, parent|
      OData.remove_nav_relation(assoc, parent)
      entity.destroy(transaction: false)
    end

    def odata_delete_relation_and_entity(req, assoc, parent)
      if parent
        if req.in_changeset
          # in-changeset requests get their own transaction
          DELETE_REL_AND_ENTY.call(self, assoc, parent)
        else
          db.transaction do
            DELETE_REL_AND_ENTY.call(self, assoc, parent)
          end
        end
      else
        destroy(transaction: false)
      end
    rescue StandardError => e
      raise SequelAdapterError.new(e)
    end

    # TODO: differentiate between POST/PUT/PATCH/MERGE
    def odata_post(req)
      if req.walker.media_value
        odata_media_value_put(req)
      elsif req.accept?(APPJSON)
        data.delete('__metadata')

        if req.in_changeset
          set_fields(data, self.class.data_fields, missing: :skip)
          save(transaction: false)
        else
          update_fields(data, self.class.data_fields, missing: :skip)
        end

        [202, EMPTY_HASH, to_odata_post_json(service: req.service)]
      else # TODO: other formats
        415
      end
    end

    def odata_put(req)
      if req.walker.media_value
        odata_media_value_put(req)
      elsif req.accept?(APPJSON)
        data = JSON.parse(req.body.read)
        data.delete('__metadata')

        if req.in_changeset
          set_fields(data, self.class.data_fields, missing: :skip)
          save(transaction: false)
        else
          update_fields(data, self.class.data_fields, missing: :skip)
        end

        ARY_204_EMPTY_HASH_ARY
      else # TODO: other formats
        415
      end
    end

    def odata_patch(req)
      req.with_parsed_data do |data|
        data.delete('__metadata')

        # validate payload column names
        if (invalid = self.class.invalid_hash_data?(data))
          ::OData::Request::ON_CGST_ERROR.call(req)
          return [422, EMPTY_HASH, ['Invalid attribute name: ', invalid.to_s]]
        end
        # TODO: check values/types

        my_data_fields = self.class.data_fields

        if req.in_changeset
          set_fields(data, my_data_fields, missing: :skip)
          save(transaction: false)
        else
          update_fields(data, my_data_fields, missing: :skip)
        end
        # patch should return 204 + no content
        ARY_204_EMPTY_HASH_ARY
      end
    end

    # redefinitions of the main methods for a navigated collection
    # (eg. all Books of Author[2]  is Author[2].Books.all )
    module NavigationRedefinitions
      def all
        @child_method.call
      end

      def count
        @child_method.call.count
      end

      def dataset
        @child_dataset_method.call
      end

      def navigated_dataset
        @child_dataset_method.call
      end

      def each
        y = @child_method.call
        y.each { |enty| yield enty }
      end

      # TODO: design... this is not DRY
      def slug_field
        superclass.slug_field
      end

      def type_name
        superclass.type_name
      end

      def time_cols
        superclass.time_cols
      end

      def media_handler
        superclass.media_handler
      end

      def uri
        superclass.uri
      end

      def default_template
        superclass.default_template
      end

      def allowed_transitions
        superclass.allowed_transitions
      end

      def entity_allowed_transitions
        superclass.entity_allowed_transitions
      end

      def to_a
        y = @child_method.call
        y.to_a
      end
    end
    # GetRelated that returns a anonymous Class (ie. representing a collection)
    # subtype of the related object Class  ( childklass )
    # (...to_many relationship )
    def get_related(childattrib)
      parent = self
      childklass = self.class.nav_collection_attribs[childattrib]
      Class.new(childklass) do
        # this makes use of Sequel's Model relationships; eg this is
        # 'Race[12].Edition'
        # where Race[12] would be our self and 'Edition' is the
        # childattrib(collection)
        @child_method = parent.method(childattrib.to_sym)
        @child_dataset_method = parent.method("#{childattrib}_dataset".to_sym)
        @nav_parent = parent
        @navattr_reflection = parent.class.association_reflections[childattrib.to_sym]
        prepare_pk
        prepare_fields
        # Now in this anonymous Class we can refine the "all, count and []
        # methods, to take into account the relationship
        extend NavigationRedefinitions
      end
    end

    # GetRelatedEntity that returns an single related Entity
    # (...to_one relationship )
    def get_related_entity(childattrib)
      # this makes use of Sequel's Model relationships; eg this is
      # 'Race[12].RaceType'
      # where Race[12] would be our self and 'RaceType' is the single
      # childattrib entity

      # when the child attribute is nil (because the FK is NULL on DB)
      # then we return a Nil... wrapper object. This object then
      # allows to receive a POST operation that would actually create the nav attribute entity

      ret = method(childattrib.to_sym).call || OData::NilNavigationAttribute.new

      ret.set_relation_info(self, childattrib)

      ret
    end
  end
  # end of module ODataEntity
  module Entity
    include EntityBase
  end

  module NonMediaEntity
    # non media entity metadata for json h
    def metadata_h(namespace = nil)
      {   uri: uri,
          type: namespace ? "#{namespace}.#{type_name}" : type_name }
    end

    def values_for_odata
      values
    end

    def odata_delete(req)
      if req.accept?(APPJSON)
        #        delete
        begin
          odata_delete_relation_and_entity(req, @navattr_reflection, @nav_parent)
          [200, CT_JSON, [{ 'd' => req.service.get_emptycoll_odata_h }.to_json]]
        rescue SequelAdapterError => e
          BadRequestSequelAdapterError.new(e).odata_get(req)
        end
      else # TODO: other formats
        415
      end
    end

    # in case of a non media entity, we have to return an error on $value request
    def odata_media_value_get(_req)
      BadRequestNonMediaValue.odata_get
    end

    # in case of a non media entity, we have to return an error on $value PUT
    def odata_media_value_put(_req)
      BadRequestNonMediaValue.odata_get
    end
  end

  module MappingBeforeOutput
    # needed for proper datetime output
    def casted_values(cols = nil)
      vals = case cols
             when nil
               # we need to dup the model values as we need to change it before passing to_json,
               # but we dont want to interfere with Sequel's owned data
               # (eg because then in worst case it could happen that we write back changed values to DB)
               values_for_odata.dup
             else
               selected_values_for_odata(cols)
        end
      self.class.time_cols.each { |tc| vals[tc] = vals[tc]&.iso8601 if vals.key?(tc) }
      vals
    end
  end
  module NoMappingBeforeOutput
    # current model does not have eg. Time fields--> no special mapping, just to_json is fine
    # --> we can use directly the model.values (values_for_odata) withoud dup'ing it as we dont
    # need to change it, just output as is
    def casted_values(cols = nil)
      case cols
      when nil
        values_for_odata
      else
        selected_values_for_odata(cols)
        end
    end
  end

  module MediaEntity
    # media entity metadata for json h
    def metadata_h(namespace = nil)
      {   uri: uri,
          type: namespace ? "#{namespace}.#{type_name}" : type_name,
          media_src: media_src,
          edit_media: edit_media,
          content_type: @values[:content_type] }
    end

    def media_src
      version = self.class.media_handler.ressource_version(self)
      "#{uri}/$value?version=#{version}"
    end

    def edit_media
      "#{uri}/$value"
    end

    # directory where to put/find the media files for this entity-type
    def klass_dir
      type_name
    end

    #    # this is just ModelKlass/pk as a single string
    #    def qualified_media_path_id
    #      "#{self.class}/#{media_path_id}"
    #    end

    def values_for_odata
      ret = values.dup
      ret.delete(:content_type)
      ret
    end

    def odata_delete(req)
      if req.accept?(APPJSON)
        # delete the MR
        # delegate to the media handler on collection(ie class) level
        # TODO error handling

        self.class.media_handler.odata_delete(entity: self)
        # delete the relation(s)  to parent(s) (if any) and then entity
        odata_delete_relation_and_entity(req, @navattr_reflection, @nav_parent)
        # result
        [200, CT_JSON, [{ 'd' => req.service.get_emptycoll_odata_h }.to_json]]
      else # TODO: other formats
        415
      end
    end

    # real implementation for returning $value for a media entity
    def odata_media_value_get(req)
      # delegate to the media handler on collection(ie class) level
      self.class.media_handler.odata_get(request: req, entity: self)
    end

    # real implementation for replacing $value for a media entity
    def odata_media_value_put(req)
      model = self.class
      req.with_media_data do |data, mimetype, filename|
        emdata = { content_type: mimetype }
        if req.in_changeset
          set_fields(emdata, model.data_fields, missing: :skip)
          save(transaction: false)
        else
          update_fields(emdata, model.data_fields, missing: :skip)
        end
        model.media_handler.replace_file(data: data,
                                         entity: self,
                                         filename: filename)
        ARY_204_EMPTY_HASH_ARY
      end
    end
  end

  # for a single public key
  module EntitySinglePK
    include Entity
    def pk_uri
      pk
    end

    def media_path_id
      pk.to_s
    end

    def media_path_ids
      [pk]
    end
  end

  # for multiple key
  module EntityMultiPK
    include Entity
    def pk_uri
      # pk_hash is provided by Sequel
      pk_hash.map { |k, v| "#{k}='#{v}'" }.join(COMMA)
    end

    def media_path_id
      pk_hash.values.join(SPACE)
    end

    def media_path_ids
      pk_hash.values
    end
  end
end
# end of Module OData
