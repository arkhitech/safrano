require 'json'
require 'rexml/document'
require 'safrano.rb'

# Error handling
module OData
  # for errors occurring in API (publishing) usage --> Exceptions
  module API
    # when published class is not a Sequel Model
    class ModelNameError < NameError
      def initialize(name)
        super("class #{name} is not a Sequel Model", name)
      end
    end
    # when published class as media does not have the mandatory media fields
    class MediaModelError < NameError
      def initialize(name)
        super("Model #{name} does not have the mandatory media attributes content_type/media_src", name)
      end
    end

    # when published association was not defined on Sequel level
    class ModelAssociationNameError < NameError
      def initialize(klass, symb)
        symbname = symb.to_s
        msg = "There is no association :#{symbname} defined in class #{klass}"
        super(msg, symbname)
      end
    end
  end

  # base module for HTTP errors, when used as a Error Class
  module ErrorClass
    def odata_get(req)
      if req.accept?(APPJSON)
        [const_get(:HTTP_CODE), CT_JSON,
         { 'odata.error' => { 'code' => '', 'message' => @msg } }.to_json]
      else
        [const_get(:HTTP_CODE), CT_TEXT, @msg]
      end
    end
  end

  # base module for HTTP errors, when used as an Error instance
  module ErrorInstance
    def odata_get(req)
      if req.accept?(APPJSON)
        [self.class.const_get(:HTTP_CODE), CT_JSON,
         { 'odata.error' => { 'code' => '', 'message' => @msg } }.to_json]
      else
        [self.class.const_get(:HTTP_CODE), CT_TEXT, @msg]
      end
    end
  end

  # http Bad Req.
  class BadRequestError
    extend  ErrorClass
    HTTP_CODE = 400
    @msg = 'Bad Request Error'
  end
  # Generic failed changeset
  class BadRequestFailedChangeSet < BadRequestError
    HTTP_CODE = 400
    @msg = 'Bad Request: Failed changeset '
  end

  # $value request for a non-media entity
  class BadRequestNonMediaValue < BadRequestError
    HTTP_CODE = 400
    @msg = 'Bad Request: $value request for a non-media entity'
  end
  class BadRequestSequelAdapterError < BadRequestError
    include ErrorInstance
    def initialize(err)
      @msg = err.inner.message
    end
  end

  # for Syntax error in Filtering
  class BadRequestFilterParseError < BadRequestError
    HTTP_CODE = 400
    @msg = 'Bad Request: Syntax error in $filter'
  end

  # for Syntax error in $expand param
  class BadRequestExpandParseError < BadRequestError
    HTTP_CODE = 400
    @msg = 'Bad Request: Syntax error in $expand'
  end

  # for Syntax error in $orderby param
  class BadRequestOrderParseError < BadRequestError
    HTTP_CODE = 400
    @msg = 'Bad Request: Syntax error in $orderby'
  end

  # for $inlinecount error
  class BadRequestInlineCountParamError < BadRequestError
    HTTP_CODE = 400
    @msg = 'Bad Request: wrong $inlinecount parameter'
  end
  # http not found
  class ErrorNotFound
    extend  ErrorClass
    HTTP_CODE = 404
    @msg = 'The requested ressource was not found'
  end
  # Transition error (Safrano specific)
  class ServerTransitionError
    extend  ErrorClass
    HTTP_CODE = 500
    @msg = 'Server error: Segment could not be parsed'
  end
  # generic http 500 server err
  class ServerError
    extend  ErrorClass
    HTTP_CODE = 500
    @msg = 'Server error'
  end
  # not implemented (Safrano specific)
  class NotImplementedError
    extend  ErrorClass
    HTTP_CODE = 501
  end
  # batch not implemented (Safrano specific)
  class BatchNotImplementedError
    extend  ErrorClass
    HTTP_CODE = 501
    @msg = 'Not implemented: OData batch'
  end

  # error in filter parsing (Safrano specific)
  class FilterParseError < BadRequestError
    extend  ErrorClass
    HTTP_CODE = 400
  end

  class FilterFunctionNotImplementedError < BadRequestError
    extend  ErrorClass
    include ErrorInstance
    @msg = 'the requested $filter function is Not implemented'
    HTTP_CODE = 400
    def initialize(exception)
      @msg = exception.to_s
    end
  end
  class FilterInvalidFunctionError < BadRequestError
    include ErrorInstance
    HTTP_CODE = 400
    def initialize(exception)
      @msg = exception.to_s
    end
  end
end
