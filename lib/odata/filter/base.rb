# frozen_string_literal: true

module OData
  module Filter
    # Base class for Leaves, Trees, RootTrees etc
    class Node
    end

    # Leaves are Nodes with a parent but no children
    class Leave < Node
    end

    # RootTrees have childrens but no parent
    class RootTree < Node
    end

    # Tree's have Parent and children
    class Tree < RootTree
    end

    # For functions... should have a single child---> the argument list
    class FuncTree < Tree
    end

    # Indentity Func to use as "parent" func of parenthesis expressions
    # --> allow to handle generically parenthesis always as argument of
    # some function
    class IdentityFuncTree < FuncTree
    end

    # unary op eg. NOT
    class UnopTree < Tree
    end

    # Bin ops
    class BinopTree < Tree
    end

    class BinopBool < BinopTree
    end

    class BinopArithm < BinopTree
    end

    # Arguments or lists
    class ArgTree < Tree
    end

    # Numbers (floating point, ints, dec)
    class FPNumber < Leave
    end

    # Literals are unquoted words without /
    class Literal < Leave
    end

    # Qualit (qualified lits) are words separated by /
    # path/path/path/attrib
    class Qualit < Literal
    end

    # Quoted Strings
    class QString < Leave
    end
  end
end
