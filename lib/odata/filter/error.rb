require_relative '../error'

module OData
  class SequelAdapterError < StandardError
    attr_reader :inner
    def initialize(err)
      @inner = err
    end
  end

  # exception to OData error bridge
  module ErrorBridge
    # return an odata error object wrapping the exception
    # the odata error object should respond to odata_get for output
    def odata_error
      self.class.const_get('ODATA_ERROR_KLASS').new(self)
    end
  end

  module Filter
    class FunctionNotImplemented < StandardError
      ODATA_ERROR_KLASS = OData::FilterFunctionNotImplementedError
      include ::OData::ErrorBridge
    end

    class Parser
      # Parser errors
      class Error < StandardError
        attr_reader :tok
        attr_reader :typ
        attr_reader :cur_val
        attr_reader :cur_typ
        def initialize(tok, typ, cur)
          @tok = tok
          @typ = typ
          return unless cur

          @cur_val = cur.value
          @cur_typ = cur.class
        end
      end
      # Invalid Tokens
      class ErrorInvalidToken < Error
      end
      # Unmached closed
      class ErrorUnmatchedClose < Error
      end

      class ErrorFunctionArgumentType < StandardError
      end

      class ErrorWrongColumnName < StandardError
      end

      # attempt to add a child to a Leave
      class ErrorLeaveChild < StandardError
      end

      # invalid function error (literal attach to IdentityFuncTree)
      class ErrorInvalidFunction < StandardError
        ODATA_ERROR_KLASS = OData::FilterInvalidFunctionError
        include ::OData::ErrorBridge
      end

      # Invalid function arity
      class ErrorInvalidArity < Error
      end
      # Invalid separator in this context (missing parenthesis?)
      class ErrorInvalidSeparator < Error
      end

      # unmatched quot3
      class UnmatchedQuoteError < Error
      end

      # wrong type of function argument
      class ErrorInvalidArgumentType < StandardError
        def initialize(tree, expected:,  actual:)
          @tree = tree
          @expected = expected
          @actual = actual
        end
      end
    end
  end
end
