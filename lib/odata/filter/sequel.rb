# frozen_string_literal: true

require_relative './base.rb'
require_relative './sequel_function_adapter.rb'

module OData
  module Filter
    # Base class for Leaves, Trees, RootTrees etc
    #    class Node
    #    end

    # Leaves are Nodes with a parent but no children
    #    class Leave < Node
    #    end

    # RootTrees have childrens but no parent
    class RootTree
      def apply_to_dataset(dtcx, jh)
        filtexpr = @children.first.leuqes(jh)
        jh.dataset(dtcx).where(filtexpr).select_all(jh.start_model.table_name)
      end

      def sequel_expr(jh)
        @children.first.leuqes(jh)
      end
    end

    # Tree's have Parent and children
    class Tree < RootTree
    end

    # For functions... should have a single child---> the argument list
    # note: Adapter specific function helpers like year() or substringof_sig2()
    # need to be mixed in on startup (eg. on publish finalize)
    class FuncTree < Tree
      def leuqes(jh)
        case @value
        when :startswith
          Sequel.like(args[0].leuqes(jh),
                      args[1].leuqes_starts_like(jh))
        when :endswith
          Sequel.like(args[0].leuqes(jh),
                      args[1].leuqes_ends_like(jh))
        when :substringof

          # there are multiple possible argument types (but all should return edm.string)
          if args[0].is_a?(QString)
            # substringof('Rhum', name)  -->
            # name contains substr 'Rhum'
            Sequel.like(args[1].leuqes(jh),
                        args[0].leuqes_substringof_sig1(jh))
          # special non standard (ui5 client) case ?
          elsif args[0].is_a?(Literal) && args[1].is_a?(Literal)
            Sequel.like(args[1].leuqes(jh),
                        args[0].leuqes_substringof_sig1(jh))
          elsif args[1].is_a?(QString)
            substringof_sig2(jh) # adapter specific
          else
            # TODO... actually not supported?
            raise OData::Filter::Parser::ErrorFunctionArgumentType
          end
        when :concat
          Sequel.join([args[0].leuqes(jh),
                       args[1].leuqes(jh)])
        when :length
          Sequel.char_length(args.first.leuqes(jh))
        when :trim
          Sequel.trim(args.first.leuqes(jh))
        when :toupper
          Sequel.function(:upper, args.first.leuqes(jh))
        when :tolower
          Sequel.function(:lower, args.first.leuqes(jh))
        # all datetime funcs are adapter specific (because sqlite does not have extract)
        when :year
          year(jh)
        when :month
          month(jh)
        when :second
          second(jh)
        when :minute
          minute(jh)
        when :hour
          hour(jh)
        when :day
          day(jh)
        # math functions
        when :round
          Sequel.function(:round, args.first.leuqes(jh))
        when :floor
          floor(jh)
        when :ceiling
          ceiling(jh)
        else
          raise OData::FilterParseError
        end
      end
    end

    # Indentity Func to use as "parent" func of parenthesis expressions
    # --> allow to handle generically parenthesis always as argument of
    # some function
    class IdentityFuncTree < FuncTree
      def leuqes(jh)
        args.first.leuqes(jh)
      end
    end

    # unary op eg. NOT
    class UnopTree < Tree
      def leuqes(jh)
        case @value
        when :not
          Sequel.~(@children.first.leuqes(jh))
        else
          raise OData::FilterParseError
        end
      end
    end

    # logical Bin ops
    class BinopBool
      def leuqes(jh)
        leuqes_op = case @value
                    when :eq
                      :'='
                    when :ne
                      :'!='
                    when :le
                      :<=
                    when :ge
                      :'>='
                    when :lt
                      :<
                    when :gt
                      :>
                    when :or
                      :OR
                    when :and
                      :AND
                    else
                      raise OData::FilterParseError
                    end

        Sequel::SQL::BooleanExpression.new(leuqes_op,
                                           @children[0].leuqes(jh),
                                           @children[1].leuqes(jh))
      end
    end

    # Arithmetic Bin ops
    class BinopArithm
      def leuqes(jh)
        leuqes_op = case @value
                    when :add
                      :+
                    when :sub
                      :-
                    when :mul
                      :*
                    when :div
                      :/
                    when :mod
                      :%
                    else
                      raise OData::FilterParseError
                    end

        Sequel::SQL::NumericExpression.new(leuqes_op,
                                           @children[0].leuqes(jh),
                                           @children[1].leuqes(jh))
      end
    end

    # Arguments or lists
    class ArgTree
    end

    # Numbers (floating point, ints, dec)
    class FPNumber
      def leuqes(_jh)
        Sequel.lit(@value)
      end

      def leuqes_starts_like(_jh)
        "#{@value}%"
      end

      def leuqes_ends_like(_jh)
        "%#{@value}"
      end

      def leuqes_substringof_sig1(_jh)
        "%#{@value}%"
      end
    end

    # Literals are unquoted words
    class Literal
      def leuqes(jh)
        raise OData::Filter::Parser::ErrorWrongColumnName unless jh.start_model.db_schema.key?(@value.to_sym)

        Sequel[jh.start_model.table_name][@value.to_sym]
      end

      # non stantard extensions to support things like
      # substringof(Rhum, name)  ????
      def leuqes_starts_like(_jh)
        "#{@value}%"
      end

      def leuqes_ends_like(_jh)
        "%#{@value}"
      end

      def leuqes_substringof_sig1(_jh)
        "%#{@value}%"
      end

      def as_string
        @value
      end
    end

    # Qualit (qualified lits) are words separated by /
    class Qualit
      def leuqes(jh)
        jh.add(@path)
        talias = jh.start_model.get_alias_sym(@path)
        Sequel[talias][@attrib.to_sym]
      end
    end

    # Quoted Strings
    class QString
      def leuqes(_jh)
        @value
      end

      def leuqes_starts_like(_jh)
        "#{@value}%"
      end

      def leuqes_ends_like(_jh)
        "%#{@value}"
      end

      def leuqes_substringof_sig1(_jh)
        "%#{@value}%"
      end
    end
  end
end
