# frozen_string_literal: true

require_relative './tree.rb'
require_relative './sequel.rb'

module OData
  module Filter
    # sqlite adapter specific function handler
    module FuncTreeSqlite
      def substringof_sig2(jh)
        # substringof(name, '__Route du Rhum__')  -->
        # '__Route du Rhum__' contains name as a substring
        # sqlite uses instr()

        substr_func = Sequel.function(:instr, args[1].leuqes(jh), args[0].leuqes(jh))

        Sequel::SQL::BooleanExpression.new(:>, substr_func, 0)
      end
      # %d		day of month: 00
      # %f		fractional seconds: SS.SSS
      # %H		hour: 00-24
      # %j		day of year: 001-366
      # %J		Julian day number
      # %m		month: 01-12
      # %M		minute: 00-59
      # %s		seconds since 1970-01-01
      # %S		seconds: 00-59
      # %w		day of week 0-6 with Sunday==0
      # %W		week of year: 00-53
      # %Y		year: 0000-9999
      # %%		%

      # sqlite does not have extract but recommends to use strftime
      def year(jh)
        Sequel.function(:strftime, '%Y', args.first.leuqes(jh)).cast(:integer)
      end

      def month(jh)
        Sequel.function(:strftime, '%m', args.first.leuqes(jh)).cast(:integer)
      end

      def second(jh)
        Sequel.function(:strftime, '%S', args.first.leuqes(jh)).cast(:integer)
      end

      def minute(jh)
        Sequel.function(:strftime, '%M', args.first.leuqes(jh)).cast(:integer)
      end

      def hour(jh)
        Sequel.function(:strftime, '%H', args.first.leuqes(jh)).cast(:integer)
      end

      def day(jh)
        Sequel.function(:strftime, '%d', args.first.leuqes(jh)).cast(:integer)
      end

      def floor(jh)
        raise OData::Filter::FunctionNotImplemented, "$filter function 'floor' is not implemented in sqlite adapter"
      end

      def ceiling(jh)
        raise OData::Filter::FunctionNotImplemented, "$filter function 'ceiling' is not implemented in sqlite adapter"
      end
    end
    # re-useable module with math floor/ceil functions for those adapters having these SQL funcs
    module MathFloorCeilFuncTree
      def floor(jh)
        Sequel.function(:floor, args.first.leuqes(jh))
      end

      def ceiling(jh)
        Sequel.function(:ceil, args.first.leuqes(jh))
      end
    end

    # re-useable module with Datetime functions with extract()
    module DateTimeFuncTreeExtract
      def year(jh)
        args.first.leuqes(jh).extract(:year)
      end

      def year(jh)
        args.first.leuqes(jh).extract(:year)
      end

      def month(jh)
        args.first.leuqes(jh).extract(:month)
      end

      def second(jh)
        args.first.leuqes(jh).extract(:second)
      end

      def minute(jh)
        args.first.leuqes(jh).extract(:minute)
      end

      def hour(jh)
        args.first.leuqes(jh).extract(:hour)
      end

      def day(jh)
        args.first.leuqes(jh).extract(:day)
      end
    end

    # postgresql adapter specific function handler
    module FuncTreePostgres
      def substringof_sig2(jh)
        # substringof(name, '__Route du Rhum__')  -->
        # '__Route du Rhum__' contains name as a substring
        # postgres does not know instr() but has strpos
        substr_func = Sequel.function(:strpos, args[1].leuqes(jh), args[0].leuqes(jh))

        Sequel::SQL::BooleanExpression.new(:>, substr_func, 0)
      end

      # postgres uses extract()
      include DateTimeFuncTreeExtract

      # postgres has floor/ceil funcs
      include MathFloorCeilFuncTree
    end

    # default adapter  function handler for all others... try to use the most common version
    # :substring --> instr because here is seems Postgres is special
    # datetime funcs --> exctract, here sqlite is special(uses format)
    # note: we dont test this, provided as an example/template, might work eg for mysql
    module FuncTreeDefault
      def substringof_sig2(jh)
        # substringof(name, '__Route du Rhum__')  -->
        # '__Route du Rhum__' contains name as a substring
        # instr() seems to be the most common substring func
        substr_func = Sequel.function(:instr, args[1].leuqes(jh), args[0].leuqes(jh))

        Sequel::SQL::BooleanExpression.new(:>, substr_func, 0)
      end

      # XYZ uses extract() ?
      include DateTimeFuncTreeExtract

      # ... assume SQL
      include MathFloorCeilFuncTree
    end
  end
end
