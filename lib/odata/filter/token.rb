# frozen_string_literal: true

# top level OData namespace
module OData
  module Filter
    class Parser
      # Input tokenizer
      module Token
        FUNCNAMES = %w[concat substringof endswith startswith length indexof
                       replace substring trim toupper tolower
                       day hour minute month second year
                       round floor ceiling].freeze
        FUNCRGX = FUNCNAMES.join('|').freeze
        QSTRINGRGX = /'((?:[^']|(?:\'{2}))*)'/.freeze
        BINOBOOL = '[eE][qQ]|[LlgGNn][eETt]|[aA][nN][dD]|[oO][rR]'.freeze
        BINOARITHM = '[aA][dD][dD]|[sS][uU][bB]|[mM][uU][lL]|[dD][iI][vV]|[mM][oO][dD]'.freeze
        NOTRGX = 'not|NOT'.freeze
        FPRGX = '\d+(?:\.\d+)?(?:e[+-]?\d+)?'.freeze
        QUALITRGX = '\w+(?:\/\w+)+'.freeze
        RGX = /(#{FUNCRGX})|([\(\),])|(#{BINOBOOL})|(#{BINOARITHM})|(#{NOTRGX})|#{QSTRINGRGX}|(#{FPRGX})|(#{QUALITRGX})|(\w+)|(')/.freeze
        def each_typed_token(inp)
          typ = nil

          inp.scan(RGX) do |groups|
            idx = nil
            found = nil
            groups.each_with_index do |tok, i|
              if (found = tok)
                idx = i
                break
              end
            end
            typ = case idx
                  when 0
                    :FuncTree
                  when 1
                    case found
                    when '(', ')'
                      :Delimiter
                    when ','
                      :Separator
                    end
                  when 2
                    :BinopBool
                  when 3
                    :BinopArithm
                  when 4
                    :UnopTree
                  when 5
                    :QString
                  when 6
                    :FPNumber
                  when 7
                    :Qualit
                  when 8
                    :Literal
                  when 9
                    :unmatchedQuote
                  end
            yield found, typ
          end
        end
      end
    end
  end
end
