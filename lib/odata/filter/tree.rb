# frozen_string_literal: true

require_relative './base.rb'
require_relative './error.rb'

module OData
  module Filter
    # Base class for Leaves, Trees, RootTrees etc
    class Node
      attr_reader :value
      def initialize(val, &block)
        @value = val
        instance_eval(&block) if block_given?
      end

      def ==(other)
        @value == other.value
      end
    end

    # Leaves are Nodes with a parent but no children
    class Leave
      attr_accessor :parent
      def accept?(tok, typ)
        [false, Parser::ErrorInvalidToken(tok, typ)]
      end

      def check_types; end

      def attach(child)
        # TODO better reporting of error infos
        raise ErrorLeaveChild
      end
    end

    # RootTrees have childrens but no parent
    class RootTree
      attr_reader :children
      attr_accessor :state
      def initialize(val: :root, &block)
        @children = []
        super(val, &block)
      end

      def attach(child)
        child.parent = self
        @children << child
      end

      def detach(child)
        child.parent = nil
        @children.delete(child)
      end

      def ==(other)
        super(other) && (@children == other.children)
      end

      def update_state(tok, typ) end

      def accept?(tok, typ)
        case typ
        when :Literal, :Qualit, :QString, :FuncTree, :ArgTree, :UnopTree, :FPNumber
          true
        when :Delimiter
          if tok == '('
            true
          else
            [false, Parser::ErrorInvalidToken.new(tok, typ, self)]
          end
        else
          [false, Parser::ErrorInvalidToken.new(tok, typ, self)]
        end
      end

      def check_types
        @children.each(&:check_types)
      end
    end

    # Tree's have Parent and children
    class Tree
      attr_accessor :parent

      def initialize(val)
        super(val: val)
      end
    end

    # For functions... should have a single child---> the argument list
    class FuncTree
      def initialize(val)
        super(val.downcase.to_sym)
      end

      def args
        @children.first.children
      end

      def arity_full?(cursize)
        cursize >= max_arity
      end

      def max_arity
        case @value
        when :replace
          3
        when :concat, :substringof, :substring, :endswith, :startswith
          2
        else
          1
        end
      end

      def edm_type
        case @value
        when :concat, :substring
          :string
        when :length
          :int
        when :substringof, :endswith, :startswith
          :bool
        else
          :any
        end
      end

      def accept?(tok, typ)
        case typ
        when :BinopBool, :BinopArithm
          true
        else
          super(tok, typ)
        end
      end

      def check_types
        case @value
        when :length
          argtyp = args.first.edm_type
          if (argtyp != :any) && (argtyp != :string)
            raise Parser::ErrorInvalidArgumentType.new(self,
                                                       expected: :string,
                                                       actual: argtyp)
          end
        end
        super
      end
    end

    # Indentity Func to use as "parent" func of parenthesis expressions
    # --> allow to handle generically parenthesis always as argument of
    # some function
    class IdentityFuncTree
      def initialize
        super(:__indentity)
      end

      # we can have parenthesis with one expression inside everywhere
      # only in FuncTree this is redefined for the function's arity
      def arity_full?(cursize)
        cursize >= 1
      end

      def edm_type
        @children.first.edm_type
      end
    end

    # unary op eg. NOT
    class UnopTree
      def initialize(val)
        super(val.downcase.to_sym)
      end

      # reference:
      # OData v4 par 5.1.1.9 Operator Precedence
      def precedence
        case @value
        when :not
          7
        else
          999
        end
      end

      def edm_type
        case @value
        when :not
          :bool
        else
          :any
        end
      end
    end

    # Bin ops
    class BinopTree
      def initialize(val)
        @state = :open
        super(val.downcase.to_sym)
      end

      def update_state(_tok, typ)
        case typ
        when :Literal, :Qualit, :QString, :FuncTree, :BinopBool, :BinopArithm, :UnopTree, :FPNumber
          @state = :closed
        end
      end
    end

    class BinopBool
      # reference:
      # OData v4 par 5.1.1.9 Operator Precedence
      def precedence
        case @value
        when :or
          1
        when :and
          2
        when :eq, :ne
          3
        when :gt, :ge, :lt, :le, :isof
          4
        else
          999
        end
      end

      def edm_type
        :bool
      end
    end

    class BinopArithm
      # reference:
      # OData v4 par 5.1.1.9 Operator Precedence
      def precedence
        case @value
        when :add, :sub
          5
        when :mul, :div, :mod
          6
        else
          999
        end
      end

      # TODO: different num types?
      def edm_type
        :any
      end
    end

    # Arguments or lists
    class ArgTree
      attr_reader :type
      def initialize(val)
        @type = :expression
        @state = :open
        super
      end

      def update_state(_tok, typ)
        case typ
        when :Delimiter
          @state = :closed
        when :Separator
          @state =  :sep
        when :Literal, :Qualit, :QString, :FuncTree, :FPNumber
          @state =  :val
        end
      end

      def accept?(tok, typ)
        case typ
        when :Delimiter
          if @value == '(' && tok == ')' && @state != :closed
            if @parent.arity_full?(@children.size)
              true
            else
              [false, Parser::ErrorInvalidArity.new(tok, typ, self)]
            end
          else
            [false, Parser::ErrorUnmatchedClose.new(tok, typ, self)]
          end
        when :Separator
          if @value == '(' && tok == ',' && @state == :val
            true
          elsif @state == :sep
            [false, Parser::ErrorInvalidToken.new(tok, typ, self)]
          else
            true
          end
        when :Literal, :Qualit, :QString, :FuncTree, :FPNumber
          if (@state == :open) || (@state == :sep)
            if @parent.arity_full?(@children.size)
              [false, Parser::ErrorInvalidArity.new(tok, typ, self)]
            else
              true
            end
          else
            [false, Parser::ErrorInvalidToken.new(tok, typ, self)]
          end
        when :BinopBool, :BinopArithm
          true
        else
          [false, Parser::ErrorInvalidToken.new(tok, typ, self)]
        end
      end

      def ==(other)
        super(other) && @type == other.type && @state == other.state
      end
    end

    # Numbers (floating point, ints, dec)
    class FPNumber
      def accept?(tok, typ)
        case typ
        when :Delimiter, :Separator, :BinopBool, :BinopArithm
          true
        else
          [false, Parser::ErrorInvalidToken.new(tok, typ, self)]
        end
      end

      # TODO
      def edm_type
        :number
      end
    end

    # Literals are unquoted words without /
    class Literal
      def accept?(tok, typ)
        case typ
        when :Delimiter, :Separator, :BinopBool, :BinopArithm
          true
        else
          [false, Parser::ErrorInvalidToken.new(tok, typ, self)]
        end
      end

      def edm_type
        :any
      end

      # error, Literal are leaves
      # when the child is a IdentityFuncTree then this looks like
      # an attempt to use a unknown function, eg. ceil(Total)
      # instead of ceiling(Total)
      def attach(child)
        if child.kind_of? OData::Filter::IdentityFuncTree
          raise Parser::ErrorInvalidFunction.new("Error in $filter expr.: invalid function #{self.value}")
        else
          super
        end
      end
    end

    # Qualit (qualified lits) are words separated by /
    # path/path/path/attrib
    class Qualit
      REGEXP = /((?:\w+\/)+)(\w+)/.freeze
      attr_reader :path
      attr_reader :attrib
      def initialize(val)
        super(val)
        # split into path + attrib
        raise Parser::Error.new(self, Qualit) unless (md = REGEXP.match(val))

        @path = md[1].chomp('/')
        @attrib = md[2]
      end
    end

    # Quoted Strings
    class QString
      DBL_QO = "''".freeze
      SI_QO = "'".freeze
      def initialize(val)
        # unescape double quotes
        super(val.gsub(DBL_QO, SI_QO))
      end

      def accept?(tok, typ)
        case typ
        when :Delimiter, :Separator, :BinopBool, :BinopArithm
          true
        else
          [false, Parser::ErrorInvalidToken.new(tok, typ, self)]
        end
      end

      def edm_type
        :string
      end
    end
  end
end
