require 'json'
require_relative '../safrano/core.rb'
require_relative './entity.rb'

module OData
  # remove the relation between entity and parent by clearing
  # the FK field(s) (if allowed)
  def self.remove_nav_relation(assoc, parent)
    return unless assoc

    return unless assoc[:type] == :many_to_one

    # removes/clear the FK values in parent
    # thus deleting the "link" between the entity and the parent
    # Note: This is called if we have to delete the child--> can only be
    # done after removing the FK in parent (if allowed!)
    lks = [assoc[:key]].flatten
    lks.each do |lk|
      parent.set(lk => nil)
      parent.save(transaction: false)
    end
  end

  # link newly created entities(child) to an existing parent
  # by following the association_reflection rules
  def self.create_nav_relation(child, assoc, parent)
    return unless assoc

    # Note: this coding shares some bits from our sequel/plugins/join_by_paths,
    # method build_unique_join_segments
    # eventually there is an opportunity to have more reusable code here
    case assoc[:type]
    when :one_to_many, :one_to_one
      # sets the FK values in child to corresponding related parent key-values
      # thus creating the "link" between the new entity and the parent
      # if a FK value is already set (not nil/NULL) then only check the
      # consistency with the corresponding parent key-value
      # If the FK value and the parent key value are different, then it's  a
      # a Bad Request error

      leftm = assoc[:model] # should be same as parent.class
      lks = [leftm.primary_key].flatten
      rks = [assoc[:key]].flatten
      join_cond = rks.zip(lks).to_h
      join_cond.each do |rk, lk|
        if child.values[rk] # FK in new entity from payload not nil, only check consistency
          # with the parent - id(s)
          # if (child.values[rk] != parent.pk_hash[lk]) # error...
          # TODO
          # end
        else # we can set the FK value, thus creating the "link"
          child.set(rk => parent.pk_hash[lk])
        end
      end
    when :many_to_one
      # sets the FK values in parent to corresponding related child key-values
      # thus creating the "link" between the new entity and the parent
      # Per design, this can only be called when the FK value is nil
      # from NilNavigationAttribute.odata_post
      lks = [assoc[:key]].flatten
      rks = [child.class.primary_key].flatten
      join_cond = rks.zip(lks).to_h
      join_cond.each do |rk, lk|
        if parent.values[lk] # FK in parent not nil, only check consistency
          # with the child - id(s)
          # if parent.values[lk] != child.pk_hash[rk] # error...
          # TODO
          # end
        else # we can set the FK value, thus creating the "link"
          parent.set(lk => child.pk_hash[rk])
        end
      end
    end
  end

  module EntityBase
    module NavigationInfo
      attr_reader :nav_parent
      attr_reader :navattr_reflection
      attr_reader :nav_name
      def set_relation_info(parent, name)
        @nav_parent = parent
        @nav_name = name
        @navattr_reflection = parent.class.association_reflections[name.to_sym]
        @nav_klass = @navattr_reflection[:class_name].constantize
      end
    end
  end

  # Represents a named but nil-valued navigation-attribute of an Entity
  # (usually resulting from a NULL FK db value)
  class NilNavigationAttribute
    include EntityBase::NavigationInfo
    def odata_get(req)
      if req.walker.media_value
        OData::ErrorNotFound.odata_get
      elsif req.accept?(APPJSON)
        [200, CT_JSON, to_odata_json(service: req.service)]
      else # TODO: other formats
        415
      end
    end

    # create the nav. entity
    def odata_post(req)
      # delegate to the class method
      @nav_klass.odata_create_entity_and_relation(req,
                                                  @navattr_reflection,
                                                  @nav_parent)
    end

    # create the nav. entity
    def odata_put(req)
      #      if req.walker.raw_value
      # delegate to the class method
      @nav_klass.odata_create_entity_and_relation(req,
                                                  @navattr_reflection,
                                                  @nav_parent)
      #      else
      #      end
    end

    # empty output as OData json (v2)
    def to_odata_json(*)
      { 'd' => EMPTY_HASH }.to_json
    end

    # for testing purpose (assert_equal ...)
    def ==(other)
      (@nav_parent == other.nav_parent) && (@nav_name == other.nav_name)
    end

    # methods related to transitions to next state (cf. walker)
    module Transitions
      def transition_end(_match_result)
        [nil, :end]
      end

      def transition_value(_match_result)
        [self, :end_with_value]
      end

      ALLOWED_TRANSITIONS = [Safrano::TransitionEnd,
                             Safrano::TransitionValue].freeze

      def allowed_transitions
        ALLOWED_TRANSITIONS
      end
    end
    include Transitions
  end
end
