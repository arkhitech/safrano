#!/usr/bin/env ruby

require 'set'

# OData relation related classes/module
module OData
  # we represent a relation as a Set (unordered) of two end elements
  class Relation < Set
    #    attr_reader :rid
    attr_accessor :multiplicity

    def initialize(arg)
      super(arg)
      @multiplicity = {}
    end

    def sa
      sort
    end

    # we need a from/to order independant ID
    def rid
      OData::RelationManager.build_id(self)
    end

    # we need a from/to order independant OData like name
    def name
      x = sa.map(&:to_s)
      y = x.reverse
      [x.join('_'), y.join('_')].join('_')
    end

    def each_endobj
      tmp = to_a.sort
      yield tmp.first
      yield tmp.last
    end

    def set_multiplicity(obj, mult)
      ms = mult.to_s
      raise ArgumentError unless include?(obj)

      case ms
      when '1', '*', '0..1'
        @multiplicity[obj] = ms
      else
        raise ArgumentError
      end
    end

    def with_metadata_info(xnamespace)
      bdprops = []
      each_endobj do |eo|
        bdprops << { type: xnamespace ? "#{xnamespace}.#{eo}" : eo,
                     role: eo,
                     multiplicity: multiplicity[eo] }
      end
      yield name, bdprops
    end
  end

  # some usefull stuff
  class RelationManager
    def initialize
      @list = {}
    end

    def self.build_id(arg)
      arg.sort.map(&:to_s).join('_')
    end

    def each_rel
      raise ArgumentError unless block_given?

      @list.each { |_rid, rel| yield rel }
    end

    def get(arg)
      rid = OData::RelationManager.build_id(arg)
      if @list.key?(rid)
        @list[rid]
      else
        rel = OData::Relation.new(arg)
        @list[rid] = rel
      end
    end

    def get_metadata_xml_attribs(from, to, assoc_type, xnamespace, attrname)
      rel = get([from, to])
      # use Sequel reflection to get multiplicity (will be used later
      # in 2. Associations below)
      case assoc_type
      # TODO: use multiplicity 1 when needed instead of '0..1'
      when :one_to_one
        rel.set_multiplicity(from, '0..1')
        rel.set_multiplicity(to, '0..1')
      when :one_to_many
        rel.set_multiplicity(from, '0..1')
        rel.set_multiplicity(to, '*')
      when :many_to_one
        rel.set_multiplicity(from, '*')
        rel.set_multiplicity(to, '0..1')
      when :many_to_many
        rel.set_multiplicity(from, '*')
        rel.set_multiplicity(to, '*')
      end
      # <NavigationProperty Name="Supplier"
      # Relationship="ODataDemo.Product_Supplier_Supplier_Products"
      #         FromRole="Product_Supplier" ToRole="Supplier_Products"/>
      { 'Name' => attrname, 'Relationship' => xnamespace ? "#{xnamespace}.#{rel.name}" : rel.name,
        'FromRole' => from, 'ToRole' => to }
    end
  end
end
