require 'odata/error.rb'

# all dataset selecting related classes in our OData module
# ie  do eager loading
module OData
  # base class for selecting. We have to distinguish between
  # fields of the current entity, and the navigation properties
  # we can have one special case
  #  empty, ie no $select specified --> return all fields and all nav props
  #            ==> SelectAll

  class SelectBase
    ALL = new # re-useable selecting-all handler

    def self.factory(selectstr)
      case selectstr&.strip
      when nil, '', '*'
        ALL
      else
        Select.new(selectstr)
      end
    end

    def all_props?
      false
    end

    def ALL.all_props?
      true
    end
  end

  # single select
  class Select < SelectBase
    COMASPLIT = /\s*,\s*/.freeze
    attr_reader :props
    def initialize(selstr)
      @selectp = selstr.strip
      @props = @selectp.split(COMASPLIT)
    end
  end
end
