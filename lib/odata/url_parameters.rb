require 'odata/error.rb'

# url parameters processing . Mostly delegates to specialised classes
# (filter, order...) to convert into Sequel exprs.
module OData
  class UrlParametersBase
    attr_reader :expand
    attr_reader :select

    def check_expand
      return BadRequestExpandParseError if @expand.parse_error?
    end
  end

  # url parameters for a single entity expand/select
  class UrlParameters4Single < UrlParametersBase
    def initialize(params)
      @params = params
      @expand = ExpandBase.factory(@params['$expand'])
      @select = SelectBase.factory(@params['$select'])
    end
  end

  # url parameters for a collection expand/select + filter/order
  class UrlParameters4Coll < UrlParametersBase
    attr_reader :filt
    attr_reader :ordby

    def initialize(model, params)
      # join helper is only needed for odering or filtering
      @jh = model.join_by_paths_helper if params['$orderby'] || params['$filter']
      @params = params
      @ordby = OrderBase.factory(@params['$orderby'], @jh)
      @filt = FilterBase.factory(@params['$filter'])
      @expand = ExpandBase.factory(@params['$expand'])
      @select = SelectBase.factory(@params['$select'])
    end

    def check_filter
      return BadRequestFilterParseError if @filt.parse_error?
    end

    def check_order
      return BadRequestOrderParseError if @ordby.parse_error?
    end

    def apply_to_dataset(dtcx)
      dtcx = apply_expand_to_dataset(dtcx)
      apply_filter_order_to_dataset(dtcx)
    end

    def apply_expand_to_dataset(dtcx)
      return dtcx if @expand.empty?

      @expand.apply_to_dataset(dtcx)
    end

    # Warning, the @ordby and @filt objects are coupled by way of the join helper
    def apply_filter_order_to_dataset(dtcx)
      return dtcx if @filt.empty? && @ordby.empty?

      # filter object and join-helper need to be finalized after filter has been parsed and checked
      @filt.finalize(@jh)

      # start with the join
      dtcx = @jh.dataset(dtcx)

      dtcx = @filt.apply_to_dataset(dtcx)
      dtcx = @ordby.apply_to_dataset(dtcx)

      dtcx.select_all(@jh.start_model.table_name)
    end
  end
end
