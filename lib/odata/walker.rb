require 'json'
require 'rexml/document'
require 'safrano.rb'

module OData
  # handle navigation in the Datamodel tree of entities/attributes
  # input is the url path. Url parameters ($filter etc...) are NOT handled here
  # This uses a state transition algorithm
  class Walker
    attr_accessor :contexts
    attr_accessor :context
    attr_accessor :end_context
    attr_reader   :path_start
    attr_accessor :path_remain
    attr_accessor :path_done
    attr_accessor :status
    attr_accessor :error

    # is $count requested?
    attr_accessor :do_count

    # is $value (of attribute) requested?
    attr_reader :raw_value

    # is $value (of media entity) requested?
    attr_reader :media_value

    # are $links requested ?
    attr_reader :do_links
    NIL_SERVICE_FATAL = 'Walker is called with a nil service'.freeze
    EMPTYSTR = ''.freeze
    SLASH = '/'.freeze
    def initialize(service, path, content_id_refs = nil)
      raise NIL_SERVICE_FATAL unless service

      path = URI.decode_www_form_component(path)
      @context = service
      @content_id_refs = content_id_refs

      @contexts = [@context]

      @path_start = @path_remain = if service
                                     unprefixed(service.xpath_prefix, path)
                                   else # This is for batch function

                                     path
                                   end
      @path_done = ''
      @status = :start
      @end_context = nil
      @do_count = nil
      eo
    end

    def unprefixed(prefix, path)
      if (prefix == EMPTYSTR) || (prefix == SLASH)
        path
      else
        #        path.sub!(/\A#{prefix}/, '')
        # TODO check
        path.sub(/\A#{prefix}/, EMPTYSTR)
      end
    end

    def get_next_transition
      # this does not work if there are multiple valid transitions
      # like when we have attributes that are substring of each other
      # --> instead of using detect (ie take first transition)
      # we need to use select and then find the longest match
      #        tr_next = @context.allowed_transitions.detect do |t|
      #          t.do_match(@path_remain)
      #        end

      valid_tr = @context.allowed_transitions.select do |t|
        t.do_match(@path_remain)
      end
      # this is a very fragile and obscure but required hack (wanted: a
      # better one) to make attributes that are substrings of each other
      # work well
      @tr_next = if valid_tr
                   if valid_tr.size == 1
                     valid_tr.first
                   elsif valid_tr.size > 1
                     valid_tr.max_by { |t| t.match_result[1].size }
                   end
                 end
    end

    # perform a content-id ($batch changeset ref) transition
    def do_run_with_content_id
      if @content_id_refs.is_a? Hash
        if (@context = @content_id_refs[@context.to_s])
          @status = :run
        else
          @context = nil
          @status = :error
          # TODO: more appropriate error handling
          @error = OData::ErrorNotFound
        end
      else
        @context = nil
        @status = :error
        # TODO: more appropriate error handling
        @error = OData::ErrorNotFound
      end
    end

    # little hacks... depending on returned state, set some attributes
    def state_mappings
      case @status
      when :end_with_count
        @do_count = true
        @status = :end
      when :end_with_value
        @raw_value = true
        @status = :end
      when :end_with_media_value
        @media_value = true
        @status = :end
      when :run_with_links
        @do_links = true
        @status = :run
      end
    end

    def do_next_transition
      @context, @status, @error = @tr_next.do_transition(@context)
      # little hack's
      case @status
        # we dont have the content-id references data on service level
        # but we have it here, so in case of a $content-id transition
        # the returned context is just the id, and we get the final result
        # entity reference here and place it in @context
      when :run_with_content_id
        do_run_with_content_id
      end

      @contexts << @context
      @path_remain = @tr_next.path_remain
      @path_done << @tr_next.path_done

      # little hack's
      state_mappings
    end

    def eo
      while @context
        get_next_transition
        if @tr_next
          do_next_transition
        else
          @context = nil
          @status = :error
          @error = OData::ErrorNotFound
        end
      end
      # TODO: shouldnt we raise an error here if @status != :end ?
      return false unless @status == :end

      @end_context = @contexts.size >= 2 ? @contexts[-2] : @contexts[1]
    end
  end
end
