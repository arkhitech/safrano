require 'json'
require 'rexml/document'
require_relative 'safrano/multipart.rb'
require_relative 'safrano/core.rb'
require_relative 'odata/entity.rb'
require_relative 'odata/attribute.rb'
require_relative 'odata/navigation_attribute.rb'
require_relative 'odata/collection.rb'
require_relative 'safrano/service.rb'
require_relative 'odata/walker.rb'
require 'sequel'
require_relative 'safrano/sequel_join_by_paths.rb'
require_relative 'safrano/rack_app'
require_relative 'safrano/odata_rack_builder'
require_relative 'safrano/version'

# picked from activsupport; needed for ruby < 2.5
# Destructively converts all keys using the +block+ operations.
# Same as +transform_keys+ but modifies +self+.
class Hash
  def transform_keys!
    keys.each do |key|
      self[yield(key)] = delete(key)
    end
    self
  end unless method_defined? :transform_keys!

  def symbolize_keys!
    transform_keys! { |key| key.to_sym rescue key }
  end
end

# needed for ruby < 2.5
class Dir
  def self.each_child(dir)
    Dir.foreach(dir) do |x|
      next if (x == '.') || (x == '..')

      yield x
    end
  end unless respond_to? :each_child
end
