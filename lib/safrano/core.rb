# our main namespace
module OData
  # frozen empty Array/Hash to reduce unncecessary object creation
  EMPTY_ARRAY = [].freeze
  EMPTY_HASH = {}.freeze
  EMPTY_HASH_IN_ARY = [EMPTY_HASH].freeze
  EMPTY_STRING = ''.freeze
  ARY_204_EMPTY_HASH_ARY = [204, EMPTY_HASH, EMPTY_ARRAY].freeze
  SPACE = ' '.freeze
  COMMA = ','.freeze

  # some prominent constants... probably already defined elsewhere eg in Rack
  # but lets KISS
  CONTENT_TYPE = 'Content-Type'.freeze
  CTT_TYPE_LC = 'content-type'.freeze
  TEXTPLAIN_UTF8 = 'text/plain;charset=utf-8'.freeze
  APPJSON = 'application/json'.freeze
  APPXML = 'application/xml'.freeze
  MP_MIXED = 'multipart/mixed'.freeze
  APPXML_UTF8 = 'application/xml;charset=utf-8'.freeze
  APPATOMXML_UTF8 = 'application/atomsvc+xml;charset=utf-8'.freeze
  APPJSON_UTF8 = 'application/json;charset=utf-8'.freeze

  CT_JSON = { CONTENT_TYPE => APPJSON_UTF8 }.freeze
  CT_TEXT = { CONTENT_TYPE => TEXTPLAIN_UTF8 }.freeze
  CT_ATOMXML = { CONTENT_TYPE => APPATOMXML_UTF8 }.freeze
  CT_APPXML = { CONTENT_TYPE => APPXML_UTF8 }.freeze

  # Type mapping DB --> Edm
  #  TypeMap = {"INTEGER" => "Edm.Int32" , "TEXT" => "Edm.String",
  # "STRING" => "Edm.String"}
  # Todo: complete mapping... this is just for the most common ones

  # TODO: use Sequel GENERIC_TYPES: -->
  # Constants
  # GENERIC_TYPES = %w'String Integer Float Numeric BigDecimal Date DateTime
  # Time File TrueClass FalseClass'.freeze
  # Classes specifying generic types that Sequel will convert to
  # database-specific types.
  DB_TYPE_STRING_RGX = /\ACHAR\s*\(\d+\)\z/.freeze

  # TODO... complete; used in $metadata
  def self.get_edm_type(db_type:)
    case db_type.upcase
    when 'INTEGER'
      'Edm.Int32'
    when 'NUMERIC'
      'Edm.Decimal'
    when 'BIGINT'
      'Edm.Int32'
    when 'TEXT', 'STRING', 'CHARACTER VARYING'
      'Edm.String'
    when 'TIMESTAMP WITHOUT TIME ZONE', 'TIMESTAMP(6) WITHOUT TIME ZONE'
      'Edm.DateTime'
    when 'BOOLEAN'
      'Edm.Boolean'
    when 'JSONB'
      'Edm.String'
    else
      'Edm.String' if DB_TYPE_STRING_RGX =~ db_type.upcase
    end
  end
end

module REXML
  # some small extensions
  class Document
    def to_pretty_xml
      formatter = REXML::Formatters::Pretty.new(2)
      formatter.compact = true
      strio = ''
      formatter.write(root, strio)
      strio
    end
  end
end

# Core
module Safrano
  # represents a state transition when navigating/parsing the url path
  # from left to right
  class Transition < Regexp
    attr_accessor :trans
    attr_accessor :match_result
    attr_accessor :rgx
    attr_reader :remain_idx
    def initialize(arg, trans: nil, remain_idx: 2)
      @rgx = if arg.respond_to? :each_char
               Regexp.new(arg)
             else
               arg
             end
      @trans = trans
      @remain_idx = remain_idx
    end

    def do_match(str)
      @match_result = @rgx.match(str)
    end

    # remain_idx is the index of the last match-data. ususally its 2
    # but can be overidden
    def path_remain
      @match_result[@remain_idx] if @match_result && @match_result[@remain_idx]
    end

    def path_done
      if @match_result
        @match_result[1] || ''
      else
        ''
      end
    end

    def do_transition(ctx)
      ctx.method(@trans).call(@match_result)
    end
  end

  TransitionEnd = Transition.new('\A(\/?)\z', trans: 'transition_end')
  TransitionMetadata = Transition.new('\A(\/\$metadata)(.*)',
                                      trans: 'transition_metadata')
  TransitionBatch = Transition.new('\A(\/\$batch)(.*)',
                                   trans: 'transition_batch')
  TransitionContentId = Transition.new('\A(\/\$(\d+))(.*)',
                                       trans: 'transition_content_id',
                                       remain_idx: 3)
  TransitionCount = Transition.new('(\A\/\$count)(.*)\z',
                                   trans: 'transition_count')
  TransitionValue = Transition.new('(\A\/\$value)(.*)\z',
                                   trans: 'transition_value')
  TransitionLinks = Transition.new('(\A\/\$links)(.*)\z',
                                   trans: 'transition_links')
  attr_accessor :allowed_transitions
end
