CRLF = "\r\n".freeze
LF = "\n".freeze

require 'securerandom'
require 'webrick/httpstatus'

# Simple multipart support for OData $batch purpose
module MIME
  CTT_TYPE_LC = 'content-type'.freeze
  TEXT_PLAIN = 'text/plain'.freeze

  # a mime object has a header(with content-type etc) and a content(aka body)
  class Media
    # Parser for MIME::Media
    class Parser
      HMD_RGX = /^([\w-]+)\s*:\s*(.*)/.freeze

      attr_accessor :lines
      attr_accessor :target
      def initialize
        @state = :h
        @lines = []
        @target_hd = {}
        @target_ct = nil
        @sep = CRLF
      end

      def addline(line)
        @lines << line
      end

      def parse_first_part(line)
        if @target.parser.next_part(line)
          @state = :next_part
        # this is for when there is only one part
        # (first part is the last one)
        elsif @target.parser.last_part(line)
          @state = :end
        else
          @target.parser.addline(line)
        end
      end

      def parse_next_part(line)
        if @target.parser.next_part(line)

        elsif @target.parser.last_part(line)
          @state = :end
        else
          @target.parser.addline(line)
        end
      end

      def parse_head(line)
        if (hmd = HMD_RGX.match(line))
          @target_hd[hmd[1].downcase] = hmd[2].strip

        elsif CRLF == line
          @target_ct = @target_hd[CTT_TYPE_LC] || TEXT_PLAIN
          @state = new_content

        end
      end

      def parse_line(line)
        case @state
        when :h
          parse_head(line)

        when :b
          @target.parser.addline(line)

        when :bmp
          @state = :first_part if @target.parser.first_part(line)

        when :first_part
          parse_first_part(line)

        when :next_part
          parse_next_part(line)
        end
      end

      def parse(level: 0)
        @level = level
        return unless @lines

        @lines.each do |line|
          parse_line(line)
        end
        # Warning: recursive here
        @target.parser.parse(level: level)
        @target.parser = nil
        @lines = nil
        @target
      end

      def multipart_content(boundary)
        MIME::Content::Multipart::Base.new(boundary)
      end

      def hook_multipart(content_type, boundary)
        @target_hd[CTT_TYPE_LC] = content_type
        @target_ct = @target_hd[CTT_TYPE_LC]
        @target = multipart_content(boundary)
        @target.hd = @target_hd
        @target.ct = @target_ct
        @state = :bmp
      end
      MPS = 'multipart/'.freeze
      MP_RGX1 = %r{^(digest|mixed);\s*boundary=\"(.*)\"}.freeze
      MP_RGX2 = %r{^(digest|mixed);\s*boundary=(.*)}.freeze
      #      APP_HTTP_RGX = %r{^application/http}.freeze
      APP_HTTP = 'application/http'.freeze
      def new_content
        @target =
          if @target_ct.start_with?(MPS) &&
             (md = (MP_RGX1.match(@target_ct[10..-1]) || MP_RGX2.match(@target_ct[10..-1])))
            multipart_content(md[2].strip)
          elsif @target_ct.start_with?(APP_HTTP)
            MIME::Content::Application::Http.new
          else
            MIME::Content::Text::Plain.new
          end
        @target.hd.merge! @target_hd
        @target.ct = @target_ct
        @target.level = @level
        @state = @target.is_a?(MIME::Content::Multipart::Base) ? :bmp : :b
      end

      def parse_lines(inp, level: 0)
        @lines = inp
        parse(level: level)
      end

      def parse_str(inpstr, level: 0)
        # we need to keep the line separators --> use io.readlines
        if inpstr.respond_to?(:readlines)
          @lines = inpstr.readlines(@sep)
        else
          # rack input wrapper only has gets but not readlines
          sepsave = $INPUT_RECORD_SEPARATOR
          $INPUT_RECORD_SEPARATOR = @sep
          while (line = inpstr.gets)
            @lines << line
          end
          $INPUT_RECORD_SEPARATOR = sepsave

        end
        # tmp hack for test-tools that convert CRLF in payload to LF :-(
        if @lines.size == 1
          @sep = LF
          @lines = @lines.first.split(LF).map { |xline| "#{xline}#{CRLF}" }
        end
        parse(level: level)
      end

      def parse_string(inpstr, level: 0)
        @lines = inpstr.split(@sep)
        if @lines.size == 1
          @sep = LF
          @lines = @lines.first.split(LF)
        end
        # split is not keeping the separator, we re-add it
        @lines = @lines.map { |line| "#{line}#{CRLF}" }
        parse(level: level)
      end
    end

    attr_accessor :content
    attr_accessor :hd
    attr_accessor :ct
    attr_accessor :level
    attr_reader :response

    def initialize
      @state = :h
      @hd = {}
      @ct = nil
      @parser = Parser.new(self)
    end
  end

  # Subclasses for Mime media content
  module Content
    # due to the recursive nature of multipart media, a media-content
    # should be seen as a full-fledged media(header+content) as well
    class Media < ::MIME::Media
      attr_accessor :parser
    end

    # text mime types
    module Text
      # Well, it says plain text
      class Plain < Media
        # Parser for Text::Plain
        class Parser
          HMD_RGX = /^([\w-]+)\s*:\s*(.*)/.freeze
          def initialize(target)
            @state = :h
            @lines = []
            @target = target
          end

          def addline(line)
            @lines << line
          end

          def parse_head(line)
            if (hmd = HMD_RGX.match(line))
              @target.hd[hmd[1].downcase] = hmd[2].strip
            elsif CRLF == line
              @state = :b
            else
              @target.content << line
              @state = :b
            end
          end

          def parse(level: 0)
            return unless @lines

            @level = level
            @lines.each do |line|
              case @state
              when :h
                parse_head(line)
              when :b
                @target.content << line
              end
            end
            @lines = nil
            @target.level = level
            @target
          end
        end

        def initialize
          @hd = {}
          @content = ''
          # set default values. Can be overwritten by parser
          @hd[CTT_TYPE_LC] = TEXT_PLAIN
          @ct = TEXT_PLAIN
          @parser = Parser.new(self)
        end

        def unparse
          @content
        end

        def ==(other)
          (@hd == other.hd) && (@content == other.content)
        end
      end
    end

    # Multipart media
    module Multipart
      # base class for multipart mixed, related, digest etc
      class Base < Media
        # Parser for Multipart Base class
        class Parser
          CRLF_ENDING_RGX = /#{CRLF}$/.freeze
          def initialize(target)
            @body_lines = []
            @target = target
            @parts = []
            @bound_rgx = /^--#{@target.boundary}\s*$/
            @lastb_rgx = /^--#{@target.boundary}--\s*$/
          end

          def parse(level: 0)
            @target.content = @parts.map do |mlines|
              MIME::Media::Parser.new.parse_lines(mlines, level: level + 1)
            end
            @body_lines = @bound_rgx = @lastb_rgx = nil
            @parts = nil
            @target.level = level
            @target
          end

          def first_part(line)
            return unless @bound_rgx =~ line

            @body_lines = []
          end

          def next_part(line)
            return unless @bound_rgx =~ line

            collect_part
            @body_lines = []
          end

          def last_part(line)
            return unless @lastb_rgx =~ line

            collect_part
          end

          def collect_part
            # according to the multipart RFC spec, the preceeding CRLF
            # belongs to the boundary
            # but because we are a CRLF line-based parser we need
            # to remove it from the end of the last body line
            return unless @body_lines

            #            @body_lines.last.sub!(CRLF_ENDING_RGX, '')
            @body_lines.last.chomp!(CRLF)
            @parts << @body_lines
          end

          def addline(line)
            @body_lines << line
          end
        end

        attr_reader :boundary

        def initialize(boundary)
          @boundary = boundary
          @hd = {}
          @content_id_references = nil
          @parser = Parser.new(self)
        end

        def ==(other)
          (@boundary == other.boundary) && (@content == other.content)
        end

        def each_changeset
          return unless @level.zero?

          @content.each do |part|
            yield part if part.is_a? MIME::Content::Multipart::Base
          end
        end

        def prepare_content_id_refs
          if @level.zero?
            each_changeset(&:prepare_content_id_refs)
          elsif @level == 1

            @content_id_references = {}

            @content.each do |part|
              next unless part.is_a? MIME::Content::Application::Http

              case part.content.http_method
              when 'POST', 'PUT'
                if (ctid = part.hd['content-id'])
                  part.content.content_id = ctid
                  @content_id_references[ctid] = nil
                end
              end
              part.content.content_id_references = @content_id_references
            end

          end
        end

        def set_multipart_header
          @hd[CTT_TYPE_LC] = "#{OData::MP_MIXED}; boundary=#{@boundary}"
        end

        def get_http_resp(batcha)
          get_response(batcha)
          [@response.hd, @response.unparse(true)]
        end

        def get_response(batcha)
          @response = self.class.new(::SecureRandom.uuid)
          @response.set_multipart_header
          if @level == 1 # changeset need their own global transaction
            # the change requests that are part of it have @level==2
            # and will be flagged with in_changeset=true
            # and this will finally be used to skip the transaction
            # of the changes
            batcha.db.transaction do
              begin
                @response.content = @content.map { |part| part.get_response(batcha) }
              rescue Sequel::Rollback => e
                # one of the changes of the changeset has failed
                # --> provide a dummy empty response for the change-parts
                # then transmit the Rollback to Sequel
                get_failed_changeset_response(e)
                raise
              end
            end
          else
            @response.content = @content.map { |prt| prt.get_response(batcha) }
          end
          @response
        end

        def get_failed_changeset_response(_xeption)
          @response = MIME::Content::Application::HttpResp.new
          @response.status = '400'
          @response.content = [{ 'odata.error' =>
                               { 'message' =>
                                 'Bad Request: Failed changeset ' } }.to_json]
          @response.hd = OData::CT_JSON
          @response
        end

        def unparse(bodyonly = false)
          b = ''
          unless bodyonly
            #            b << OData::CONTENT_TYPE << ': ' << @hd[OData::CTT_TYPE_LC] << CRLF
            b << "#{OData::CONTENT_TYPE}: #{@hd[CTT_TYPE_LC]}#{CRLF}"
          end

          b << crbdcr = "#{CRLF}--#{@boundary}#{CRLF}"
          b << @content.map(&:unparse).join(crbdcr)
          b << "#{CRLF}--#{@boundary}--#{CRLF}"
          b
        end
      end
    end

    # for application(http etc.) mime types
    module Application
      # Http Request
      class HttpReq < Media
        attr_accessor :http_method
        attr_accessor :uri

        # for changeset content-id refs
        attr_accessor :content_id
        attr_accessor :content_id_references

        def initialize
          @hd = {}
          @content = ''
        end

        def unparse
          b = "#{@http_method} #{@uri} HTTP/1.1#{CRLF}"
          @hd.each { |k, v| b << "#{k}: #{v}#{CRLF}" }
          b << CRLF
          b << @content if @content != ''
          b
        end
      end

      # Http Response
      class HttpResp < Media
        attr_accessor :status
        attr_accessor :content

        APPLICATION_HTTP_11 = ['Content-Type: application/http',
                               "Content-Transfer-Encoding: binary#{CRLF}",
                               'HTTP/1.1 '].join(CRLF).freeze

        StatusMessage = ::WEBrick::HTTPStatus::StatusMessage.freeze

        def initialize
          @hd = {}
          @content = []
        end

        def ==(other)
          (@hd == other.hd) && (@content == other.content)
        end

        def unparse
          b = String.new(APPLICATION_HTTP_11)
          b << "#{@status} #{StatusMessage[@status]} #{CRLF}"
          @hd.each { |k, v| b << "#{k}: #{v}#{CRLF}" }
          b << CRLF
          b << @content.join if @content
          b
        end
      end

      # For application/http . Content is either a Request or a Response
      class Http < Media
        HTTP_MTHS_RGX = 'POST|GET|PUT|MERGE|PATCH|DELETE'.freeze
        HTTP_R_RGX = %r{^(#{HTTP_MTHS_RGX})\s+(\S*)\s?(HTTP/1\.1)\s*$}.freeze
        HEADER_RGX = %r{^([a-zA-Z\-]+):\s*([0-9a-zA-Z\-\/,\s]+;?\S*)\s*$}.freeze
        HTTP_RESP_RGX = %r{^HTTP/1\.1\s(\d+)\s}.freeze

        # Parser for Http Media
        class Parser
          HMD_RGX = /^([\w-]+)\s*:\s*(.*)/.freeze

          def initialize(target)
            @state = :http
            @lines = []
            @body_lines = []
            @target = target
          end

          def addline(line)
            @lines << line
          end

          def parse_http(line)
            if (hmd = HMD_RGX.match(line))
              @target.hd[hmd[1].downcase] = hmd[2].strip
            elsif (mdht = HTTP_R_RGX.match(line))
              @state = :hd
              @target.content = MIME::Content::Application::HttpReq.new
              @target.content.http_method = mdht[1]
              @target.content.uri = mdht[2]
            #  HTTP 1.1 status line  --> HttpResp.new
            elsif (mdht = HTTP_RESP_RGX.match(line))
              @state = :hd
              @target.content = MIME::Content::Application::HttpResp.new
              @target.content.status = mdht[1]
            end
          end

          def parse_head(line)
            if (hmd = HMD_RGX.match(line))
              @target.content.hd[hmd[1].downcase] = hmd[2].strip
            elsif CRLF == line
              @state = :b
            else
              @body_lines << line
              @state = :b
            end
          end

          def parse(level: 0)
            return unless @lines

            @lines.each do |line|
              case @state
              when :http
                parse_http(line)
              when :hd
                parse_head(line)
              when :b
                @body_lines << line
              end
            end

            @target.content.content = @body_lines.join
            @target.content.level = level
            @target.level = level
            @lines = nil
            @body_lines = nil
            @target
          end
        end

        def initialize
          @hd = {}
          @ct = 'application/http'
          @parser = Parser.new(self)
        end

        def ==(other)
          @content = other.content
        end

        def get_response(batchapp)
          # self.content should be the request
          rack_resp = batchapp.batch_call(@content)
          @response = MIME::Content::Application::HttpResp.new
          @response.status = rack_resp[0]
          @response.hd = rack_resp[1]
          @response.content = rack_resp[2]
          @response
        end

        def unparse
          b =  "Content-Type: #{@ct}#{CRLF}"
          b << "Content-Transfer-Encoding: binary#{CRLF}#{CRLF}"
          b << @content.unparse
          b
        end
      end
    end
  end
end
