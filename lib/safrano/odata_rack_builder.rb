require 'rack'
require 'rack/cors'

module Rack
  module OData
    # just a Wrapper to ensure (force?) that mandatory middlewares are acutally
    # used
    class Builder < ::Rack::Builder
      def initialize(default_app = nil, &block)
        super(default_app) {}
        use ::Rack::Cors
        instance_eval(&block) if block_given?
        use ::Rack::ContentLength
      end
    end
  end
end
