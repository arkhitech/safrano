#!/usr/bin/env ruby

require 'rack'
require_relative '../odata/walker.rb'
require_relative 'request.rb'
require_relative 'response.rb'

module OData
  # handle GET PUT etc
  module MethodHandlers
    def odata_options
      # cf. stackoverflow.com/questions/22924678/sinatra-delete-response-headers

      x = if @walker.status == :end
            headers.delete('Content-Type')
            @response.headers.delete('Content-Type')
            [200, EMPTY_HASH, '']
          else
            odata_error
          end
      @response.headers['Content-Type'] = ''
      x
    end

    def odata_error
      return @walker.error.odata_get(@request) unless @walker.error.nil?

      # this is too critical; raise a real Exception
      raise 'Walker construction failed with a unknown Error '
    end

    def odata_delete
      if @walker.status == :end
        @walker.end_context.odata_delete(@request)
      else
        odata_error
      end
    end

    def odata_put
      if @walker.status == :end
        @walker.end_context.odata_put(@request)
      else
        odata_error
      end
    end

    def odata_patch
      if @walker.status == :end
        @walker.end_context.odata_patch(@request)
      else
        odata_error
      end
    end

    def odata_get
      if @walker.status == :end
        @walker.end_context.odata_get(@request)
      else
        odata_error
      end
    end

    def odata_post
      if @walker.status == :end
        @walker.end_context.odata_post(@request)
      else
        odata_error
      end
    end

    def odata_head
      [200, EMPTY_HASH, [EMPTY_STRING]]
    end
  end

  # the main Rack server app. Source: the Rack docu/examples and partly
  # inspired from Sinatra
  class ServerApp
    METHODS_REGEXP = Regexp.new('HEAD|OPTIONS|GET|POST|PATCH|MERGE|PUT|DELETE').freeze
    NOCACHE_HDRS = { 'Cache-Control' => 'no-cache',
                     'Expires' => '-1',
                     'Pragma' => 'no-cache' }.freeze
    DATASERVICEVERSION = 'DataServiceVersion'.freeze
    include MethodHandlers
    def before
      @request.service_base = self.class.get_service_base

      neg_error = @request.negotiate_service_version

      raise RuntimeError if neg_error

      return false unless @request.service

      myhdrs = NOCACHE_HDRS.dup
      myhdrs[DATASERVICEVERSION] = @request.service.data_service_version
      headers myhdrs
    end

    # dispatch for all methods requiring parsing of the path
    # with walker (ie. allmost all excepted HEAD)
    def dispatch_with_walker
      @walker = @request.create_odata_walker
      case @request.request_method
      when 'GET'
        odata_get
      when 'POST'
        odata_post
      when 'DELETE'
        odata_delete
      when 'OPTIONS'
        odata_options
      when 'PUT'
        odata_put
      when 'PATCH', 'MERGE'
        odata_patch
      else
        raise Error
      end
    end

    def dispatch
      req_ret = if @request.request_method !~ METHODS_REGEXP
                  [404, EMPTY_HASH, ['Did you get lost?']]
                elsif @request.request_method == 'HEAD'
                  odata_head
                else
                  dispatch_with_walker
                end
      @response.status, rsph, @response.body = req_ret
      headers rsph
    end

    def call(env)
      @request = OData::Request.new(env)
      @response = OData::Response.new

      before

      dispatch

      @response.finish
    end

    # Set multiple response headers with Hash.
    def headers(hash = nil)
      @response.headers.merge! hash if hash
      @response.headers
    end

    def self.enable_batch
      @service_base.enable_batch
    end

    def self.path_prefix(path_pr)
      @service_base.path_prefix path_pr
    end

    def self.get_service_base
      @service_base
    end

    def self.set_servicebase(sbase)
      @service_base = sbase
      @service_base.enable_v1_service
      @service_base.enable_v2_service
    end

    def self.publish_service(&block)
      sbase = OData::ServiceBase.new
      sbase.instance_eval(&block) if block_given?
      sbase.finalize_publishing
      set_servicebase(sbase)
    end
  end
end
