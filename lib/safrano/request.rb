require 'rack'
require 'rfc2047'

module OData
  # monkey patch deactivate Rack/multipart because it does not work on simple
  # OData $batch requests when the content-length
  # is not passed
  class Request < Rack::Request
    HEADER_PARAM = /\s*[\w.]+=(?:[\w.]+|"(?:[^"\\]|\\.)*")?\s*/.freeze
    HEADER_VAL_RAW = '(?:\w+|\*)\/(?:\w+(?:\.|\-|\+)?|\*)*'.freeze
    HEADER_VAL_WITH_PAR = /(?:#{HEADER_VAL_RAW})\s*(?:;#{HEADER_PARAM})*/.freeze
    ON_CGST_ERROR = (proc { |r| raise(Sequel::Rollback) if r.in_changeset })

    # borowed from Sinatra
    class AcceptEntry
      attr_accessor :params
      attr_reader :entry
      def initialize(entry)
        params = entry.scan(HEADER_PARAM).map! do |s|
          key, value = s.strip.split('=', 2)
          value = value[1..-2].gsub(/\\(.)/, '\1') if value.start_with?('"')
          [key, value]
        end

        @entry = entry
        @type = entry[/[^;]+/].delete(' ')
        @params = Hash[params]
        @q = @params.delete('q') { 1.0 }.to_f
      end

      def method_missing(*args, &block)
        to_str.send(*args, &block)
      end

      def <=>(other)
        other.priority <=> priority
      end

      def priority
        # We sort in descending order; better matches should be higher.
        [@q, -@type.count('*'), @params.size]
      end

      def respond_to?(*args)
        super || to_str.respond_to?(*args)
      end

      def to_s(full = false)
        full ? entry : to_str
      end

      def to_str
        @type
      end
    end
    ## original coding:
    #    # The set of media-types. Requests that do not indicate
    #    # one of the media types presents in this list will not be eligible
    #    # for param parsing like soap attachments or generic multiparts
    #    PARSEABLE_DATA_MEDIA_TYPES = [
    #      'multipart/related',
    #      'multipart/mixed'
    #    ]
    #    # Determine whether the request body contains data by checking
    #    # the request media_type against registered parse-data media-types
    #    def parseable_data?
    #      PARSEABLE_DATA_MEDIA_TYPES.include?(media_type)
    #    end
    def parseable_data?
      false
    end

    # OData extension
    attr_accessor :service_base
    attr_accessor :service
    attr_accessor :walker

    # request is part of a $batch changeset
    attr_accessor :in_changeset

    # content_id of request in a $batch changeset
    attr_accessor :content_id

    # content-id references map
    attr_accessor :content_id_references

    # stores the newly created entity for the current content-id of
    # the processed request
    def register_content_id_ref(new_entity)
      return unless @in_changeset
      return unless @content_id

      @content_id_references[@content_id] = new_entity
    end

    def create_odata_walker
      @env['safrano.walker'] = @walker = Walker.new(@service, path_info, @content_id_references)
    end

    def accept
      @env['safrano.accept'] ||= begin
        if @env.include?('HTTP_ACCEPT') && (@env['HTTP_ACCEPT'].to_s != '')
          @env['HTTP_ACCEPT'].to_s.scan(HEADER_VAL_WITH_PAR)
                             .map! { |e| AcceptEntry.new(e) }.sort
        else
          [AcceptEntry.new('*/*')]
        end
      end
    end

    def accept?(type)
      preferred_type(type).to_s.include?(type)
    end

    def preferred_type(*types)
      accepts = accept # just evaluate once
      return accepts.first if types.empty?

      types.flatten!
      return types.first if accepts.empty?

      accepts.detect do |pattern|
        type = types.detect { |t| File.fnmatch(pattern, t) }
        return type if type
      end
    end

    def with_media_data
      if (filename = @env['HTTP_SLUG'])

        yield @env['rack.input'],
               content_type.split(';').first,
               Rfc2047.decode(filename)

      else
        ON_CGST_ERROR.call(self)
        [400, EMPTY_HASH, ['File upload error: Missing SLUG']]
      end
    end

    def with_parsed_data
      if content_type == APPJSON
        # Parse json payload
        begin
          data = JSON.parse(body.read)
        rescue JSON::ParserError => e
          ON_CGST_ERROR.call(self)
          return [400, EMPTY_HASH, ['JSON Parser Error while parsing payload : ',
                                    e.message]]
        end

        yield data

      else # TODO: other formats

        [415, EMPTY_HASH, EMPTY_ARRAY]
      end
    end

    def negotiate_service_version
      maxv = if (rqv = env['HTTP_MAXDATASERVICEVERSION'])
               OData::ServiceBase.parse_data_service_version(rqv)
             else
               OData::MAX_DATASERVICE_VERSION
             end
      return OData::BadRequestError if maxv.nil?
      # client request an too old version --> 501
      return OData::NotImplementedError if maxv < OData::MIN_DATASERVICE_VERSION

      minv = if (rqv = env['HTTP_MINDATASERVICEVERSION'])
               OData::ServiceBase.parse_data_service_version(rqv)
             else
               OData::MIN_DATASERVICE_VERSION
             end
      return OData::BadRequestError if minv.nil?
      # client request an too new version --> 501
      return OData::NotImplementedError if minv > OData::MAX_DATASERVICE_VERSION
      return OData::BadRequestError if minv > maxv

      v = if (rqv = env['HTTP_DATASERVICEVERSION'])
            OData::ServiceBase.parse_data_service_version(rqv)
          else
            OData::MAX_DATASERVICE_VERSION
          end

      return OData::BadRequestError if v.nil?
      return OData::NotImplementedError if v > OData::MAX_DATASERVICE_VERSION
      return OData::NotImplementedError if v < OData::MIN_DATASERVICE_VERSION

      @service = nil
      @service = case maxv
                 when '1'
                   @service_base.v1
                 when '2', '3', '4'
                   @service_base.v2
                 end
      nil
    end
  end
end
