require 'rack'

# monkey patch deactivate Rack/multipart because it does not work on simple
# OData $batch requests when the content-length is not passed
module OData
  # borrowed fro Sinatra
  # The response object. See Rack::Response and Rack::Response::Helpers for
  # more info:
  # http://rubydoc.info/github/rack/rack/master/Rack/Response
  # http://rubydoc.info/github/rack/rack/master/Rack/Response/Helpers
  class Response < ::Rack::Response
    DROP_BODY_RESPONSES = [204, 205, 304].freeze
    def initialize(*)
      super
      headers['Content-Type'] ||= 'text/html'
    end

    def body=(value)
      value = value.body while ::Rack::Response === value
      @body = String === value ? [value.to_str] : value
    end

    def each
      block_given? ? super : enum_for(:each)
    end

    def finish
      result = body

      if drop_content_info?
        headers.delete 'Content-Length'
        headers.delete 'Content-Type'
      end

      if drop_body?
        close
        result = EMPTY_ARRAY
      end

      if calculate_content_length?
        # if some other code has already set Content-Length, don't muck with it
        # currently, this would be the static file-handler
        headers['Content-Length'] = calculated_content_length.to_s
      end

      [status.to_i, headers, result]
    end

    private

    def calculate_content_length?
      headers['Content-Type'] && !headers['Content-Length'] && (Array === body)
    end

    def calculated_content_length
      body.inject(0) { |l, p|  l + p.bytesize }
    end

    def drop_content_info?
      (status.to_i / 100 == 1) || drop_body?
    end

    def drop_body?
      DROP_BODY_RESPONSES.include?(status.to_i)
    end
  end
end
