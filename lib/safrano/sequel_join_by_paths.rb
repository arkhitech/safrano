#!/usr/bin/env ruby

require_relative '../sequel/plugins/join_by_paths.rb'

Sequel::Model.plugin Sequel::Plugins::JoinByPaths
