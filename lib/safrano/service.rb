require 'rexml/document'
require 'odata/relations.rb'
require 'odata/batch.rb'
require 'odata/error.rb'
require 'odata/filter/sequel.rb'

module OData
  # this module has all methods related to expand/defered output preparation
  # and will be included in Service class
  module ExpandHandler
    PATH_SPLITTER = %r{\A(\w+)\/?(.*)\z}.freeze
    DEFERRED = '__deferred'.freeze
    URI = 'uri'.freeze

    def get_deferred_odata_h(entity_uri:, attrib:)
      { DEFERRED => { URI => "#{entity_uri}/#{attrib}" } }
    end

    # split expand argument into first and rest part
    def split_entity_expand_arg(exp_one)
      if exp_one.include?('/')
        m = PATH_SPLITTER.match(exp_one)
        cur_exp = m[1].strip
        rest_exp = m[2]
        # TODO: check errorhandling
        raise OData::ServerError if cur_exp.nil?

        k_s = cur_exp

      else
        k_s = exp_one.strip
        rest_exp = nil
      end
      k = k_s.to_sym
      yield k, k_s, rest_exp
    end

    # default v2
    # overriden in ServiceV1
    RESULTS_K = 'results'.freeze
    COUNT_K = '__count'.freeze
    def get_coll_odata_h(array:, template:, icount: nil)
      array.map! do |w|
        get_entity_odata_h(entity: w, template: template)
      end
      if icount
        { RESULTS_K => array, COUNT_K => icount }
      else
        { RESULTS_K => array }
      end
    end

    # handle $links ... Note: $expand seems to be ignored when $links
    # are  requested
    def get_entity_odata_link_h(entity:)
      { uri: entity.uri }
    end

    EMPTYH = {}.freeze
    METADATA_K = '__metadata'.freeze
    def get_entity_odata_h(entity:, template:)
      # start with metadata
      hres = { METADATA_K => entity.metadata_h(xnamespace) }

      template.each do |elmt, arg|
        case elmt
        when :all_values
          hres.merge! entity.casted_values
        when :selected_vals
          hres.merge! entity.casted_values(arg)
        when :expand_e

          arg.each do |attr, templ|
            enval = entity.send(attr)
            hres[attr] = if enval
                           get_entity_odata_h(entity: enval, template: templ)
                         else
                           # FK is NULL --> nav_value is nil --> return empty json
                           EMPTYH
                         end
          end
        when :expand_c
          arg.each do |attr, templ|
            next unless  (encoll = entity.send(attr))

            #  nav attributes that are a collection (x..n)
            hres[attr] = get_coll_odata_h(array: encoll, template: templ)
            # else error ?
          end
        when :deferr
          euri = entity.uri
          arg.each do |attr|
            hres[attr] = get_deferred_odata_h(entity_uri: euri, attrib: attr)
          end
        end
      end
      hres
    end
  end
end

module OData
  # xml namespace constants needed for the output of XML service and
  # and metadata
  module XMLNS
    MSFT_ADO = 'http://schemas.microsoft.com/ado'.freeze
    MSFT_ADO_2009_EDM = "#{MSFT_ADO}/2009/11/edm".freeze
    MSFT_ADO_2007_EDMX = "#{MSFT_ADO}/2007/06/edmx".freeze
    MSFT_ADO_2007_META = MSFT_ADO + \
                         '/2007/08/dataservices/metadata'.freeze

    W3_2005_ATOM = 'http://www.w3.org/2005/Atom'.freeze
    W3_2007_APP = 'http://www.w3.org/2007/app'.freeze
  end
end

# Link to Model
module OData
  MAX_DATASERVICE_VERSION = '2'.freeze
  MIN_DATASERVICE_VERSION = '1'.freeze
  include XMLNS
  # Base class for service. Subclass will be for V1, V2 etc...
  class ServiceBase
    include Safrano
    include ExpandHandler

    XML_PREAMBLE = '<?xml version="1.0" encoding="utf-8" standalone="yes"?>' + \
                   "\r\n".freeze

    # This is just a hash of entity Set Names to the corresponding Class
    # Example
    # Book < Sequel::Model(:book)
    #    @entity_set_name = 'books'
    # end
    # ---> @cmap ends up as   {'books' => Book }
    attr_reader :cmap

    # this is just the *sorted* list of the entity classes
    # (ie... @cmap.values.sorted)
    attr_accessor :collections

    attr_accessor :xtitle
    attr_accessor :xname
    attr_accessor :xnamespace
    attr_accessor :xpath_prefix
    attr_accessor :xserver_url
    attr_accessor :uribase
    attr_accessor :meta
    attr_accessor :batch_handler
    attr_accessor :relman
    # Instance attributes for specialized Version specific Instances
    attr_accessor :v1
    attr_accessor :v2

    # TODO: more elegant design
    attr_reader :data_service_version

    def initialize(&block)
      # Warning: if you add attributes here, you shall need add them
      # in copy_attribs_to as well
      #  because of the version subclasses that dont use "super" initialise
      # (todo: why not??)
      @meta = ServiceMeta.new(self)
      @batch_handler = OData::Batch::DisabledHandler.new
      @relman = OData::RelationManager.new
      @cmap = {}
      instance_eval(&block) if block_given?
    end

    DATASERVICEVERSION_RGX = /\A([1234])(?:\.0);*\w*\z/.freeze
    TRAILING_SLASH = %r{/\z}.freeze
    DEFAULT_PATH_PREFIX = '/'.freeze
    DEFAULT_SERVER_URL = 'http://localhost:9494'

    # input is the       DataServiceVersion request header string, eg.
    # '2.0;blabla' ---> Version -> 2
    def self.parse_data_service_version(inp)
      m = DATASERVICEVERSION_RGX.match(inp)
      m[1] if m
    end

    def enable_batch
      @batch_handler = OData::Batch::EnabledHandler.new
      (@v1.batch_handler = @batch_handler) if @v1
      (@v2.batch_handler = @batch_handler) if @v2
    end

    def enable_v1_service
      @v1 = OData::ServiceV1.new
      copy_attribs_to @v1
    end

    def enable_v2_service
      @v2 = OData::ServiceV2.new
      copy_attribs_to @v2
    end

    # public API
    def name(nam)
      @xname = nam
    end

    def namespace(namsp)
      @xnamespace = namsp
    end

    def title(tit)
      @xtitle = tit
    end

    def path_prefix(path_pr)
      @xpath_prefix = path_pr.sub(TRAILING_SLASH, '')
      (@v1.xpath_prefix = @xpath_prefix) if @v1
      (@v2.xpath_prefix = @xpath_prefix) if @v2
    end

    def server_url(surl)
      @xserver_url = surl.sub(TRAILING_SLASH, '')
      (@v1.xserver_url = @xserver_url) if @v1
      (@v2.xserver_url = @xserver_url) if @v2
    end

    # end public API

    def set_uribase
      @uribase = if @xpath_prefix.empty?
                   @xserver_url
                 else
                   if @xpath_prefix[0] == '/'
                     @xserver_url + @xpath_prefix
                   else
                     @xserver_url + '/' + @xpath_prefix
                   end
                 end
      (@v1.uribase = @uribase) if @v1
      (@v2.uribase = @uribase) if @v2
    end

    def copy_attribs_to(other)
      other.cmap = @cmap
      other.collections = @collections
      other.allowed_transitions = @allowed_transitions
      other.xtitle = @xtitle
      other.xname = @xname
      other.xnamespace = @xnamespace
      other.xpath_prefix = @xpath_prefix
      other.xserver_url = @xserver_url
      other.uribase = @uribase
      other.meta = ServiceMeta.new(other) # hum ... #todo: versions as well ?
      other.relman = @relman
      other.batch_handler = @batch_handler
      other
    end

    # this is a central place. We extend Sequel models with OData functionality
    # The included/extended modules depends on the properties(eg, pks, field types) of the model
    # we differentiate
    #  * Single/Multi PK
    #  * Media/Non-Media entity
    # Putting this logic here in modules loaded once on start shall result in less runtime overhead
    def register_model(modelklass, entity_set_name = nil, is_media = false)
      # check that the provided klass is a Sequel Model

      raise(OData::API::ModelNameError, modelklass) unless modelklass.is_a? Sequel::Model::ClassMethods

      if modelklass.ancestors.include? OData::Entity
        # modules were already added previously;
        # cleanup state to avoid having data from previous calls
        # mostly usefull for testing (eg API)
        modelklass.reset
      elsif modelklass.primary_key.is_a?(Array) # first API call... (normal non-testing case)
        modelklass.extend OData::EntityClassMultiPK
        modelklass.include OData::EntityMultiPK
      else
        modelklass.extend OData::EntityClassSinglePK
        modelklass.include OData::EntitySinglePK
      end
      # Media/Non-media
      if is_media
        modelklass.extend OData::EntityClassMedia
        # set default media handler . Can be overridden later with the
        #  "use HandlerKlass, options" API

        modelklass.set_default_media_handler
        modelklass.api_check_media_fields
        modelklass.include OData::MediaEntity
      else
        modelklass.extend OData::EntityClassNonMedia
        modelklass.include OData::NonMediaEntity
      end

      modelklass.prepare_pk
      modelklass.prepare_fields
      esname = (entity_set_name || modelklass).to_s.freeze
      modelklass.instance_eval { @entity_set_name = esname }
      @cmap[esname] = modelklass
      set_collections_sorted(@cmap.values)
    end

    def publish_model(modelklass, entity_set_name = nil, &block)
      register_model(modelklass, entity_set_name)
      # we need to execute the passed block in a deferred step
      # after all models have been registered (due to rel. dependancies)
      #      modelklass.instance_eval(&block) if block_given?
      modelklass.deferred_iblock = block if block_given?
    end

    def publish_media_model(modelklass, entity_set_name = nil, &block)
      register_model(modelklass, entity_set_name, true)
      # we need to execute the passed block in a deferred step
      # after all models have been registered (due to rel. dependancies)
      #      modelklass.instance_eval(&block) if block_given?
      modelklass.deferred_iblock = block if block_given?
    end

    def cmap=(imap)
      @cmap = imap
      set_collections_sorted(@cmap.values)
    end

    # take care of sorting required to match longest first while parsing
    # with     base_url_regexp
    # example: CrewMember must be matched before Crew otherwise we get error
    def set_collections_sorted(coll_data)
      @collections = coll_data
      @collections.sort_by! { |klass| klass.entity_set_name.size }.reverse! if @collections
      @collections
    end

    # to be called at end of publishing block to ensure we get the right names
    # and additionally build the list of valid attribute path's used
    # for validation of $orderby or $filter params

    def finalize_publishing
      # build the cmap
      @cmap = {}
      @collections.each do |klass|
        @cmap[klass.entity_set_name] = klass
      end

      # now that we know all model klasses we can handle relationships
      execute_deferred_iblocks

      # set default path prefix if path_prefix was not called
      path_prefix(DEFAULT_PATH_PREFIX) unless @xpath_prefix

      # set default server url if server_url was not called
      server_url(DEFAULT_SERVER_URL) unless @xserver_url

      set_uribase

      @collections.each(&:finalize_publishing)

      # finalize the uri's and include NoMappingBeforeOutput or MappingBeforeOutput as needed
      @collections.each { |klass|
        klass.build_uri(@uribase)
        if klass.time_cols.empty?
          klass.include OData::NoMappingBeforeOutput
        else
          klass.include OData::MappingBeforeOutput
        end
      }

      # build allowed transitions (requires that @collections are filled and sorted for having a
      # correct base_url_regexp)
      build_allowed_transitions

      # mixin adapter specific modules where needed
      case Sequel::Model.db.adapter_scheme
      when :postgres
        OData::Filter::FuncTree.include OData::Filter::FuncTreePostgres
      when :sqlite
        OData::Filter::FuncTree.include OData::Filter::FuncTreeSqlite
      else
        OData::Filter::FuncTree.include OData::Filter::FuncTreeDefault
      end
    end

    def execute_deferred_iblocks
      @collections.each do |k|
        k.instance_eval(&k.deferred_iblock) if k.deferred_iblock
      end
    end

    ## Warning: base_url_regexp depends on '@collections', and this needs to be
    ## evaluated after '@collections' is filled !
    # A regexp matching all allowed base entities (eg product|categories )
    def base_url_regexp
      @collections.map(&:entity_set_name).join('|')
    end

    def service
      hres = {}
      hres['d'] = { 'EntitySets' => @collections.map(&:type_name) }
      hres
    end

    def service_xml(req)
      doc = REXML::Document.new
      # separator required ? ?
      root = doc.add_element('service', 'xml:base' => @uribase)

      root.add_namespace('xmlns:atom', XMLNS::W3_2005_ATOM)
      root.add_namespace('xmlns:app', XMLNS::W3_2007_APP)
      # this generates the main xmlns attribute
      root.add_namespace(XMLNS::W3_2007_APP)
      wp = root.add_element 'workspace'

      title = wp.add_element('atom:title')
      title.text = @xtitle

      @collections.each do |klass|
        col = wp.add_element('collection', 'href' => klass.entity_set_name)
        ct = col.add_element('atom:title')
        ct.text = klass.type_name
      end

      XML_PREAMBLE + doc.to_pretty_xml
    end

    def add_metadata_xml_entity_type(schema)
      @collections.each do |klass|
        enty = klass.add_metadata_rexml(schema)
        klass.add_metadata_navs_rexml(enty, @relman, @xnamespace)
      end
    end

    def add_metadata_xml_associations(schema)
      @relman.each_rel do |rel|
        rel.with_metadata_info(@xnamespace) do |name, bdinfo|
          assoc = schema.add_element('Association', 'Name' => name)
          bdinfo.each do |bdi|
            assoend = { 'Type' => bdi[:type],
                        'Role' => bdi[:role],
                        'Multiplicity' => bdi[:multiplicity] }
            assoc.add_element('End', assoend)
          end
        end
      end
    end

    def add_metadata_xml_entity_container(schema)
      ec = schema.add_element('EntityContainer',
                              'Name' => @xname,
                              'm:IsDefaultEntityContainer' => 'true')
      @collections.each do |klass|
        # 3.a Entity set's
        ec.add_element('EntitySet',
                       'Name' => klass.entity_set_name,
                       'EntityType' => @xnamespace ? "#{@xnamespace}.#{klass.type_name}" : klass.type_name)
      end
      # 3.b Association set's
      @relman.each_rel do |rel|
        assoc = ec.add_element('AssociationSet',
                               'Name' => rel.name,
                               'Association' => @xnamespace ? "#{@xnamespace}.#{rel.name}" : rel.name)

        rel.each_endobj do |eo|
          clazz = Object.const_get(eo)
          assoend = { 'EntitySet' => clazz.entity_set_name.to_s, 'Role' => eo }
          assoc.add_element('End', assoend)
        end
      end
    end

    def metadata_xml(_req)
      doc = REXML::Document.new
      doc.add_element('edmx:Edmx', 'Version' => '1.0')
      doc.root.add_namespace('xmlns:edmx', XMLNS::MSFT_ADO_2007_EDMX)
      serv = doc.root.add_element('edmx:DataServices',
                                  'm:DataServiceVersion' => '1.0')
      # 'm:DataServiceVersion' => "#{self.dataServiceVersion}" )
      # DataServiceVersion: This attribute MUST be in the data service
      # metadata namespace
      # (http://schemas.microsoft.com/ado/2007/08/dataservices) and SHOULD
      # be present on an
      # edmx:DataServices element [MC-EDMX] to indicate the version of the
      # data service CSDL
      # annotations (attributes in the data service metadata namespace) that
      # are used by the document.
      # Consumers of a data-service metadata endpoint ought to first read this
      # attribute value to determine if
      # they can safely interpret all constructs within the document. The
      # value of this attribute MUST be 1.0
      # unless a "FC_KeepInContent" customizable feed annotation
      # (section 2.2.3.7.2.1) with a value equal to
      # false is present in the CSDL document within the edmx:DataServices
      # node. In this case, the
      # attribute value MUST be 2.0 or greater.
      # In the absence of DataServiceVersion, consumers of the CSDL document
      # can assume the highest DataServiceVersion they can handle.
      serv.add_namespace('xmlns:m', XMLNS::MSFT_ADO_2007_META)

      schema = serv.add_element('Schema',
                                'Namespace' => @xnamespace,
                                'xmlns' => XMLNS::MSFT_ADO_2009_EDM)
      # 1. all EntityType
      add_metadata_xml_entity_type(schema)

      # 2. Associations
      add_metadata_xml_associations(schema)

      # 3. Enty container
      add_metadata_xml_entity_container(schema)

      XML_PREAMBLE + doc.to_pretty_xml
    end

    # methods related to transitions to next state (cf. walker)
    module Transitions
      DOLLAR_ID_REGEXP = Regexp.new('\A\/\$').freeze
      ALLOWED_TRANSITIONS_FIXED = [
        Safrano::TransitionEnd,
        Safrano::TransitionMetadata,
        Safrano::TransitionBatch,
        Safrano::TransitionContentId
      ].freeze

      def build_allowed_transitions
        @allowed_transitions = (ALLOWED_TRANSITIONS_FIXED + [
          Safrano::Transition.new(%r{\A/(#{base_url_regexp})(.*)},
                                  trans: 'transition_collection')
        ]).freeze
      end

      def allowed_transitions
        @allowed_transitions
      end

      def transition_collection(match_result)
        [@cmap[match_result[1]], :run] if match_result[1]
      end

      def transition_batch(_match_result)
        [@batch_handler, :run]
      end

      def transition_content_id(match_result)
        [match_result[2], :run_with_content_id]
      end

      def transition_metadata(_match_result)
        [@meta, :run]
      end

      def transition_end(_match_result)
        [nil, :end]
      end
    end

    include Transitions

    def odata_get(req)
      if req.accept?(APPXML)
        #  app.headers  'Content-Type' => 'application/xml;charset=utf-8'
        # Doc: 2.2.3.7.1 Service Document  As per [RFC5023], AtomPub Service
        # Documents MUST be
        # identified with the "application/atomsvc+xml" media type (see
        # [RFC5023] section 8).
        [200, CT_ATOMXML, [service_xml(req)]]
      else
        # this is returned by http://services.odata.org/V2/OData/OData.svc
        415
      end
    end
  end

  # for OData V1
  class ServiceV1 < ServiceBase
    def initialize
      @data_service_version = '1.0'
    end

    def get_coll_odata_links_h(array:, icount: nil)
      array.map do |w|
        get_entity_odata_link_h(entity: w)
      end
    end

    def get_coll_odata_h(array:, template:, icount: nil)
      array.map! do |w|
        get_entity_odata_h(entity: w,
                           template: template)
      end
      array
    end

    def get_emptycoll_odata_h
      EMPTY_HASH_IN_ARY
    end
  end

  # for OData V2
  class ServiceV2 < ServiceBase
    def initialize
      @data_service_version = '2.0'
    end

    def get_coll_odata_links_h(array:, icount: nil)
      res = array.map do |w|
        get_entity_odata_link_h(entity: w)
      end
      if icount
        { RESULTS_K => res, COUNT_K => icount }
      else
        { RESULTS_K => res }
      end
    end

    def get_emptycoll_odata_h
      { RESULTS_K => EMPTY_HASH_IN_ARY }
    end
  end

  # a virtual entity for the service metadata
  class ServiceMeta
    attr_accessor :service
    def initialize(service)
      @service = service
    end

    ALLOWED_TRANSITIONS_FIXED = [Safrano::TransitionEnd].freeze
    def allowed_transitions
      ALLOWED_TRANSITIONS_FIXED
    end

    def transition_end(_match_result)
      [nil, :end]
    end

    def odata_get(req)
      if req.accept?(APPXML)
        [200, CT_APPXML, [@service.metadata_xml(req)]]
      else
        415
      end
    end
  end
end
# end of Module OData
