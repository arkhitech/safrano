#!/usr/bin/env ruby

require 'set'
require 'sequel'

# some helpers
class String
  # thanks https://stackoverflow.com/questions/1448670/ruby-stringto-class
  def constantize
    names = split('::')
    names.shift if names.empty? || names.first.empty?

    const = Object
    names.each do |name|
      const = if const.const_defined?(name)
                const.const_get(name)
              else
                const.const_missing(name)
              end
    end
    const
  end
end

class PathNode < String
  attr_accessor :model_class
  attr_accessor :path_str
  def initialize(str, start_model)
    super(str)
    @start_model = start_model
  end

  def table_name
    @model_class.table_name
  end

  def set_model_class_by_parent_model(pmclass)
    @model_class = pmclass.association_reflection(to_sym)[:class_name].constantize
  end

  def alias_sym
    @start_model.get_alias_sym(path_str)
  end
end

class QPath
  attr_reader :segments
  attr_reader :start_model
  attr_reader :qpath

  def initialize(smodel, path_str)
    @qpath = path_str
    @start_model = smodel
    segment
  end

  def size
    @segments.size
  end

  # source: ..https://ruby-doc.org/core-2.6.5/Hash.html
  def ==(other)
    (self.class === other) &&
      (@start_model == other.start_model) &&
      (@qpath == other.qpath)
  end

  alias eql? ==

  def hash
    @start_model.hash ^ @qpath.hash # XOR
  end

  def segment
    @segments = []
    start_node = @start_model.create_path_node('')
    start_node.model_class = @start_model
    path_str = ''
    path_nodes = []
    start_node.path_str = path_str
    nodes = [start_node]
    nodes.concat(@qpath.split('/').map { |nstr| @start_model.create_path_node(nstr) })
    return unless nodes.size > 1

    nodes[0...-1].each_with_index do |node, i|
      nodes[i + 1].set_model_class_by_parent_model(node.model_class)
      path_nodes << nodes[i + 1]
      nodes[i + 1].path_str = path_nodes.join('/')
      @segments << [node, nodes[i + 1]]
    end
  end
end

class JoinByPathsHelper < Set
  attr_reader :result
  attr_reader :start_model
  EMPTY_ARRAY = [].freeze

  def initialize(smodel)
    super()
    @start_model = smodel
  end

  def build_unique_join_segments
    return (@result = EMPTY_ARRAY) if empty?

    maxlen = map(&:size).max
    iset = nil
    @result = []

    (0...maxlen).each do |i|
      iset = Set.new
      each { |qp| (iset << qp.segments[i]) if qp.segments[i] }
      @result << iset
    end

    @result.map! do |jseg|
      jseg.map  do |seg|
        leftm = seg.first.model_class
        assoc = leftm.association_reflection(seg.last.to_sym)

        rightm = seg.last.model_class
        # cf.  documentation in sequel/model/associations.rb
        case assoc[:type]
        # :many_to_one :: Foreign key in current model's table points to
        #                 associated model's primary key.
        when :many_to_one
          lks = [assoc[:key]].flatten
          rks = [rightm.primary_key].flatten

        when :one_to_many, :one_to_one
          # :one_to_many :: Foreign key in associated model's table points to this
          #                 model's primary key.
          # :one_to_one :: Similar to one_to_many in terms of foreign keys, but
          #                only one object is associated to the current object through the
          #                association.
          lks = [leftm.primary_key].flatten
          rks = [assoc[:key]].flatten

          # TODO
          # when         # :many_to_many :: A join table is used that has a foreign key that points
          #                  to this model's primary key and a foreign key that points to the
          #                  associated model's primary key.  Each current model object can be
          #                  associated with many associated model objects, and each associated
          #                  model object can be associated with many current model objects.
          # when    # :one_through_one :: Similar to many_to_many in terms of foreign keys, but only one object
          #                     is associated to the current object through the association.
          #                     Provides only getter methods, no setter or modification methods.

        end

        lks.map! { |k| Sequel[seg.first.alias_sym][k] } unless seg.first.empty?

        rks.map! { |k| Sequel[seg.last.alias_sym][k] }

        { type: assoc[:type],
          segment: seg,
          left: leftm.table_name,
          right: rightm.table_name,
          alias: seg.last.alias_sym,
          left_keys: lks,
          right_keys: rks,
          cond: rks.zip(lks).to_h }
      end
    end
  end

  def add(path_str)
    super(QPath.new(@start_model, path_str))
  end

  def dataset(start_dtset = nil)
    start_dataset = (start_dtset || @start_model.dataset)
    return start_dataset if empty?

    build_unique_join_segments
    @result.flatten.inject(start_dataset) do |dt, jo|
      dt.left_join(Sequel[jo[:right]].as(jo[:alias]),
                   jo[:cond],
                   implicit_qualifier: jo[:left])
    end
  end

  def join_by_paths_helper(*pathlist)
    pathlist.each { |path_str| add path_str }
    self
  end
end

module Sequel
  module Plugins
    module JoinByPaths
      def self.apply(model)
        model.instance_exec do
          @aliases_sym = {}
          @alias_cnt = 0
        end
      end

      def self.configure(model)
        model.instance_eval do
          @aliases_sym = {}
          @alias_cnt = 0
        end
      end
      module ClassMethods
        attr_reader :aliases_sym
        attr_reader :alias_cnt
        Plugins.inherited_instance_variables(self,
                                             :@aliases_sym => :dup,
                                             :@alias_cnt => :dup)

        def get_alias_sym(pstr)
          if @aliases_sym.key?(pstr)
            @aliases_sym[pstr]
          else
            @alias_cnt += 1
            @aliases_sym[pstr] = "a#{@alias_cnt}".to_sym
          end
        end

        def create_path_node(pstr)
          PathNode.new(pstr, self)
        end

        def join_by_paths_helper(*pathlist)
          jh = JoinByPathsHelper.new(self)
          pathlist.each { |path_str| jh.add path_str }
          jh
        end
      end

      module DatasetMethods
        attr_reader :join_helper
        def join_by_paths(*pathlist)
          model.join_by_paths_helper(*pathlist).dataset
        end
      end
    end
  end
end
