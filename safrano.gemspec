require_relative 'lib/safrano/version'

D = 'Safrano is an OData server library based on Ruby, Rack and Sequel.'.freeze
F = ['lib/safrano.rb'].freeze
F += Dir.glob('lib/*.rb') + Dir.glob('lib/odata/*.rb') + Dir.glob('lib/odata/filter/*.rb')
F += Dir.glob('lib/safrano/*.rb')
F += Dir.glob('lib/sequel/plugins/*.rb')

Gem::Specification.new do |s|
  s.name        = 'safrano'
  s.version     = Safrano::VERSION
  s.summary     = 'Safrano is a Ruby OData server library based on Sequel and Rack'
  s.description = D
  s.authors     = ['D.M.']
  s.email       = 'dev@aithscel.eu'
  s.files       = F
  s.required_ruby_version = '>= 2.4.0'
  s.add_dependency 'rack', '~> 2.0'
  s.add_dependency 'rack-cors', '~> 1.0'
  s.add_dependency 'sequel', '~> 5.15'
  s.add_dependency 'rfc2047'
  s.add_development_dependency 'rake', '~> 12.3'
  s.add_development_dependency 'rack-test'
  s.add_development_dependency 'rubocop', '~> 0.51'
  s.homepage = 'https://gitlab.com/dm0da/safrano'
  s.license = 'MIT'
  s.metadata = {
    "bug_tracker_uri" => "https://gitlab.com/dm0da/safrano/issues",
    "changelog_uri" => "https://gitlab.com/dm0da/safrano/blob/master/CHANGELOG",
    "source_code_uri" => "https://gitlab.com/dm0da/safrano/tree/master",
    "wiki_uri" => "https://gitlab.com/dm0da/safrano/wikis/home"
  }
end
