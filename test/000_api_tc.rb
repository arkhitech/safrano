#!/usr/bin/env ruby

require 'test/unit'
require 'pp'
require 'set'
require_relative '../lib/safrano.rb'

#require_relative  'botanics/db.rb'
require_relative 'test_botanics_db.rb'

# only for testing
module API_Testing
  refine OData::Media::Static do
    attr_reader :root
  end
  ## workaround bad Testcase design (last media testcase has side-effects)
  #refine OData::EntityClassBase do
    #def reset_media_4_testing
      #@media_handler
    #end
  #end
end
using API_Testing

class Cultivar < Sequel::Model(:cultivar)
  # Navigation attributes
  one_to_many :parent, class: :Parentage, key: :cultivar_id
  one_to_many :child, class: :Parentage, key: :parent_id

  many_to_one :breeder, key: :breeder_id
end

class Breeder < Sequel::Model(:breeder)
  one_to_many :cultivar, key: :breeder_id
end
# some class not derived from Sequel::Model()
class NonModelClass
end

class Parentage < Sequel::Model(:parentage)
  # A parentage is for a given cultivar; there can be multiple parentage records
  # (type: father or mother), or one (type seedling) or none
  #
  many_to_one :child, class: :Cultivar, key: :cultivar_id
  many_to_one :parent, class: :Cultivar, key: :parent_id
  many_to_one :par_type, class: :ParentageType, key: :ptype_id
end

class ODataBotanicsAPI_TC1_App < OData::ServerApp
end
class ODataBotanicsAPI_TCAssoc_App < OData::ServerApp
end
class ODataBotanicsAPI_TCNoPrefix_App < OData::ServerApp
end
class ODataBotanicsAPI_TCPrefix_App < OData::ServerApp
end
class ODataBotanicsAPI_TCBatch_App < OData::ServerApp
end
class ODataBotanicsAPI_TCMedia_App < OData::ServerApp
end
class ODataBotanicsAPI_TCMedia_App2 < OData::ServerApp
end
class ODataBotanicsAPI_TCMedia_App3 < OData::ServerApp
end
class ODataBotanicsAPI_TCMedia_Err_App < OData::ServerApp
end


class API_TC < BotanicsDBTestII
  
  
  def test_api_publish_ok
    assert_nothing_raised do
      ODataBotanicsAPI_TC1_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'

        publish_model Cultivar do
          add_nav_prop_single :breeder
          add_nav_prop_collection :parent
          add_nav_prop_collection :child
        end
        publish_model Breeder do add_nav_prop_collection :cultivar end

        publish_model Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child
        end
      end
    end
    sb = ODataBotanicsAPI_TC1_App.get_service_base
    assert_equal OData::ServiceBase, sb.class
    assert_equal OData::ServiceV1, sb.v1.class
    assert_equal OData::ServiceV2, sb.v2.class

    assert_equal '', sb.xpath_prefix
    assert_equal '', sb.v1.xpath_prefix
    assert_equal '', sb.v2.xpath_prefix


    assert_equal OData::Batch::DisabledHandler, sb.batch_handler.class
    assert_equal OData::Batch::DisabledHandler, sb.v1.batch_handler.class
    assert_equal OData::Batch::DisabledHandler, sb.v2.batch_handler.class

  end

  def test_path_prefix

    assert_nothing_raised do
      ODataBotanicsAPI_TCPrefix_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/botany.svc'

        publish_model Cultivar do
          add_nav_prop_single :breeder
          add_nav_prop_collection :parent
          add_nav_prop_collection :child
        end
        publish_model Breeder do add_nav_prop_collection :cultivar end

        publish_model Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child
        end
      end
    end
    sb = ODataBotanicsAPI_TCPrefix_App.get_service_base
    assert_equal '/botany.svc', sb.xpath_prefix
    assert_equal '/botany.svc', sb.v1.xpath_prefix
    assert_equal '/botany.svc', sb.v2.xpath_prefix

  end

  def test_nopath_prefix

    assert_nothing_raised do
      ODataBotanicsAPI_TCNoPrefix_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        # path_prefix not called !
        
        publish_model Cultivar do
          add_nav_prop_single :breeder
          add_nav_prop_collection :parent
          add_nav_prop_collection :child
        end
        publish_model Breeder do add_nav_prop_collection :cultivar end

        publish_model Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child
        end
      end
    end
    sb = ODataBotanicsAPI_TCNoPrefix_App.get_service_base
    assert_equal '', sb.xpath_prefix
    assert_equal '', sb.v1.xpath_prefix
    assert_equal '', sb.v2.xpath_prefix

  end


  def test_enable_batch

    assert_nothing_raised do
      ODataBotanicsAPI_TCBatch_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/botany.svc'
        enable_batch

        publish_model Cultivar do
          add_nav_prop_single :breeder
          add_nav_prop_collection :parent
          add_nav_prop_collection :child
        end
        publish_model Breeder do add_nav_prop_collection :cultivar end

        publish_model Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child
        end
      end
    end
    sb = ODataBotanicsAPI_TCBatch_App.get_service_base

    assert_equal OData::Batch::EnabledHandler, sb.batch_handler.class
    assert_equal OData::Batch::EnabledHandler, sb.v1.batch_handler.class
    assert_equal OData::Batch::EnabledHandler, sb.v2.batch_handler.class
  end

  def test_publish_invalid_model
    assert_raise OData::API::ModelNameError do
      ODataBotanicsAPI_TC1_App.publish_service do
        publish_model NonModelClass
      end
    end
  end

  def test_publish_invalid_association_single
    assert_raise OData::API::ModelAssociationNameError do
      ODataBotanicsAPI_TCAssoc_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'

        publish_model Cultivar do
          # hey...
          add_nav_prop_single :breeeeeeeeder
          add_nav_prop_collection :parent
          add_nav_prop_collection :child
        end
        publish_model Breeder do add_nav_prop_collection :cultivar end

        publish_model Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child
        end
      end
    end
  end


  def test_publish_invalid_association_collection
    assert_raise OData::API::ModelAssociationNameError do
      ODataBotanicsAPI_TCAssoc_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'
        path_prefix '/'

        publish_model Cultivar do
          add_nav_prop_single :breeder
          # hey...
          add_nav_prop_collection :paaaaaaaaaaaarent
          add_nav_prop_collection :child
        end
        publish_model Breeder do add_nav_prop_collection :cultivar end

        publish_model Parentage do
          add_nav_prop_single :parent
          add_nav_prop_single :child

        end
      end
    end
  end
end

class API_Media_TC < BotanicsDBTestII
  # TODO : test the creation of media root and mediaentity directories !
  
  MediaRoot = File.absolute_path('xyz/media', File.dirname(__FILE__))
  MediaRootAlt = File.absolute_path('abc/medium', File.dirname(__FILE__))
  
  # cleanup directories potentially created in testcases
  def delete_media_dirs
    Dir.delete('Breeder') if File.exists? 'Breeder'
    Dir.delete('Photo') if File.exists? 'Photo'
    
    
    Dir.delete('xyz/media/Photo') if File.exists? 'xyz/media/Photo'
    Dir.delete('xyz/media') if File.exists? 'xyz/media'
    Dir.delete('xyz') if File.exists? 'xyz'
    Dir.delete('test/xyz/media/Photo') if File.exists? 'test/xyz/media/Photo'
    Dir.delete('test/xyz/media') if File.exists? 'test/xyz/media'
    Dir.delete('test/xyz') if File.exists? 'test/xyz'
    
    Dir.delete('abc/medium/Photo') if File.exists? 'abc/medium/Photo'
    Dir.delete('abc/medium') if File.exists? 'abc/medium'
    Dir.delete('abc') if File.exists? 'abc'
    Dir.delete('test/abc/medium/Photo') if File.exists? 'test/abc/medium/Photo'
    Dir.delete('test/abc/medium') if File.exists? 'test/abc/medium'
    Dir.delete('test/abc') if File.exists? 'test/abc'
    
    
  end
  
  def setup
    delete_media_dirs
  end
  def teardown
    delete_media_dirs
  end
  
  def test_publish_media_entity


    assert_nothing_raised do
      ODataBotanicsAPI_TCMedia_App.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'

        publish_media_model Photo 
        
      end
    end
    
    # check the default media handler is set
    md = Photo.media_handler
    assert_kind_of OData::Media::Static, md
    
    assert_equal Dir.pwd, md.root
  end

  def test_publish_media_entity_static_handler

    assert_nothing_raised do
      ODataBotanicsAPI_TCMedia_App2.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'

        publish_media_model Photo do 
          use OData::Media::Static, :root => MediaRoot
        end

        
      end      
    end
    # check the requested static media handler is set    
    md = Photo.media_handler
    assert_kind_of OData::Media::Static, md
    assert_equal MediaRoot , md.root
  end

  # change media handler (mostly for testing)
  def test_publish_change_media_entity_handler

    assert_nothing_raised do
      ODataBotanicsAPI_TCMedia_App3.publish_service do
        title 'Cultivar Parentage OData API'
        name 'ParentageService'
        namespace 'ODataPar'

        publish_media_model Photo do 
          use OData::Media::Static, :root => MediaRoot 
        end

        
      end      
    end
    # check the requested static media handler is set    
    md = Photo.media_handler
    assert_kind_of OData::Media::Static, md
    assert_equal MediaRoot , md.root
    
    Photo.use OData::Media::StaticTree, :root => MediaRootAlt
    md = Photo.media_handler
    assert_kind_of OData::Media::StaticTree, md
    assert_equal MediaRootAlt, md.root
  end

  def test_publish_media_entity_err
# testcase has side effects. Better leave it out for now
    #assert_raise OData::API::MediaModelError do
      #ODataBotanicsAPI_TCMedia_Err_App.publish_service do
        #title 'Cultivar Parentage OData API'
        #name 'ParentageService'
        #namespace 'ODataPar'

        #publish_media_model  Breeder
        
      #end
    #end
    
  end


end
