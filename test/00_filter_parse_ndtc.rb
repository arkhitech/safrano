require 'test/unit'

require_relative '../lib/odata/filter/parse.rb'

class FilterParseBasics < TCWithoutDB

  include OData::Filter

  def test_lit_node
    assert_nothing_raised do
      @ast =  Parser.new('axy').build
    end
    expt = RootTree.new do attach Literal.new('axy') end
    assert_equal expt, @ast
  end

  def test_qualit_node
    assert_nothing_raised do
      @ast =  Parser.new('a/xy').build
    end
    expt = RootTree.new do attach Qualit.new('a/xy') end
    assert_equal expt, @ast
  end

  def test_qstring_node
    assert_nothing_raised do
      @ast =  Parser.new("'axy'").build
    end
    expt = RootTree.new do attach QString.new('axy') end
    assert_equal expt, @ast
  end

  def test_qstring_escape_quote_node
    assert_nothing_raised do
      @ast =  Parser.new("'axy''quoted'''").build
    end
    expt = RootTree.new do attach QString.new("axy'quoted'") end
    assert_equal expt, @ast
  end

  def test_fpnumber_node
    assert_nothing_raised do
      @ast =  Parser.new('5').build
    end
    expt = RootTree.new do attach FPNumber.new('5') end
    assert_equal expt, @ast

    assert_nothing_raised do
      @ast =  Parser.new('5.12').build
    end
    expt = RootTree.new do attach FPNumber.new('5.12') end
    assert_equal expt, @ast

    assert_nothing_raised do
      @ast =  Parser.new('3.1415e1').build
    end
    expt = RootTree.new do attach FPNumber.new('3.1415e1') end
    assert_equal expt, @ast

    assert_nothing_raised do
      @ast =  Parser.new('3.1415e+1').build
    end
    expt = RootTree.new do attach FPNumber.new('3.1415e+1') end
    assert_equal expt, @ast

    assert_nothing_raised do
      @ast =  Parser.new('3.1415e-1').build
    end
    expt = RootTree.new do attach FPNumber.new('3.1415e-1') end
    assert_equal expt, @ast

  end

  def test_func_2_args
    assert_nothing_raised do
      @ast =  Parser.new('concat(a,b)').build
    end
    # instance_eval magic....
    expt = RootTree.new do
      attach FuncTree.new(:concat){
        attach ArgTree.new('('){
          attach Literal.new 'a'
          update_state(',', :Separator)
          attach Literal.new 'b'
          update_state(')', :Delimiter)
        }
      }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_func_2_args_lit_qstring
    assert_nothing_raised do
      @ast =  Parser.new(%Q<concat(a,'b')>).build
    end
    # instance_eval magic....
    expt = RootTree.new do
      attach FuncTree.new(:concat){
        attach ArgTree.new('('){
          attach Literal.new 'a'
          update_state(',', :Separator)
          attach QString.new 'b'
          update_state(')', :Delimiter)
        }
      }

    end
    assert_equal expt, @ast
  end

  def test_binops_expr_eq
    assert_nothing_raised do
      @ast =  Parser.new('a EQ b').build
    end
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:eq){
            attach Literal.new 'a'
            attach Literal.new 'b'
          }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_binops_expr_arith
    assert_nothing_raised do
      @ast =  Parser.new('a add b').build
    end
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:add){
            attach Literal.new 'a'
            attach Literal.new 'b'
          }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_binops_expr_le
    assert_nothing_raised do
      @ast =  Parser.new('a Le b').build
    end
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:le){
            attach Literal.new 'a'
            attach Literal.new 'b'
          }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_binops_expr_gt_in_parenthesis
    assert_nothing_raised do
      @ast =  Parser.new('(a GT b)').build
    end
    # instance_eval magic....
    expt = RootTree.new do
          attach IdentityFuncTree.new{
            attach ArgTree.new('(') {
              attach BinopTree.new(:gt){
                attach Literal.new 'a'
                attach Literal.new 'b'
            }
            update_state(')', :Delimiter)
          }
        }
    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_concat_func_eql_exp
    assert_nothing_raised do
      @ast =  Parser.new('concat(a,b) EQ x').build
    end
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:eq){
            attach FuncTree.new(:concat){
              attach ArgTree.new('('){
                attach Literal.new 'a'
                update_state(',', :Separator)
                attach Literal.new 'b'
                update_state(')', :Delimiter)
              }
            }
            attach Literal.new 'x'
          }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_trim_func_eql_exp
    assert_nothing_raised do
      @ast =  Parser.new(%Q<trim(a) EQ 'xyz'>).build
    end
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:eq){
            attach FuncTree.new(:trim){
              attach ArgTree.new('('){
                attach Literal.new 'a'
                update_state(')', :Delimiter)
              }
            }
            attach QString.new 'xyz'
          }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_not_expr
    assert_nothing_raised do
      @ast =  Parser.new('not true').build
    end
    # instance_eval magic....
    expt = RootTree.new do
          attach UnopTree.new(:not){
            attach Literal.new 'true'
          }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_not_not_expr
    assert_nothing_raised do
      @ast =  Parser.new('not not true').build
    end
    # instance_eval magic....
    expt = RootTree.new do
          attach UnopTree.new(:not){
            attach UnopTree.new(:not){
              attach Literal.new 'true'
            }
          }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_not_func_expr
    assert_nothing_raised do
      @ast =  Parser.new(%Q<not endswith(a,'name')>).build
    end
    # instance_eval magic....
    expt = RootTree.new do
          attach UnopTree.new(:not){
            attach FuncTree.new(:endswith){
              attach ArgTree.new('('){
                attach Literal.new 'a'
                update_state(',', :Separator)
                attach QString.new 'name'
                update_state(')', :Delimiter)
              }
            }
          }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  # now it's not basics anymore
end

class FilterParseLogical < TCWithoutDB

  include OData::Filter

  ## is this correct? normally NOT has higher precedence as EQ, thus
  ## is should read as (not concat(a,b)) EQ x) but concat does return a string
  ## --> should be a typ error ?
  #def test_not_func_eql_exp
    #assert_nothing_raised do
      #@ast =  Parser.new('not concat(a,b) EQ x').build
    #end
    ## instance_eval magic....
    #expt = RootTree.new do
        #attach UnopTree.new(:not){
          #attach BinopTree.new(:eq){
            #attach FuncTree.new(:concat){
              #attach ArgTree.new('('){
                #attach Literal.new 'a'
                #update_state(',', :Separator)
                #attach Literal.new 'b'
                #update_state(')', :Delimiter)
              #}
            #}
            #attach Literal.new 'x'
          #}
    #}
    #end
    #assert_kind_of RootTree, @ast
    #assert_equal expt, @ast
  #end

  # mul/div has higher preced as add/sub
  def test_arithm_mul_preced_sub
    # should eval as (a*b) - 5
    assert_nothing_raised do
      @ast =  Parser.new('a mul b sub 5').build
    end
    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:sub){
            attach BinopTree.new(:mul){
              attach Literal.new 'a'
              attach Literal.new 'b'
              update_state('b', :Literal)
          }
          attach FPNumber.new '5'
          update_state('5', :FPNumber)
        }
    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  # arithm has higher preced as comparison
  def test_arithm_preced
    # should eval as (a+b) gt c
    assert_nothing_raised do
      @ast =  Parser.new('a add b GT 5').build
    end
    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:gt){
            attach BinopTree.new(:add){
              attach Literal.new 'a'
              attach Literal.new 'b'
              update_state('b', :Literal)
          }
          attach FPNumber.new '5'
          update_state('5', :FPNumber)
        }
    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  # precedence not > relational(le,gt...) > and
  def test_unop_gt

    # should eval as (not x) eq b
    assert_nothing_raised do
      @ast =  Parser.new('not x eq b').build
    end
    # instance_eval magic....
    expt = RootTree.new do
      attach BinopTree.new(:eq){
        attach UnopTree.new(:not){
          attach Literal.new('x')
      }
      attach Literal.new('b')
      update_state('b', :Literal)
    }
    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  # check and > or precedence
  def test_binops_expr_and_or

    assert_nothing_raised do
      @ast =  Parser.new('a and b or c').build
    end

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:or){
            attach BinopTree.new(:and){
              attach Literal.new 'a'
              attach Literal.new 'b'
              update_state('b', :Literal)
          }
          attach Literal.new 'c'
          update_state('c', :Literal)
        }
    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_binops_expr_or_and

    assert_nothing_raised do
      @ast =  Parser.new('a or b and c').build
    end

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:or){
            attach Literal.new 'a'
            attach( ba = BinopTree.new(:and){
              attach Literal.new 'b'
              attach Literal.new 'c'
              update_state('c', :Literal)
          })
          update_state(ba, :BinopTree)
        }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  # precedence not > relational(le,gt...) > and
  def test_unop_binop_expr_and_gt

    # should eval as (not x) and (a gt 5)
    assert_nothing_raised do
      @ast =  Parser.new('not x and a gt 5').build
    end
    # instance_eval magic....
    expt = RootTree.new do
      attach BinopTree.new(:and){
        attach UnopTree.new(:not){
          attach Literal.new('x')
      }
      attach BinopTree.new(:gt){
        attach Literal.new('a')
        attach FPNumber.new('5')
        update_state('5', :FPNumber)
      }
    }
    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  # precedence relational(le,gt...) > and
  def test_binops_expr_le_and_gt_2

    assert_nothing_raised do
      @ast =  Parser.new('a le 200 and a gt 5').build
    end

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:and){
            attach BinopTree.new(:le){
            attach Literal.new 'a'
            attach FPNumber.new '200'
            update_state('200', :FPNumber)
          }
          attach BinopTree.new(:gt){
          attach Literal.new 'a'
          attach FPNumber.new '5'
          update_state('5', :FPNumber)
        }
      }
    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  def test_binops_expr_le_and_gt_3

    # according to precedence, this is grouped  as
    # ' ((a le 200) and (a gt 5)) or (b eq 100)'
    assert_nothing_raised do
      @ast =  Parser.new('a le 200 and a gt 5 or b eq 100').build
    end

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:or){
      attach BinopTree.new(:and){
            attach BinopTree.new(:le){
            attach Literal.new 'a'
            attach FPNumber.new '200'
          }
          attach BinopTree.new(:gt){
          attach Literal.new 'a'
          attach FPNumber.new '5'
        }
      }
      attach BinopTree.new(:eq){
        attach Literal.new 'b'
        attach FPNumber.new '100'
      }
    }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt.children.first, @ast.children.first
  end

  def test_binops_expr_func_eq_or_2

    assert_nothing_raised do
      @ast =  Parser.new(%Q<toupper(a) eq 'ME' or toupper(a) eq 'AND'>).build
    end

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:or){
            attach BinopTree.new(:eq){
              attach FuncTree.new(:toupper){
                attach ArgTree.new('('){
                  attach Literal.new 'a'
                  update_state(')', :Delimiter)
                }
              }
            attach QString.new 'ME'
            }
            attach BinopTree.new(:eq){
              attach FuncTree.new(:toupper){
                attach ArgTree.new('('){
                  attach Literal.new 'a'
                  update_state(')', :Delimiter)
                }
              }
            attach QString.new 'AND'
            }
          }
    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

  # same precedences (or) --> uses leftmost
  def test_binops_expr_func_eq_or_3

    assert_nothing_raised do
      @ast =  Parser.new(%Q<toupper(a) eq 'ME' or toupper(b) eq 'AND' or toupper(c) eq 'YOU'>).build
    end

    ta = BinopTree.new(:eq){
      attach FuncTree.new(:toupper){
        attach ArgTree.new('('){
          attach Literal.new 'a'
          update_state(')', :Delimiter)
        }
      }
      attach QString.new 'ME'
      update_state('ME', :QString)
      }

      tb = BinopTree.new(:eq){
        attach FuncTree.new(:toupper){
          attach ArgTree.new('('){
            attach Literal.new 'b'
            update_state(')', :Delimiter)
          }
        }
        attach QString.new 'AND'
        update_state('AND', :QString)
      }

       tc = BinopTree.new(:eq){
        attach FuncTree.new(:toupper){
          attach ArgTree.new('('){
            attach Literal.new 'c'
            update_state(')', :Delimiter)
          }
        }
      attach QString.new 'YOU'
      update_state('YOU', :QString)
      }

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:or){
      attach BinopTree.new(:or){
         attach ta
         attach tb
         update_state(tb, :BinopTree)
         }
      attach tc
      update_state(tc, :BinopTree)
      }
    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end

end

class FilterParseNested < TCWithoutDB

  include OData::Filter

  def test_concat_nested_eql
    assert_nothing_raised do
      inp = %Q<concat(firstname,concat(' ', lastname)) EQ 'Larry Joplin'>
      @ast =  Parser.new(inp).build
    end
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:eq){
            attach FuncTree.new(:concat){
              attach ArgTree.new('('){
                attach Literal.new 'firstname'
                @state = accept?(',', :Separator)
                attach FuncTree.new(:concat){
                  attach ArgTree.new('('){
                  attach QString.new ' '
                  update_state(',', :Separator)
                  attach Literal.new 'lastname'
                  update_state(')', :Delimiter)
              }
                }
                update_state(')', :Delimiter)
              }
            }
            attach QString.new 'Larry Joplin'
          }

    end
    assert_kind_of RootTree, @ast
    assert_equal expt, @ast
  end
end

class FilterParseBasicErrors < TCWithoutDB

  include OData::Filter

  def test_single_closing_parenth
    assert_nothing_raised do
      @ast =  Parser.new(')').build
    end
    assert_kind_of Parser::ErrorUnmatchedClose, @ast
  end

  def test_unmatched_closing_parenth
    assert_nothing_raised do
      @ast =  Parser.new('(a EQ b))').build
    end
    assert_kind_of Parser::ErrorUnmatchedClose, @ast
  end

    def test_unmatched_quote
    assert_nothing_raised do
      @ast =  Parser.new("startswith(name,B')").build
    end
    assert_kind_of Parser::UnmatchedQuoteError, @ast
  end

    def test_unmatched_and
    assert_nothing_raised do
      @ast =  Parser.new("AND name eq 'test'").build
    end
    assert_kind_of Parser::ErrorInvalidToken, @ast
  end



  def test_two_seps
    assert_nothing_raised do
      @ast =  Parser.new('concat(a,,b)').build
    end
    assert_kind_of Parser::ErrorInvalidToken, @ast
  end

  def test_invalid_token_fp
    assert_nothing_raised do
      @ast =  Parser.new('3.1415d-1').build
    end
    assert_kind_of Parser::ErrorInvalidToken, @ast

    assert_nothing_raised do
      @ast =  Parser.new('3.1415x-1').build
    end
    assert_kind_of Parser::ErrorInvalidToken, @ast

    assert_nothing_raised do
      @ast =  Parser.new('3,1415').build
    end
    assert_kind_of Parser:: ErrorInvalidSeparator, @ast

  end

  def test_two_lits
    assert_nothing_raised do
      @ast =  Parser.new('a b').build
    end
    assert_kind_of Parser::ErrorInvalidToken, @ast
  end

  def test_func_arity
    assert_nothing_raised do
      @ast =  Parser.new('toupper(a,b)').build
    end
    assert_kind_of Parser::ErrorInvalidArity, @ast

    assert_nothing_raised do
      @ast =  Parser.new('concat(a,b,c)').build
    end
    assert_kind_of Parser::ErrorInvalidArity, @ast

    assert_nothing_raised do
      @ast =  Parser.new('concat(a)').build
    end
    assert_kind_of Parser::ErrorInvalidArity, @ast

  end

  # a parenthesis expression should not contain a list
  def test_parenth_expr_arity
    assert_nothing_raised do
      @ast =  Parser.new('(a,b)').build
    end
    assert_kind_of Parser::ErrorInvalidArity, @ast

    # but it can contain a single (eg. logical) expression
    assert_nothing_raised do
      @ast =  Parser.new('(a EQ b)').build
    end
    assert_kind_of RootTree, @ast

    assert_nothing_raised do
      @ast =  Parser.new('(a EQ 3.14)').build
    end
    assert_kind_of RootTree, @ast

  end

  # we dont have yet a full type check, because the type
  # of literals is on DB level... but we can at least
  # check that use with number raise an invalid type error
  def test_func_type_error
    assert_nothing_raised do
      @ast =  Parser.new('length(a)').build
    end
    assert_kind_of RootTree, @ast

    assert_nothing_raised do
      @ast =  Parser.new('length(3.14)').build
    end
    assert_kind_of Parser::ErrorInvalidArgumentType, @ast
  end

  def test_nested_mixed

    assert_nothing_raised do
      @ast =  Parser.new("concat(concat(first_name,' '),last_name)").build
    end
    assert_kind_of RootTree, @ast

    assert_nothing_raised do
      @ast =  Parser.new("length(concat(concat(first_name,' '),last_name)) eq 10 ").build
    end
    assert_kind_of RootTree, @ast

    #check with spaces
    assert_nothing_raised do
      @ast =  Parser.new("length(concat(concat(first_name, ' '), last_name)) eq 10 ").build
    end
    assert_kind_of RootTree, @ast

  end
end
