#!/usr/bin/env ruby

require_relative './test_botanics_db.rb' # Botanics-DB testdata

class ServiceMetadataTC < BotanicsDBTest

  def test_it_has_service
    get '/'
    assert last_response.ok?
  end


  def test_it_has_metadata_properties
    get '/$metadata'

    assert_equal "application/xml;charset=utf-8",last_response.content_type

    assert last_response.ok?

    xml = REXML::Document.new(last_response.body)

    xmlds = xml.root.elements.first
    assert_equal 'DataServices', xmlds.name
    
    xmlsch = xmlds.elements.first
    assert_equal 'Schema', xmlsch.name
    
    assert cultivarTypeXml = xmlsch.elements.find('EntityType'){|e| e.attributes['Name'] == 'Cultivar'}

    cultivarProps = { id: 'Edm.Int32',
                      name: 'Edm.String',
                      year: 'Edm.Int32',
                      breeder_id: 'Edm.Int32',
                      inst_id: 'Edm.Int32'}
    
    cultivarProps.each{|name, type| 
      assert cultivarTypeXml.each_element('Property'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
            (   e.attributes['Type'] == type ) )
      }, "#{name}, #{type} not ok"

    }
    
    assert cultivarTypeXml.each_element('Key'){}.find{|e|
      e.each_element('PropertyRef'){}.find{|k| k.attributes['Name'] == 'id' }
    } 

    
  end


  def test_it_has_metadata_navproperties
    get '/$metadata'

    assert_equal "application/xml;charset=utf-8",last_response.content_type

    assert last_response.ok?

    xml = REXML::Document.new(last_response.body)
    assert_equal('http://schemas.microsoft.com/ado/2007/06/edmx',
                 xml.root.attributes['xmlns:edmx'])
    assert_equal('Edmx', xml.root.name)
    assert_equal('edmx', xml.root.prefix)

    xmlds = xml.root.elements.first
    assert_equal 'DataServices', xmlds.name
    
    xmlsch = xmlds.elements.first
    assert_equal 'Schema', xmlsch.name
    
    assert cultivarTypeXml = xmlsch.elements.find('EntityType'){|e| e.attributes['Name'] == 'Cultivar'}

    cultivarRels = { institute: %w( Institute 0..1 ),
                     parent: %w(Parentage *),
                     child: %w(Parentage *),
                     photo: %w(Photo *),
                     breeder: %w(Breeder 0..1)}
    
    cultivarRels.each{|name, props| 
      toRole = props[0]
      multpl = props[1]
      
      # check that we have all NavProperties
      assert nav_prop = cultivarTypeXml.elements.find('NavigationProperty'){|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
              ( e.attributes['ToRole'] == toRole ) )
      }
      
      # check that their referenced Association exists
      namespace, nav_prop_rel = nav_prop.attributes['Relationship'].split('.')
      assert_equal 'ODataPar', namespace
      
      assert ( assoc = xmlsch.elements.find('Association'){|e| e.attributes['Name'] == nav_prop_rel } )
      
      # check the multiplicity of the toRole End (ie. single/multiple )
      assert (toRoleProps = assoc.each_element('End'){}.find{|e| e.attributes['Role'] == toRole})
      
      assert_equal multpl, toRoleProps.attributes['Multiplicity']
      
    }

    
  end


 def test_it_has_stream_attribute
    get '/$metadata'

    assert_equal "application/xml;charset=utf-8",last_response.content_type

    assert last_response.ok?

    xml = REXML::Document.new(last_response.body)

    xmlds = xml.root.elements.first
    assert_equal 'DataServices', xmlds.name
    
    xmlsch = xmlds.elements.first
    assert_equal 'Schema', xmlsch.name
    
    assert photoTypeXml = xmlsch.elements.find('EntityType'){|e| e.attributes['Name'] == 'Photo'}
    
    assert_equal 'true', photoTypeXml.attributes['HasStream']

    assert cultivarTypeXml = xmlsch.elements.find('EntityType'){|e| e.attributes['Name'] == 'Cultivar'}

    assert_nil cultivarTypeXml.attributes['HasStream']
  end
  
end

