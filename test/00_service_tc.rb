#!/usr/bin/env ruby

require_relative './test_saildb.rb' # Sail-DB testdata

class ServiceTC < SailDBTest

  def test_it_has_service
    get '/'
    assert last_response.ok?
# TODO: put this in a separate test unit for version handling
# here we just test the default version is 2.0
    assert_equal '2.0', last_response.headers['DataServiceVersion']
    xml = REXML::Document.new(last_response.body)
    xmlr = xml.root
    assert_equal('http://www.w3.org/2005/Atom', xmlr.attributes['xmlns:atom'])
    assert_equal('http://www.w3.org/2007/app', xmlr.attributes['xmlns:app'])
    assert_equal('http://www.w3.org/2007/app', xmlr.attributes['xmlns'])
    assert_equal('service', xmlr.name)
    xmltitle = xml.elements.to_a('service/workspace/atom:title').first
    assert_equal 'Sail OData API', xmltitle.get_text.value
  end

  def test_it_has_metadata
    get '/$metadata'

    assert_equal "application/xml;charset=utf-8",last_response.content_type

    assert last_response.ok?

    xml = REXML::Document.new(last_response.body)
    assert_equal('http://schemas.microsoft.com/ado/2007/06/edmx',
                 xml.root.attributes['xmlns:edmx'])
    assert_equal('Edmx', xml.root.name)
    assert_equal('edmx', xml.root.prefix)

# check the entity container a bit
    enty_cont = xml.elements.to_a('edmx:Edmx/edmx:DataServices/Schema/EntityContainer').first
    assert_equal('SailService', enty_cont.attributes['Name'])

    entity_set = Set.new
    enty_cont.get_elements('EntitySet').map do |e|
      entity_set.add [e.attributes['Name'], e.attributes['EntityType']]
    end
    assert_equal ExpectedEntitySet, entity_set

  end

  def test_it_has_options
    options '/'
    assert last_response.ok?

    options '/$metadata'
    assert last_response.ok?
  end

  def test_it_has_error_handling
    get '/blabluxdfsf'
    assert !last_response.ok?
    # documentation about last_response methods can be found here:
    # http://www.rubydoc.info/github/rack/rack/master/Rack/Response/Helpers#bad_request%3F-instance_method
    # and the http status codes: https://de.wikipedia.org/wiki/HTTP-Statuscode
    # expected OData error is ???
  end

end

