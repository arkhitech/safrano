#!/usr/bin/env ruby

require_relative './test_saildb.rb' # Sail-DB testdata

class Walker_TC < SailDBTestII

  OSERV = ODataSailApp.get_service_base
  def walker_status_context_check(st_exp, ctx_exp, path)
    walker = OData::Walker.new(OSERV, path)
    assert_equal(ctx_exp, walker.end_context,
                 "End context of path #{path} is not correct")
    assert_equal(st_exp, walker.status,
                 "End status of path #{path} is not correct")

  end

# check that a given path results in an expected error state
# TODO: check then end context as well (should be a error object
#   matching the type of error)
  def walker_error_status_check(path)
    walker = OData::Walker.new(OSERV, path)

    assert_equal(:error, walker.status,
                 "End status of path #{path} with known error is not :error")
  end

  def test_metadata
    walker_status_context_check( :end, OSERV.meta, '/$metadata')
    walker_status_context_check( :end, OSERV.meta, '/$metadata/')
  end

  def test_metadata_err
    walker_status_context_check( :error, nil, '/$metatata/')
  end

  def test_root
    walker_status_context_check( :end, OSERV, '/')
  end

  def test_top_level_collection_1
    walker_status_context_check( :end, Race, '/Race')
    walker_status_context_check( :end, Race, '/Race/')

    walker_status_context_check( :end, Person, '/Person')
    walker_status_context_check( :end, Person, '/Person/')
  end

# Check a collection with a renamed entity name (not equal the class name)
  def test_top_level_collection_renamed
    walker_status_context_check( :end, PeriodicityType, '/periodicity_type')
    walker_status_context_check( :end, PeriodicityType, '/periodicity_type/')
  end


# this is a special case because CrewMember has substring Crew
# and this requires special handling in the Regexp
  def test_top_level_collection_entity_substring

    walker_status_context_check( :end, Crew, '/Crew')
    walker_status_context_check( :end, Crew, '/Crew/')

    walker_status_context_check( :end, CrewMember, '/CrewMember')
    walker_status_context_check( :end, CrewMember, '/CrewMember/')

    walker_status_context_check( :end, RaceType, '/RaceType')
    walker_status_context_check( :end, RaceType, '/RaceType/')

  end

  # check that the collection json output works. This test covers that the
  # "uri" method works for elements of the coll
  def test_top_level_collection_1_json
    path = '/Race'
    walker = OData::Walker.new(OSERV, path)
    assert(walker.end_context.to_json.is_a?(String),
           "End context to_json of path #{path} is not a String")
  end

  def test_top_level_collection_count
    path = '/Race/$count'
    walker = OData::Walker.new(OSERV, path)
    assert_equal(Race, walker.end_context,
                "End context of path #{path} is wrong")
    assert_equal(true, walker.do_count,
                 "walker.do_count of path #{path} is wrong")
    assert_equal(:end, walker.status, "End status of path #{path} is wrong")
  end

  def test_top_level_collection_count_err

    walker_error_status_check "/Race/count"
    walker_error_status_check "/Race$count"
  end

  def test_top_level_collection_2
    walker_status_context_check( :end, Edition, '/Edition')
    walker_status_context_check( :end, Edition, '/Edition/')
  end

  def test_top_level_collection_1_err
    walker_error_status_check "/Editionx"
    walker_error_status_check "/editix "
    walker_error_status_check "/xyz"
  end

# test single public key read access
  def test_top_level_entity_single_pk
    %w</Race(1) /Race(1)/ /Race(1)/$count /Race('1')>.each{|path|
        walker_status_context_check( :end, Race[1], path)
    }
  end

  def test_top_level_entity_single_pk_bad_req

    walker_error_status_check '/Race(1a)'

  end

# test multiple public key read attribute access
  def test_top_level_entity_multi_pk_attr
    path = "/Edition(race_id='3',num='11')/start"
    exp_ctx = OData::Attribute.new(Edition[3,11], 'start')
    walker_status_context_check( :end, exp_ctx, path)
  end

# test multiple public key read access
  def test_top_level_entity_multi_pk
    path = "/Edition(race_id='3',num='11')"
    walker_status_context_check( :end, Edition[3,11], path)

    path = "/Edition(race_id='3',num='11')/"
    walker_status_context_check( :end, Edition[3,11], path)

    path = "/Edition(race_id='3',num='11')/$count"
    walker_status_context_check( :end, Edition[3,11], path)

    path = "/CrewMember(crew_id='1',person_id='1')"
    walker_status_context_check( :end, CrewMember[1,1], path)

# wrong entity or attribute names used
    walker_error_status_check "/Edition(race_id='3',num='11')xy"
    walker_error_status_check "/Editionxy(race_id='3',num='11')"
    walker_error_status_check "/Editionxy(race_id='3',num='11')"


# correct syntax but there's no object whith there id's
# (shall return a not found )
     walker_error_status_check "Edition(race_id='3',num='666')"
     walker_error_status_check "/Editionxy(race_id='666',num='13')"
  end

  def test_entity_attribute
    exp_ctx = OData::Attribute.new(Race[1], 'id')
    %w</Race(1)/id /Race(1)/id /Race('1')/id>.each{|path|
        walker_status_context_check( :end, exp_ctx, path)
    }

   exp_ctx = OData::Attribute.new(Race[1], 'name')
   %w</Race(1)/name /Race(1)/name /Race('1')/name>.each{|path|
        walker_status_context_check( :end, exp_ctx, path)
    }

    exp_ctx = OData::Attribute.new(Edition[3,11], 'year')
    path = "/Edition(race_id='3',num='11')/year"
    walker_status_context_check( :end, exp_ctx , path)
    path = "/Edition(race_id='3',num='11')/year/"
    walker_status_context_check( :end, exp_ctx , path)


  end

  def test_entity_attribute_err

    walker_error_status_check "/Race(1)/namexy"
    walker_error_status_check "/Race(1)/xyname"
    walker_error_status_check "/Race(1)/namexy/"
    walker_error_status_check "/Race(1)/xyname/"
    walker_error_status_check "/Race('1')/name/y"
    walker_error_status_check "/Race('1')/name\\"
    walker_error_status_check "/Race('1')/Name"


    walker_error_status_check "/Edition(race_id='3',num='11')/yearxy"
    walker_error_status_check "/Editionxy(race_id='3',num='11')/year"
    walker_error_status_check "/Edition(race_id='3',num='11')/year\\"
    walker_error_status_check "/Edition(race_id='3',num='11')/Year"
    walker_error_status_check "/Edition(race_idx='3',num='11')/year"
  end

# test what OData calls "navigation attributes" for example
# /Race(1)/Edition --> Race[1].Edition
  def test_entity_related_collection
    # ...to_many

    path = '/Race(1)/Edition'
    walker = OData::Walker.new(OSERV, path)
    assert_equal(Race[1].get_related('Edition').all, walker.end_context.all,
      "End context.all of path #{path} is wrong")
    assert_equal(Race[1].Edition, walker.end_context.all,
      "End context.all of path #{path} is wrong")
    assert_equal(Race[1].Edition.count, walker.end_context.count,
      "End context.count of path #{path} is wrong")
    assert_equal(:end, walker.status, "End status of path #{path} is wrong")
  end

# test $links
  def test_entity_related_collection_links
    # ...to_many

    path = '/Race(1)/$links/Edition'
    walker = OData::Walker.new(OSERV, path)
    assert_equal(Race[1].get_related('Edition').all, walker.end_context.all,
      "End context.all of path #{path} is wrong")
    assert(walker.do_links,
      "property do_links of walker for path #{path} is wrong")

    assert_equal(:end, walker.status, "End status of path #{path} is wrong")
  end

  # combine with count
  def test_entity_related_collection_count
    # first check our testcase data and sequel setup...
    assert_equal(Edition.where(race_id: 1).count,
                 Race[1].get_related('Edition').count)

    path = '/Race(1)/Edition/$count'
    walker = OData::Walker.new(OSERV, path)
    assert_equal(true, walker.do_count,
                 "walker.do_count of path #{path} is wrong")

    assert_equal(:end, walker.status,
                 "End status of path #{path} is wrong")
  end

  # combine with ID
  def test_entity_related_collection_id
    # ...to_many  + wrong ID; here the edition exists but is not related
    path = "/Race(1)/Edition(race_id='4',num='13')"
    walker = OData::Walker.new(OSERV, path)
    assert_equal(:error, walker.status, 'End status of path #{path} is wrong')

    # ...to_many  + wrong ID; here the edition does not even exists
    path = "/Race(1)/Edition(race_id='4',num='130')"
    walker = OData::Walker.new(OSERV, path)
    assert_equal(:error, walker.status, 'End status of path #{path} is wrong')


    # ...to_many  + correct ID
    path = "/Race(1)/Edition(race_id='1',num='5')"
    walker = OData::Walker.new(OSERV, path)
    assert_equal(Race[1].get_related('Edition')[1, 5].values,
                 walker.end_context.values,
                 "End context of path #{path} is wrong")
    assert_equal(Edition[1, 5].values, walker.end_context.values,
                 "End context of path #{path} is wrong")

    # ...to_many  + correct ID + Attrib
    path = "/Race(1)/Edition(race_id='1',num='5')/year"
    walker = OData::Walker.new(OSERV, path)
    assert_equal(Race[1].get_related('Edition')[1, 5].year,
                 walker.end_context.value,
                 "End context of path #{path} is wrong")
    assert_equal(Edition[1, 5].year, walker.end_context.value,
                 "End context of path #{path} is wrong")
  end

  def test_entity_related_entity
    #  ...to_one
    path = '/Race(5)/CrewType'
    walker = OData::Walker.new(OSERV, path)
    assert_equal(Race[5].get_related_entity('CrewType'), walker.end_context,
                 "End context.all of path #{path} is wrong")
    assert_equal(Race[5].CrewType, walker.end_context,
                 "End context.all of path #{path} is wrong")
    assert_equal(CrewType[1], walker.end_context,
                 "End context.all of path #{path} is wrong")
    assert_equal(:end, walker.status, "End status of path #{path} is wrong")
  end

  # the navigated entity race type of Race is renamed (Race(1)/rtype), but the
  # base race type entity has the default (class/type) name RaceType
  def test_entity_related_entity_renamed_2
    #  ...to_one
    path = '/Race(5)/rtype'
    walker = OData::Walker.new(OSERV, path)
#    assert_equal(Race[5].get_related_entity('RaceType'), walker.end_context,
#                 "End context.all of path #{path} is wrong")
    assert_equal(Race[5].get_related_entity('rtype'), walker.end_context,
                 "End context.all of path #{path} is wrong")

    assert_equal(Race[5].rtype, walker.end_context,
                 "End context.all of path #{path} is wrong")
    assert_equal(RaceType[3], walker.end_context,
                 "End context.all of path #{path} is wrong")
    assert_equal(:end, walker.status, "End status of path #{path} is wrong")
  end

  # the entity periodicity type of Race is not renamed same as
  # the base periodicity type entity
  # /periodicity_type and /Race(1)/PeriodicityType
  def test_entity_related_entity_renamed

    path = '/Race(10)/PeriodicityType'
    walker = OData::Walker.new(OSERV, path)

    assert_equal(Race[10].get_related_entity('PeriodicityType'),
                 walker.end_context,
                 "End context.all of path #{path} is wrong")
    assert_equal(Race[10].PeriodicityType, walker.end_context,
                 "End context.all of path #{path} is wrong")
    assert_equal(PeriodicityType[3], walker.end_context,
                 "End context.all of path #{path} is wrong")
    assert_equal(:end, walker.status, "End status of path #{path} is wrong")
  end

  # combine related entity and an attrib
  def test_entity_related_entity_name
    #  ...to_one attrib type Number
    path = '/Race(4)/rtype/description'
    walker = OData::Walker.new(OSERV, path)
    assert_equal(Race[4].get_related_entity('rtype').description,
                 walker.end_context.value,
                 "End context.all of path #{path} is wrong")
    assert_equal(Race[4].rtype.description, walker.end_context.value,
                 "End context.all of path #{path} is wrong")
    assert_equal(RaceType[2].description, walker.end_context.value,
                 "End context.all of path #{path} is wrong")
    assert_equal(:end, walker.status, "End status of path #{path} is wrong")

    #  ...to_one attrib type String
    path = "/Edition(race_id='1',num='8')/Race/name"
    walker = OData::Walker.new(OSERV, path)
    assert_equal(Edition[1,8].get_related_entity('Race').name,
                 walker.end_context.value,
                 "End context.all of path #{path} is wrong")
    assert_equal(Edition[1,8].Race.name, walker.end_context.value,
                 "End context.all of path #{path} is wrong")
    assert_equal(Race[1].name, walker.end_context.value,
                 "End context.all of path #{path} is wrong")

    assert_equal(:end, walker.status, "End status of path #{path} is wrong")
  end

end


