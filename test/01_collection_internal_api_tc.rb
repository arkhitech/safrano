#!/usr/bin/env ruby


require_relative './test_saildb.rb' # Sail-DB testdata


# only for testing
module Collection_Testing
  refine Race do
    def Race.params=(p)
      @params = p
    end
  end
end
using Collection_Testing

class TC_MyTest < SailDBTestII

  def test_base
    assert_respond_to(Race, :each)
    assert_respond_to(Race, :first)
    assert_respond_to(Race, :last)

    assert_kind_of(Race, Race.first)
    assert_kind_of(Race, Race.last)
  end

  def test_params_top_filter
    # according to spec, top=0 schould be considered malformed
    Race.params = { '$top' => '0' }
    e = Race.check_url_params
    assert_equal(OData::BadRequestError, e)

    Race.params = { '$top' => '1xycy2' }
    e = Race.check_url_params
    assert_equal(OData::BadRequestError, e)
    # ok
    Race.params = { '$top' => '7' }
    e = Race.check_url_params
    assert_equal(nil, e)
  end

  def test_params_skip_filter
    # according to spec, skip<0 schould be considered malformed
    Race.params = { '$skip' => '-1' }
    e = Race.check_url_params
    assert_equal(OData::BadRequestError, e)

    Race.params = { '$skip' => '1xycy2' }
    e = Race.check_url_params
    assert_equal(OData::BadRequestError, e)
    # ok
    Race.params = { '$skip' => '0' }
    e = Race.check_url_params
    assert_equal(nil, e)

    Race.params = { '$skip' => '7' }
    e = Race.check_url_params
    assert_equal(nil, e)
  end

  def test_skip_filter
    n = Race.to_a.size
    assert(n > 7)

    Race.params = { '$skip' => '7' }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert_equal(n - 7, cnt)
    assert_kind_of(Race, y.to_a.first)

    Race.params = { '$skip' => '2' }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert_equal(n - 2, cnt)
    assert_kind_of(Race, y.to_a.first)
    assert_kind_of(Race, y.to_a.last)
  end

  def test_top_filter
    Race.params = { '$top' => '7' }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert_equal(7, cnt)
    assert_kind_of(Race, y.to_a.first)

    Race.params = { '$top' => '2' }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert_equal(2, cnt)
    assert_kind_of(Race, y.to_a.first)
    assert_kind_of(Race, y.to_a.last)
  end

# request more as what is available
  def test_top_filter_too_much
    assert Race.to_a.size < 999
    Race.params = { '$top' => '999' }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert_equal(Race.to_a.size, cnt)
    assert_kind_of(Race, y.to_a.first)
  end

  def test_top_skip_filter
    Race.params = { '$top' => '10' }
    a = race_odata_get_apply_params

    Race.params = { '$skip' => '9', '$top' => '10' }
    b = race_odata_get_apply_params

    Race.params = { '$skip' => '0', '$top' => '19' }
    c = race_odata_get_apply_params

    # first record
    assert_equal(Race.first, a.to_a.first)
    assert_equal(Race.first, c.to_a.first)
    # ninth record of a is first record of b
    assert_equal(a.to_a.last, b.to_a.first)
    # last record of b is last record of c
    assert_equal(b.to_a.last, c.to_a.last)
  end

  def test_substring_filter
    Race.params = { '$filter' => "substringof('Race',name)" }

    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert(cnt >= 1)
    assert_kind_of(Race, y.to_a.first)
    y.each { |r| assert_match(/Race/, r.name) }

    Race.params = { '$filter' => "substringof('dfsyxz³³³',name)" }

    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert_equal(0, cnt)
    assert_equal([], y.to_a)
  end

  def test_equal_filter
    Race.params = { '$filter' => 'id Eq 4' }

    y = race_odata_get_apply_params

    cnt = y.to_a.size
    assert_equal(1, cnt)
    assert_kind_of(Race, y.to_a.first)
    assert_equal(Race[4], y.to_a.first)

    Race.params = { '$filter' => "name eq 'Route du Rhum'" }

    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert_equal(1, cnt)
    assert_kind_of(Race, y.to_a.first)
    assert_equal(Race[3], y.to_a.first)
  end

  def test_le_filter
    n = Race.to_a.size
    assert(n > 7)

    Race.params = { '$filter' => 'id Le 10' }

    y = race_odata_get_apply_params
    y.each { |r| assert (r.id <= 10) }
  end
  def test_not_equal_filter
    n = Race.to_a.size
    assert(n > 7)

    Race.params = { '$filter' => 'id Ne 1' }

    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert_equal(n - 1, cnt)
    assert_kind_of(Race, y.to_a.first)
    assert_equal(Race[2], y.to_a.first)
    assert_false(y.to_a.include?(Race[1]))

    Race.params = { '$filter' => "name ne 'The Race'" }

    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert_equal(n - 1, cnt)
    assert_kind_of(Race, y.to_a.first)
    assert_equal(Race[1], y.to_a.first)
    assert_false(y.to_a.include?(Race[7]))
  end

  def test_order_direct
    n = Race.to_a.size
    Race.params = { '$orderby' => 'id desc' }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert_equal(n, cnt)
    # TODO: >= nil ?
    #        assert(y.first.year >= y.last.year)
  end

  def test_order_related
    n = Race.to_a.size
    Race.params = { '$orderby' => 'CrewType/description desc' }
    y = race_odata_get_apply_params

    cnt = y.to_a.size
    assert_equal(cnt, n)

    Race.params = { '$orderby' => 'CrewType/description' }
    z = race_odata_get_apply_params
    cnt = z.to_a.size

    assert_equal(cnt, n)

    assert_equal(y, z.reverse)
  end

  def test_order_direct_filter_related_complex
    n = Race.to_a.size
    Race.params = { '$orderby' => 'id desc',
                    '$filter' => "substringof('Stop',CrewType/description) or substringof('stop',CrewType/description)" }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert(cnt <= n)
    y.each { |r| assert_match(/stop|Stop/, r.CrewType.description) }

    # DONE: OR -> or   (OR is seen as a ruby constant ? )
    n = Race.to_a.size
    Race.params = { '$orderby' => 'id desc',
                    '$filter' => "substringof('Stop',CrewType/description) OR substringof('stop',CrewType/description)" }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert(cnt <= n)
    y.each { |r| assert_match(/stop|Stop/, r.CrewType.description) }
  end

  def test_order_direct_filter_related
    n = Race.to_a.size
    Race.params = { '$orderby' => 'id desc',
                    '$filter' => "substringof('Stop',CrewType/description)" }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert(cnt <= n)
    y.each { |r| assert_match(/Stop/, r.CrewType.description) }

    ## substring without quotes?
    Race.params = { '$orderby' => 'id desc',
                    '$filter' => "substringof(Stop,CrewType/description)" }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert(cnt <= n)
    y.each { |r| assert_match(/Stop/, r.CrewType.description) }
  end

  def test_order_related_filter_direct
    Race.params = { '$orderby' => 'CrewType/description',
                    '$filter' => "substringof('Race',name)" }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert(cnt >= 1)
    assert_kind_of(Race, y.to_a.first)
    y.each { |r| assert_match(/Race/, r.name) }
  end

  def test_order_related_filter_related
    Race.params = { '$orderby' => 'CrewType/description',
                    '$filter' => 'substringof(Atla,rtype/description)' }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert(cnt >= 1)
    assert_kind_of(Race, y.to_a.first)
    y.each { |r| assert_match(/Atla/, r.rtype.description )}
  end

  # not sure if this test-case is correct, but at least
  # one of my test-ui5 client app sent such kind of requests
  # so we try to support them
  def test_expand_one_level
    Race.params = { '$expand' => 'CrewType',
                    '$filter' => 'substringof(Atla,rtype/description)' }
    y = race_odata_get_apply_params
    cnt = y.to_a.size
    assert(cnt >= 1)
    assert_kind_of(Race, y.to_a.first)
    y.each { |r| assert_match(/Atla/, r.rtype.description) }
  end

  def race_odata_get_apply_params
    Race.initialize_dataset
    Race.check_url_params
    Race.odata_get_apply_params
  end

end
