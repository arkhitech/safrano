#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require_relative './test_saildb.rb' # Sail-DB testdata

class CollectionSailTC < SailDBTest

  # Check simple collection get
  def test_get
    get '/Race'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)
  end

  # Check simple non exisiting collection get --> 404
  def test_simple_nonexisting_coll_get
    get '/Rats'
    assert last_response.not_found?
  end

  # Check simple collection get with ordering
  def test_get_order
    get '/Race?$orderby=name'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_set_equal(expected_tab, @jresd_coll)
    assert_sequel_coll_ord_asc_by(@jresd_coll){|jresd| jresd[:name] }
  end

# Check simple collection get with selecting only name
  def test_get_select_1
    get '/Race?$select=name'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_sel_set_equal(expected_tab, @jresd_coll, [:name])
  end

# Check simple collection get with selecting only name,id
  def test_get_select_2
    get '/Race?$select=name,id'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_sel_set_equal(expected_tab, @jresd_coll, [:name,:id])
  end

  # Check simple collection get with multi ordering

  def test_get_multiorder
    expected_tab = Race.all
    get '/Race?$orderby=name asc, id'
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_set_equal(expected_tab, @jresd_coll)
    assert_sequel_coll_ord_asc_by(@jresd_coll){|jresd| [jresd[:name], jresd[:id]] }

    get '/Race?$orderby=name asc,id'
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_set_equal(expected_tab, @jresd_coll)
    assert_sequel_coll_ord_asc_by(@jresd_coll){|jresd| [jresd[:name], jresd[:id]] }

    get '/Race?$orderby=name asc,id desc'
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_set_equal(expected_tab, @jresd_coll)
    assert_sequel_coll_ord_asc_by(@jresd_coll){|jresd| [jresd[:name], (0-jresd[:id])] }

  end

  # Check simple collection get with error in ordering param --> 400 bad req
  def test_get_order_err
    get '/Race?$orderby=cow'
    assert last_response.bad_request?
  end

  # Check simple collection get with error in ordering param --> 400 bad req
  # multiple orderby's some ok, some wrong
  def test_get_multiorder_err
    get '/Race?$orderby=name asc, cow'
    assert last_response.bad_request?
  end

  # Check simple collection get with error in ordering direction --> 400 bad req
  def test_get_order_dir_err
#   typo: correct is asc.... acs is 400
    get '/Race?$orderby=name acs'
    assert last_response.bad_request?
  end

  # Check simple collection get with error in ordering direction --> 400 bad req
  # multiple orderby's some ok, some wrong
  def test_get_multiorder_dir_err
#   typo: correct is desc.... decs is 400
    get '/Race?$orderby=name asc,id decs'
    assert last_response.bad_request?
  end

# test inlinecount
  def test_inlinecount
    get '/Race?$inlinecount=allpages'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_equal expected_tab.count, @jresd['__count']

    get '/Race?$inlinecount=none'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_nil @jresd['__count']

    get '/Race?$inlinecount=allpages&$filter=crew_type Eq 1'
    expected_tab = Race.where(crew_type: 1).all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_equal expected_tab.count, @jresd['__count']


    get '/Race?$top=2&$inlinecount=allpages&$filter=crew_type Eq 1'
    expected_tab = Race.where(crew_type: 1).all
    assert_last_resp_coll_parseable_ok('Race', 2)
    assert_equal expected_tab.count, @jresd['__count']

  end

# test inlinecount error handling
  def test_inlinecount_err
    #  typo: forgot an s
    get '/Race?$inlinecount=allpage'
    assert last_response.bad_request?
  end

  # Check a collection with a renamed entity name (not equal the class name)
  def test_renamed_get
    get '/periodicity_type'
    expected_tab = PeriodicityType.all
    assert_last_resp_coll_parseable_ok('PeriodicityType', expected_tab.count)
    assert_pertype_set_equal(expected_tab, @jresd_coll)
  end

  # Check filter
  def test_filter
    get '/Race?$filter=crew_type Eq 1'
    assert last_response.ok?
    @resh = nil
    assert_nothing_raised do
      @resh = JSON.parse last_response.body
    end
    assert_equal(Race.where(crew_type: 1).all.size, @resh['d']['results'].size)
  end

  # Check filter with wrong field name --> 400
  def test_filter_err
    get '/Race?$filter=cow Eq 1'
    assert last_response.bad_request?
  end

  def test_filter_strin_not_equal
    get "/Race?$filter=name ne 'test'"
    assert last_response.ok?
  end

  # Check simple collection
  # multiple filter's all ok
  def test_get_multifilter_ok
    get "/Race?$filter=name ne 'test' AND id EQ 1"
    assert last_response.ok?
  end

  # Check simple collection get with error in filter param --> 400 bad req
  # multiple filter's some ok, some wrong
  def test_get_multifilter_err
    get "/Race?$filter=name ne 'test' OR cow EQ 1"
    assert last_response.bad_request?
  end

  # Check simple collection get with error in filter op --> 400 bad req
  def test_get_filter_op_err
#   typo: correct is NE.... neq is 400
    get "/Race?$filter=name neq 'test'"
    assert last_response.bad_request?

#   typo: first part before AND forgotten.... --> 400
    get "/Race?$filter=AND name eq 'test'"
    assert last_response.bad_request?

    #   typo: first part before AND forgotten and attribute forgotten as well
    # .... --> 400
    get "/Race?$filter=AND name 'test'"
    assert last_response.bad_request?

    get "/Race?$filter=AND name 'test'"
    assert last_response.bad_request?

  end

  # Check simple collection get with error in operator --> 400 bad req
  # multiple filter's some ok, some wrong
  def test_get_multifilter_op_err
#   typo: correct is NE.... neq is 400
    get "/Race?$filter=name neq 'test' OR id EQ 1"
    assert last_response.bad_request?
  end

    #   typo: logical AND/OR repetition
    # .... --> 400
  def test_get_multifilter_andor_err
    get "/Race?$filter=name eq 'test' AND OR id EQ 1"
    assert last_response.bad_request?

    # test that it handles quotes well
    get "/Race?$filter=name eq 'AND OR'"
    assert last_response.ok?

    # test that it handles escaped quotes well
    get "/Race?$filter=name eq 'AND ''OR'''"
    assert last_response.ok?
  end

  # Check startswith filter
  def test_startwith_filter_2
    get %{/Race?$filter=startswith(name,'B')}
    expected_tab = Race.where(Sequel.like(:name, 'B%')).all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)
  end

# Check startswith filter with syntax error (unbalanced quotes)
  def test_startwith_filter_syntax_err
    get %{/Race?$filter=startswith(name,B')}
    assert last_response.bad_request?
  end

  # Check startswith filter with a non-ascii char
  def test_startwith_filter_1
    str = URI.encode_www_form_component("'Vendé'")
    get "/Race?$filter=startswith(name,#{str})"
    expected_tab = Race.where(Sequel.like(:name, 'Vendé%')).all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)
  end

  # Check startswith filter with protected quotes in string arg
  def test_startwith_filter_protected_quotes
    #record #13 has name = Test-Race 'record' with quotes
    str = URI.encode_www_form_component("'Test-Race ''record'' with quo'")
#    str = "'Test-Race ''record'' with quo'"
    get "/Race?$filter=startswith(name,#{str})"
    expected_tab = Race.where(Sequel.like(:name,
                                          "Test-Race 'record' with quo%")).all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)
  end

  # Check startswith filter with a empty result set
  def test_startwith_filter_0

    get "/Race?$filter=startswith(name,'xyzddf')"
    expected_tab = Race.where(Sequel.like(:name, 'xyzddf%')).all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)
  end

  # check length() filter
  def test_length_filter
    get "/Person?$filter=length(first_name) eq 5"
    expected_tab = Person.where(Sequel.char_length([:first_name]) => 5 ).all
    assert_last_resp_coll_parseable_ok('Person', expected_tab.count)
    assert_person_set_equal(expected_tab, @jresd_coll)
  end

  def test_length_filter_or
    get "/Person?$filter=length(first_name) eq 5 or length(last_name) eq 4"
    expected_tab = Person.where(Sequel.char_length([:first_name]) => 5 )
                         .or(Sequel.char_length([:last_name]) => 4 ).all
    assert_last_resp_coll_parseable_ok('Person', expected_tab.count)
    assert_person_set_equal(expected_tab, @jresd_coll)
  end

  def test_length_filter_concat_nested
    get "/Person?$filter=length(concat(concat(first_name,' '),last_name)) eq 10 "

    # brute force filter with ruby
    expected_tab = Person.all.select{|p| (p.first_name.length +  p.last_name.length) == 9}
    assert_last_resp_coll_parseable_ok('Person', expected_tab.count)
    assert_person_set_equal(expected_tab, @jresd_coll)
  end

  # check trim func
  def test_trim_filter
    get %Q</BoatClass?$filter=trim(name) eq '__test_trim__'>
    #brute force ruby filter SQL trim --> ruby strip
    expected_tab = BoatClass.all.select{|bc| bc.name.strip == '__test_trim__'}

    assert_last_resp_coll_parseable_ok('BoatClass', expected_tab.count)
    assert_sequel_coll_set_equal expected_tab, @jresd_coll
  end

  # check tolower/toupper funcs
  def test_tolower_filter

    get %Q</Person?$filter=tolower(first_name) eq 'armel'>
    expected_tab = [Person[8], Person[42]]

    assert_last_resp_coll_parseable_ok('Person', expected_tab.count)
    assert_equal 8, @jresd_coll.first[:id]
    assert_equal 42, @jresd_coll[1][:id]
  end


  def test_tolower_toupper_filter
    strlo = URI.encode_www_form_component('loïck')
#    strup = URI.encode_www_form_component('LE CLÉAC’H')  ### doesnt work, maybe due to special char
    strup = 'MORVAN'
    get %Q</Person?$filter=tolower(first_name) eq '#{strlo}' or toupper(last_name) eq '#{strup}'>
    expected_tab = [Person[12], Person[33]]

    assert_last_resp_coll_parseable_ok('Person', expected_tab.count)
    assert_equal 12, @jresd_coll.first[:id]
    assert_equal 33, @jresd_coll[1][:id]
  end

  # Check count
  def test_count
    get '/Race/$count'
    assert last_response.ok?
    @resh = nil
    assert_nothing_raised do
      @resh = last_response.body.to_i
    end
    assert_equal(Race.all.size, @resh)
  end

  # Check count + filter
  def test_count_filter
    get '/Race/$count?$filter=crew_type Eq 1'
    assert last_response.ok?
    @resh = nil
    assert_nothing_raised do
      @resh = last_response.body.to_i
    end
    assert_equal(Race.where(crew_type: 1).all.size, @resh)
  end

  # Check count + startswith filter
  def test_count_startwith_filter
    get "/Race/$count?$filter=startswith(name,'B')"
    assert last_response.ok?
    @resh = nil
    assert_nothing_raised do
      @resh = last_response.body.to_i
    end
    assert_equal(Race.where(Sequel.like(:name, 'B%')).all.size, @resh)
  end

  #  (( publish_model PeriodicityType, :periodicity_type ))
  # check GET with entity having a non default entity-set-name
  def get_with_non_default_es_name
    get '/periodicity_type'
    assert_last_resp_coll_parseable_ok('PeriodicityType', PeriodicityType.all.count)
    # check uri metadata
    assert_metadata_uri(PeriodicityType.first, @jresd_coll.first){|enty| "periodicity_type(#{enty.id})"}
    assert_metadata_uri(PeriodicityType.last, @jresd_coll.last){|enty| "periodicity_type(#{enty.id})"}
  end
  
end
