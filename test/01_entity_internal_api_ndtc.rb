#!/usr/bin/env ruby
# The $expand parameter functionality is tested here
# REMOVED in safrano-0.4.2 because too low level and need too frequently rework

#require 'pp'
#require 'test/unit'

#require_relative '../lib/odata/entity.rb'
#require_relative '../lib/safrano/service.rb'

  #URB_TEST =  'http://localhost:9494'
#$oserv = OData::ServiceBase.new do

  #title  'Test OData API'
  #name  'TestService'
  #namespace  'ODataTest'

  #path_prefix '/'
#end

#class Entity_base_test
  #include OData::Entity
  #include OData::NonMediaEntity
  #extend OData::EntityClassSinglePK

  #attr_accessor :values

  #def initialize
    #@values = { 'f1' => 'value of f1 abc', 'f2' => 3 }
  #end

  ## provided by Sequel normally
  #def self.table_name
    #'dummy_table'
  #end
  
  ## method not coverred here, but requires Sequel functionality--> just override
  #def set_relation_info(parent,childname)
  #end
  
  ## mocked for testing uri
  #def self.entity_set_name
    #'dummy_esname'
  #end
  
  #def pk_uri
    #'dummy_pk'
  #end
  
  #def self.newChildSingle
    #childSingle = Entity_simple_values.new
    #childSingle.values = { 'o1' => 'value of o1 xyz', 'o2' => 222,
                           #'o3' => 'blablub' }
    #childSingle
  #end

  #def self.newChildSingleDeep
    ## returns an entity having  e->childSingle2 nesting

    #child_l2 = Entity_simple_values.new
    #child_l2.values = { 'x1' => 'this is l2-x1', 'x2' => 'this is l2-x2' }

    #child_l1 = Entity_Rel_X_to_1.new
    #child_l1.values = { 'b1' => 'this is l1-b1', 'b2' => 'this is l1-b2' }
    #child_l1.nav_values = { childSingle2: child_l2 }

    #child_l1
  #end

  #def self.newChildList
    #childList = [Entity_simple_values.new]
    #childList[0].values = { 'v1' => 'value 1 of child 1', 'v2' => 'value 2 of child 1', 'v3' => 'blablub' }

    #childList << Entity_simple_values.new
    #childList[1].values = { 'v1' => 'value 1 of child 2', 'v2' => 'value 2 of child 2', 'v3' => 'cxlub' }
    #childList << Entity_simple_values.new
    #childList[2].values = { 'v1' => 'value 1 of child 3', 'v2' => 'value 2 of child 3', 'v3' => 'blxb' }
    #childList
  #end
#end

## x to one relationship
#class Entity_Rel_X_to_1 < Entity_base_test
  #attr_accessor :nav_values

  ## hardcoded x-to-1 child method for testing.
  ## Normally this is provided by Sequel Model
  #def childSingle
    #@nav_values[:childSingle]
  #end

  ## this ones are for testing deep relations
  #def childDeep
    #@nav_values[:childDeep]
  #end

  #def childSingle2
    #@nav_values[:childSingle2]
  #end
#end

## x to N relationship
#class Entity_Rel_X_to_N < Entity_base_test
  #attr_accessor :nav_values
  #attr_accessor :nav_coll

  ## hardcoded x-to-1 child method for testing.
  ## Normally this is provided by Sequel Model
  #def childSingle
    #@nav_values[:childSingle]
  #end

  #def childList
    #@nav_coll[:childList]
  #end

  ## this ones are for testing deep relations
  #def childDeep
    #@nav_values[:childDeep]
  #end

  #def childSingle2
    #@nav_values[:childSingle2]
  #end
#end

#class Entity_simple_values < Entity_base_test
#end

## Tests on the current hierarchy level (ie. 0)
## as a module for cumulative re-using
#module TM_Level_0
  #attr_accessor :entity
  #def asserts_entity_odata_h_0(x)
    #assert_kind_of(Hash, x, message = 'Kind of @entity.to_odata_h is wrong ')
    #assert_equal(3, x['f2'])
    #assert_equal(@entity.values['f1'], x['f1'])
  #end

  #def test_to_odata_h_0
    #x = $oserv.get_entity_odata_h(entity: @entity, uribase: URB_TEST)
    #asserts_entity_odata_h_0(x)
  #end
#end

## Testing x-to-1 childs
## as a module for cumulative re-using
#module TM_Level_1_1
  #attr_accessor :childSingle
  #def test_get_related_entity_1
    #xr = @entity.get_related_entity('childSingle')
    #assert_equal(@childSingle, xr)
  #end

  #def asserts_entity_odata_h_1(xh)
    #assert_kind_of(Hash, xh, message = "Kind of @entity get_entity_odata_h expand: 'childSingle' is wrong ")
    #assert_equal(222, xh['o2'])

    #assert_equal(@childSingle.values['o1'], xh['o1'])
    #assert_equal(@childSingle.values['o3'], xh['o3'])
  #end

  #def test_to_odata_h_1
    #x = $oserv.get_entity_odata_h(entity: @entity,
                                  #expand: 'childSingle',
                                  #uribase: URB_TEST)
    #asserts_entity_odata_h_0(x)
    ## test the linked child

    #xh = x['childSingle']
    #asserts_entity_odata_h_1(xh)
  #end

  ## same without expand
  #def test_to_odata_h_1_ne
    #x = $oserv.get_entity_odata_h(entity: @entity, uribase: URB_TEST)
    #asserts_entity_odata_h_0(x)
    ## test expand=false --> the child attribute should be '__deferred'
    #xh = x['childSingle']
    #assert_kind_of(Hash, x, message = "Kind of x['childSingle']  is wrong ")
    #assert_equal({ '__deferred' =>
                   #{ 'uri' => "#{URB_TEST}/dummy_esname(dummy_pk)/childSingle" } },
                   #xh)
  #end
#end
## Testing x-to-1 childs with deep childs
## as a module for cumulative re-using
#module TM_Level_2_1
  #attr_accessor :childSingle
  #attr_accessor :childDeep
  #def test_get_related_entity_2
    #xr = @entity.get_related_entity('childDeep')
    #assert_equal(@childDeep, xr)
  #end

  #def asserts_entity_odata_h_2(xh)
    #assert_kind_of(Hash, xh, message = "Kind of @entity get_entity_odata_h expand: 'childDeep' is wrong ")

    #assert_equal(@childDeep.values['a1'], xh['a1'])
    #assert_equal(@childDeep.values['a2'], xh['a2'])
  #end

  #def asserts_entity_odata_h_2_deep(xh)
    #assert_kind_of(Hash, xh, message = "Kind of @entity get_entity_odata_h expand: 'childDeep/childSingle2' is wrong ")

    #cs2 = @childDeep.nav_values[:childSingle2]
    #assert_equal(cs2.values['x1'], xh['x1'])
    #assert_equal(cs2.values['x2'], xh['x2'])
  #end

  #def test_to_odata_h_2
    #x = $oserv.get_entity_odata_h(entity: @entity,
                                  #expand: 'childDeep', uribase: URB_TEST)
    #asserts_entity_odata_h_0(x)
    ## test the linked child

    #xh = x['childDeep']
    #asserts_entity_odata_h_2(xh)
  #end

  ## same without expand
  #def test_to_odata_h_2_ne
    #x = $oserv.get_entity_odata_h(entity: @entity, uribase: URB_TEST)
    #asserts_entity_odata_h_0(x)
    ## test expand=false --> the child attribute should be '__deferred'
    #xh = x['childDeep']
    #assert_kind_of(Hash, xh, message = "Kind of x['childDeep']  is wrong ")
    #assert_equal({ '__deferred' => { 'uri' => "#{URB_TEST}/dummy_esname(dummy_pk)/childDeep" } }, xh)
  #end

  ##  check     deep
  #def test_to_odata_h_deep
    #x = $oserv.get_entity_odata_h(entity: @entity,
                                  #expand: 'childDeep/childSingle2',
                                  #uribase: URB_TEST )

    #xh = x['childDeep']
    #asserts_entity_odata_h_2(xh)
    #xh = x['childDeep']['childSingle2']
    #asserts_entity_odata_h_2_deep(xh)
  #end
#end
#class TC_Entity_simple_values < TCWithoutDB
  #attr_accessor :entity
  #include TM_Level_0

  #def setup
    #@entity = Entity_simple_values.new
  #end

  #def test_entity_type
    #assert_equal('Entity_simple_values', @entity.type_name)
  #end
#end

## x to one relationship
#class TC_Entity_Rel_X_to_1 < TCWithoutDB
  #include TM_Level_0
  #include TM_Level_1_1

  #def setup
    #@entity = Entity_Rel_X_to_1.new

    #@childSingle = Entity_base_test.newChildSingle

    #@entity.nav_values = { childSingle: @childSingle }
  #end

  #def test_entity_type
    #assert_equal('Entity_Rel_X_to_1', @entity.type_name)
  #end
#end

## Testing x-to-N childs
## as a module for cumulative re-using
#module TM_Level_1_N
  #attr_accessor :childList

  #def asserts_entity_odata_h_1n(xh)
    #assert_kind_of(Array, xh, message = "Kind of @entity get_entity_odata_h expand: 'childList' is wrong ")
    #xh.each do |eh|
      #assert_kind_of(Hash, eh, message = "Kind of 'childList' element is wrong ")
    #end
    #assert_equal('blxb', xh[2]['v3'])

    #assert_equal(@childList[0].values['v1'], xh[0]['v1'])
    #assert_equal(@childList[0].values['v2'], xh[0]['v2'])
    #assert_equal(@childList[0].values['v3'], xh[0]['v3'])

    #assert_equal(@childList[1].values['v1'], xh[1]['v1'])
    #assert_equal(@childList[1].values['v2'], xh[1]['v2'])
    #assert_equal(@childList[1].values['v3'], xh[1]['v3'])

    #assert_equal(@childList[2].values['v1'], xh[2]['v1'])
    #assert_equal(@childList[2].values['v2'], xh[2]['v2'])
    #assert_equal(@childList[2].values['v3'], xh[2]['v3'])
  #end

  #def test_to_odata_h_1n
    #x = $oserv.get_entity_odata_h(entity: @entity, expand: 'childList', uribase: URB_TEST)
    #asserts_entity_odata_h_0(x)
    ## test the linked child

    #xh = x['childList']

    ## Warning.... v2 only
    #asserts_entity_odata_h_1n(xh['results'])
  #end

  ## same without expand (or other attribs expanded)
  #def test_to_odata_h_1n_ne
    #x = $oserv.get_entity_odata_h(entity: @entity, uribase: URB_TEST)
    #asserts_entity_odata_h_0(x)
    ## test expand=false --> the child attribute should be '__deferred'
    #xh = x['childList']
    #assert_kind_of(Hash, x, message = "Kind of x['childList']  is wrong ")
    #assert_equal({ '__deferred' => { 'uri' => "#{URB_TEST}/dummy_esname(dummy_pk)/childList" } }, xh)

    ## other attrib expanded
    #x = $oserv.get_entity_odata_h(entity: @entity, expand: 'childSingle', uribase: URB_TEST)
    #asserts_entity_odata_h_0(x)
    #xh = x['childSingle']
    #asserts_entity_odata_h_1(xh)
    ## test expand=false --> the child attribute should be '__deferred'
    #xh = x['childList']
    #assert_kind_of(Hash, xh, message = "Kind of x['childList']  is wrong ")
    #assert_equal({ '__deferred' => { 'uri' => "#{URB_TEST}/dummy_esname(dummy_pk)/childList" } }, xh)
  #end

  ## check multiple expands as well
  #def test_to_odata_h_1n_multi_expand
    #x = $oserv.get_entity_odata_h(entity: @entity,
                                  #expand: 'childSingle,childList', uribase: URB_TEST)
    #asserts_entity_odata_h_0(x)

    #xh = x['childSingle']
    #asserts_entity_odata_h_1(xh)

    #xh = x['childList']
    #asserts_entity_odata_h_1n(xh['results'])  # Warning v2 only
    ## the other way around and with spaces...
    #x = $oserv.get_entity_odata_h(entity: @entity,
                                  #expand: 'childList, childSingle ', uribase: URB_TEST)
    #asserts_entity_odata_h_0(x)

    #xh = x['childSingle']
    #asserts_entity_odata_h_1(xh)

    #xh = x['childList']
    #asserts_entity_odata_h_1n(xh['results']) # Warning v2 only
  #end
#end

## x to N relationship and deep nesting as well
#class TC_Entity_Rel_X_to_N < TCWithoutDB
  #include TM_Level_0
  #include TM_Level_1_1
  #include TM_Level_2_1 # deep
  #include TM_Level_1_N

  
  #def setup
    #@entity = Entity_Rel_X_to_N.new

    #@childSingle = Entity_base_test.newChildSingle
    #@childDeep = Entity_base_test.newChildSingleDeep
    #@entity.nav_values = { childSingle: @childSingle, childDeep: @childDeep }

    #@childList = Entity_base_test.newChildList

    #@entity.nav_coll = { childList: @childList }
  #end

  #def test_entity_type
    #assert_equal('Entity_Rel_X_to_N', @entity.type_name)
  #end
#end
