
require_relative 'test_helper.rb'
require_relative './test_chinook_db.rb' # Chinook-DB testdata
require 'time'

class ChinookEntityTest < ChinookDBTest

  # minimal test our test-model/service
  def test_it_has_service
    get '/'
    assert last_response.ok?
  end

  def test_it_has_metadata
    get '/$metadata'
    assert last_response.ok?
  end

  # minimally test model a bit
  def test_model
    get '/Album(1)'
    assert last_response.ok?
    assert_equal '2.0', last_response.headers['DataServiceVersion']

    get '/Album(1)/artist'
    assert last_response.ok?

    get '/Customer(1)/salesrep'
    assert last_response.ok?

    get '/Customer(1)/techsupport'
    assert last_response.ok?
    assert_nothing_raised do
      @resh = JSON.parse last_response.body
    end
    assert_equal(Employee[10].LastName, @resh['d']['LastName'])

    get '/Customer(1)/techsupport/LastName/$value'
    assert last_response.ok?
    assert_equal(Employee[10].LastName, last_response.body)

    get "/Invoice(411)/invoice_items"
    assert last_response.ok?

  end


  # minimaly test iso formating of date
  # theres probably some work remaining when it comes to the TZ
  
  # both are valid 
  EXPT = [Time.new(2010,03,11,0,0,0,0).iso8601,    # --> 2010-03-11T00:00:00+00:00
          Time.utc(2010,03,11,0,0,0).iso8601  ]    # --> 2010-03-11T00:00:00Z
  def test_entity_date_type
    get '/Invoice(98)'
    assert_last_resp_enty_ok_no_cast('Invoice')

    assert EXPT.include?(@jrawd[:InvoiceDate])

  end

  def test_entity_attrib_date_type
    get '/Invoice(98)/InvoiceDate'
    assert_last_resp_enty_ok_no_cast
    assert EXPT.include?(@jrawd[:InvoiceDate])

  end

  def test_entity_value_date_type
    get '/Invoice(98)/InvoiceDate/$value'

    assert last_response.ok?
    
    assert EXPT.include?(last_response.body), last_response.body

  end

end


