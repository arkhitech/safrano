#!/usr/bin/env ruby

require_relative './test_saildb.rb' # Sail-DB testdata

class EntityRelation_TC < SailDBTest

  # minimal test our test-model/service
  def test_it_has_service
    get '/'
    assert last_response.ok?
  end

  def test_it_has_metadata
    get '/$metadata'
    assert last_response.ok?
  end

  # minimally test model a bit
  def test_sail_model
    get '/Race(1)'
    assert last_response.ok?
    assert_equal '2.0', last_response.headers['DataServiceVersion']

    get '/Race(6)'
    assert last_response.ok?

    get '/Person(1)'
    assert last_response.ok?

    get '/Person(2)'
    assert last_response.ok?

    get '/Crew(1)'
    assert last_response.ok?

    get "/CrewMember(crew_id='1',person_id='1')"
    assert last_response.ok?

    get "/Edition(race_id='1',num='8')"
    assert last_response.ok?

    get "/Edition(race_id='3',num='11')"
    assert last_response.ok?

    get "/Edition(race_id='666',num='666')"
    assert last_response.not_found?
  end

# test many to one Race - RaceType - nav.enty rename to rtype
  def test_many_to_one_nav
    get '/Race(1)/rtype'
    assert_last_resp_entity_parseable_ok('RaceType')

    assert_equal( 1, @jresd[:id])
    assert_equal('Non stop round-the-world', @jresd[:description])


    get '/Race(3)/rtype'
    assert_last_resp_entity_parseable_ok('RaceType')
    assert_equal( 2, @jresd[:id])
    assert_equal('Trans-Atlantic Non Stop', @jresd[:description])
  end

  def test_non_existing_attrib
    get '/Race(2)/dgfdgdgd'
    assert last_response.not_found?

    get '/Race(2)/rtypefdgfdg'
    assert last_response.not_found?

    get '/Race(1)/rtype/escription'
    assert last_response.not_found?

    get '/Race(1)/rtype/descriptions'
    assert last_response.not_found?

  end

# test many to one Race - RaceType attribute
  def test_many_to_one_nav_attrib
    get '/Race(1)/rtype/description'
    assert last_response.ok?
    assert_nothing_raised do
      @resh = JSON.parse last_response.body
    end
    assert_equal('Non stop round-the-world', @resh['d']['description'])

    get '/Race(1)/rtype/description/$value'
    assert last_response.ok?
    assert_equal('Non stop round-the-world', last_response.body)

    get '/Race(5)/rtype/description'
    assert last_response.ok?
    assert_nothing_raised do
      @resh = JSON.parse last_response.body
    end
    assert_equal('Regata', @resh['d']['description'])

    get '/Race(5)/rtype/description/$value'
    assert last_response.ok?
    assert_equal('Regata', last_response.body)

  end

# check the expand functionality
  def test_no_expand

# Per default expand should be off, but there should be
# the navigation attribute RaceType as deferred
    get '/Race(1)'
    assert_last_resp_entity_parseable_ok('Race')
    assert_not_expanded_entity(Race[1], @jresd, :rtype)
  end

  # expand nav attribut of target cardinality 1
  def test_expand_to_one_attr
    get '/Race(1)?$expand=rtype'
    assert_last_resp_entity_parseable_ok('Race')
    assert_expanded_entity(Race[1], @jresd, :rtype){|expanded| "RaceType(#{expanded.id})"}
  end

  def test_select_and_expand_to_one_attr_1
    get '/Race(1)?$expand=rtype&$select=id,rtype'
    assert_last_resp_entity_parseable_ok('Race')
    # lets do brute force
    expected = { :__metadata=>{:uri=>"http://example.org/Race(1)", :type=>"Race"},
                 :id=>1,
                 :rtype=> {:__metadata=>{:uri=>"http://example.org/RaceType(1)", :type=>"RaceType"},
                           :id=>1,
                           :description=>"Non stop round-the-world"}}
    assert_equal expected, @jresd 
    
  end
  
  # expand an attribute that is not selected --> not selected wins
  def test_select_and_expand_to_one_attr_2
    get '/Race(1)?$expand=rtype&$select=id,name'
    assert_last_resp_entity_parseable_ok('Race')
    # lets do brute force
    expected = { :__metadata=>{:uri=>"http://example.org/Race(1)", :type=>"Race"},
                 :id=>1,
                 :name=>"Vendée Globe"
}
    assert_equal expected, @jresd 
    
  end  
  
    # select only nav_attrs
  def test_select_and_expand_to_one_attr_3
    get '/Race(1)?$expand=rtype&$select=Edition,CrewType'
    assert_last_resp_entity_parseable_ok('Race')
    # lets do brute force
    expected = { :__metadata=>{:uri=>"http://example.org/Race(1)", :type=>"Race"},
                 :CrewType=>{:__deferred=>{:uri=>"http://example.org/Race(1)/CrewType"}},
                 :Edition=>{"__deferred"=>{:uri=>"http://example.org/Race(1)/Edition"}}
                }
    assert_equal expected, @jresd 
    
  end  

   # select only nav_attrs
  def test_select_and_expand_to_one_attr_4
    get '/Race(1)?$expand=rtype&$select=Edition,rtype,CrewType'
    assert_last_resp_entity_parseable_ok('Race')
    # lets do brute force
    expected = { :__metadata=>{:uri=>"http://example.org/Race(1)", :type=>"Race"},
                 :CrewType=>{:__deferred=>{:uri=>"http://example.org/Race(1)/CrewType"}},
                 :Edition=>{"__deferred"=>{:uri=>"http://example.org/Race(1)/Edition"}},
                :rtype=>{:__metadata=>{:type=>"RaceType", :uri=>"http://example.org/RaceType(1)"},
   :description=>"Non stop round-the-world",
   :id=>1}
   }
    assert_equal expected, @jresd 
    
  end  

  # select=* 
  def test_select_star_and_expand_to_one_attr_
    get '/Race(1)?$expand=rtype&$select=*'
    assert_last_resp_entity_parseable_ok('Race')
    assert_expanded_entity(Race[1], @jresd, :rtype){|expanded| "RaceType(#{expanded.id})"}
    
  end

  # one Race can have many Edition's... get tehm
  def test_one_to_many_nav_coll_pa1
    get '/Race(1)/Edition'
    assert_last_resp_coll_parseable_ok('Edition', Race[1].Edition.count)
    # Warning: this assumes records are returned in same orders as on db
    xresd = @jresd['results']

    assert_sequel_coll_set_equal(Race[1].Edition, xresd)

  end

  # expand nav attribut of target cardinality N
  def test_expand_to_multi_attr
    get '/Race(1)?$expand=Edition'
    assert_last_resp_entity_parseable_ok('Race')
    assert_expanded_entity_mult(Race[1], @jresd, :Edition){|exp|
      "Edition(race_id='#{exp.race_id}',num='#{exp.num}')"
    }
  end

# test many to one with a multiple PK's on both side
  def test_compo_pk_many_to_one_nav

    get "/Ranking(race_id='1',race_num='8',boat_class='2',rank='1',exaequo='0')/Edition"
    assert_last_resp_entity_parseable_ok('Edition')
# check result is correct
    assert_sequel_values_equal(Edition[1,8], @jresd)
  end

# Get the Race of an Edition of a Ranking record
  def test_ranking_edition_race

    get "/Ranking(race_id='1',race_num='8',boat_class='2',rank='1',exaequo='0')/Edition/Race"
    assert_last_resp_entity_parseable_ok('Race')
# check result is correct

    assert_sequel_values_equal(Race[1], @jresd)
  end
  def test_one_to_many_nav_coll_empty_result
# just check that Race(6) has 0 Edition's
    get '/Race(6)/Edition'
    assert_last_resp_coll_parseable_ok('Edition', 0)
  end
# test to many rels with top N filter
  def test_one_to_many_nav_coll_top
    get '/Race(1)/Edition?$top=2'
    # just check that Race(1)/Edition?$top=2 has 2 Editions's
    # (top 2 from 5)
    assert_last_resp_coll_parseable_ok('Edition', 2)

    ############################### same with skip 0
    get '/Race(1)/Edition?$skip=0&$top=2'
    # just check that Race(1)/Edition?$skip=0&$top=2' has 2 Editions's
    # (top 2 from 5)
    assert_last_resp_coll_parseable_ok('Edition', 2)
  end

# test to many rels with top N and skip filter
  def test_one_to_many_nav_coll_top_skip
    ############################### same with skip 1
    get '/Race(1)/Edition?$skip=1&$top=2'
    # just check that Race(1)/Edition?$skip=1&$top=2 has 2 Editions's
    # (top 2 from 5)
    assert_last_resp_coll_parseable_ok('Edition', 2)

    ############################### same with skip 4 top 2 from 5 records
    ## -->  1 records
#    # just check that Race(1)/Edition?$skip=4' has 1 Edition's
#    # (last 1 from 5)
    get '/Race(1)/Edition?$skip=4&$top=2'
    assert_last_resp_coll_parseable_ok('Edition', 1)
 ############################### same with skip 4 from 5 records--> remains 1
#    # just check that Race(1)/Edition?$skip=4' has 1 Editions's
#    # (last 1 from 5)
    get '/Race(1)/Edition?$skip=4'
    assert_last_resp_coll_parseable_ok('Edition', 1)

############################### same with skip 5 from 5 records--> remains 0

    get '/Race(1)/Edition?$skip=5'
    assert_last_resp_coll_parseable_ok('Edition', 0)

  end

  # test one to many rels + select by key ; ie Edition of Race(1) having key=...
  def test_one_to_many_nav_coll_by_key
    # this is same as Edition(race_id='1',num='8')
    get "/Race(1)/Edition(race_id='1',num='8')"

    assert_last_resp_entity_parseable_ok('Edition')

    assert_sequel_values_equal(Edition[1,8], @jresd)

  end

  # test one to many rels + select by wrong key ;
  # ie Edition of Race(1) having key=...
  def test_one_to_many_nav_coll_by_unrelated_key
    # note: Edition(race_id='4',num='13') is valid but
    # /Race(1)/Edition(race_id='4',num='13') is not valid

    get "/Race(1)/Edition(race_id='4',num='13')"
    assert last_response.not_found?

  end

end
