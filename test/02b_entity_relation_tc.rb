#!/usr/bin/env ruby

require_relative './test_botanics_db.rb' # Botanics-DB testdata

class EntityBoRelation_TC < BotanicsDBTest

  # minimal test our test-model/service
  def test_it_has_service
    get '/'
    assert last_response.ok?
  end

  def test_it_has_metadata
    get '/$metadata'
    assert last_response.ok?
  end

  # minimally test model a bit
  def test_model
    get '/Cultivar(1)'
    assert last_response.ok?
    assert_equal '2.0', last_response.headers['DataServiceVersion']

  end


  #
  def test_many_to_one_nav
    get '/Cultivar(3)/breeder'
    assert_last_resp_entity_parseable_ok('Breeder')
    assert_sequel_values_equal(Cultivar[3].breeder, @jresd)
  end


  def test_many_to_one_nav_link
    get '/Cultivar(3)/$links/breeder'
    assert_last_resp_onelink_parseable_ok
    # check content
    brdid = Cultivar[3].breeder.id
    assert_equal "http://example.org/Breeder(#{brdid})",
                 @jresd_links[:uri]
  end

  # the parent and child of Cultivar refer both to a collection
  # of Parentage records
  def test_one_to_many_nav
    get '/Cultivar(11)/parent'
    assert_last_resp_coll_parseable('Parentage', 2)
    assert_sequel_coll_values_equal(Cultivar[11].parent, @jresd_coll)
    assert_metadata_uri(Cultivar[11].parent.first, @jresd_coll.first){|par|  "ptree(cultivar_id='#{par.cultivar_id}',ptype_id='#{par.ptype_id}',parent_id='#{par.parent_id}')"}
         
    get '/Cultivar(11)/child'
    assert_last_resp_coll_parseable_ok('Parentage',1)
    assert_sequel_coll_values_equal(Cultivar[11].child, @jresd_coll)
  end

  def test_one_to_many_nav_01
    get '/Breeder(1)/cultivar'
    assert_last_resp_coll_parseable('Cultivar', Breeder[1].cultivar.size)
    assert_sequel_coll_values_equal(Breeder[1].cultivar, @jresd_coll)

    assert_metadata_uri(Breeder[1].cultivar.first, @jresd_coll.first){|cv|  "Cultivar(#{cv.id})"}
  end

  # test $links
  def test_one_to_many_nav_links
    get '/Cultivar(11)/$links/parent'
    assert_last_resp_links_parseable_ok('Parentage', 2)
    # check content
    assert_equal "http://example.org/ptree(cultivar_id='11',ptype_id='1',parent_id='8')",
                 @jresd_links[0][:uri]
    assert_equal "http://example.org/ptree(cultivar_id='11',ptype_id='2',parent_id='13')",
                 @jresd_links[1][:uri]

    get '/Cultivar(11)/$links/child'
    assert_last_resp_links_parseable_ok('Parentage', 1)

    # check content
    assert_equal "http://example.org/ptree(cultivar_id='12',ptype_id='2',parent_id='11')",
                 @jresd_links[0][:uri]


  end

end
