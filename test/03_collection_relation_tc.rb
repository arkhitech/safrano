#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require_relative './test_saildb.rb' # Sail-DB testdata

class CollectionRelationTest < SailDBTest

  # minimal test our test-model/service
  def test_it_has_service
    get '/'
    assert last_response.ok?
  end

  def test_it_has_metadata
    get '/$metadata'
    assert last_response.ok?
  end

  # minimally test model a bit
  def test_sail_model
    get '/Race(1)'
    assert last_response.ok?
    assert_equal '2.0', last_response.headers['DataServiceVersion']

    get '/Race(1)/PeriodicityType'
    assert last_response.ok?

    get '/Race(1)/CrewType'
    assert last_response.ok?

    get '/Race(5)/periodicity'
    assert last_response.ok?
    assert_nothing_raised do
      @resh = JSON.parse last_response.body
    end
    assert_equal(1, @resh['d']['periodicity'])

    get '/Race(5)/periodicity/$value'
    assert last_response.ok?
    assert_equal('1', last_response.body)

  end
    # Check ordering on related attributes
  def test_related_attr_order
    get '/Race?$orderby=PeriodicityType/description'
    expected_tab = Race.to_a.sort_by{ |pa| pa.PeriodicityType.description}
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

    assert_race_set_equal(expected_tab, @jresd_coll)
# test ordering by PeriodicityType/description'
    exp_order = expected_tab.map{|pa| pa.periodicity }
    res_order = @jresd_coll.map{|js| js[:periodicity]}
    assert_equal exp_order, res_order
  end

  # multi-orderby tests with navigation properties
  def test_get_multiorder_navi
    expected_tab = Race.all
    get '/Race?$orderby=PeriodicityType/description asc, id&$expand=PeriodicityType'
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_set_equal(expected_tab, @jresd_coll)


    assert_sequel_coll_ord_asc_by(@jresd_coll){|jresd|
      [ PeriodicityType[jresd[:periodicity]].description, jresd[:id]]
    }

    get '/Race?$orderby=PeriodicityType/description asc,id'
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_set_equal(expected_tab, @jresd_coll)
    assert_sequel_coll_ord_asc_by(@jresd_coll){|jresd|
      [ PeriodicityType[jresd[:periodicity]].description, jresd[:id]]
    }
    get '/Race?$orderby=PeriodicityType/description asc,id desc'
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_set_equal(expected_tab, @jresd_coll)
    assert_sequel_coll_ord_asc_by(@jresd_coll){|jresd|
      [ PeriodicityType[jresd[:periodicity]].description, (0-jresd[:id])]
    }

  end

  # multi-orderby tests with navigation properties and same field name
  # (ie. do we qualify correctly ordering arguments)
  def test_get_multiorder_navi_qualif
    expected_tab = Race.all
    get '/Race?$orderby=PeriodicityType/id asc, id'
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_set_equal(expected_tab, @jresd_coll)

    assert_sequel_coll_ord_asc_by(@jresd_coll){|jresd|
      [ jresd[:periodicity], jresd[:id]]
    }

    get '/Race?$orderby=PeriodicityType/id desc, id'
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_sequel_coll_set_equal(expected_tab, @jresd_coll)

    assert_sequel_coll_ord_asc_by(@jresd_coll){|jresd|
      [ (0-jresd[:periodicity]), jresd[:id]]
    }
  end

  def test_filter_protected_args
    get "Race?$filter=name NE 'id EQ 1'"
    expected_tab = Race.to_a
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)

    get "Race?$filter=name EQ 'id EQ 1'"
    expected_tab = []
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)

  end

  def test_substring_protected_args

    get "Race?$filter=startswith(name,'id EQ 1')"
    expected_tab = []
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)

  end

  # Check filtering on direct attributes
  def test_direct_attr_filter_1
    get '/Person?$filter=id eq 1'
    expected_tab = Person.where(id: 1).to_a
    assert_last_resp_coll_parseable_ok('Person', expected_tab.count)
    assert_person_set_equal(expected_tab, @jresd_coll)
  end

  # Check filtering + order with diff entity but same field name
# this produces an Sequel::DatabaseError: SQLite3::SQLException: ambiguous column name: id
# if the generate SQL does not have enough qualified names
  def test_direct_attr_filter_order
    get '/Race?$filter=id eq 10&$orderby=PeriodicityType/id'
    expected_tab = Race.where(id: 10).to_a
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)
  end

  # Check filtering on direct attributes
  def test_direct_attr_filter_2
    get '/Race?$filter=periodicity eq 3'
    expected_tab = Race.where(periodicity: 3).to_a
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

    assert_race_set_equal(expected_tab, @jresd_coll)
  end

  # Check filtering on related attributes
  def test_related_attr_filter
    get '/Race?$filter=PeriodicityType/period eq 4'
    # use brute force check(full scan...)
    expected_tab = Race.all.find_all do |ra|
      (ra.PeriodicityType.period == 4)
    end
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

    assert_race_set_equal(expected_tab, @jresd_coll)
  end

  # Check filtering on related attributes with potentially
  # ambiguous column name (id on both ends of the rel)
  # this is more tricky because both entities have an Id field...
  def test_related_attr_filter_ambig
    get '/Race?$filter=PeriodicityType/id eq 3'
    # use brute force check(full scan...)
    expected_tab = Race.all.find_all do |ra|
      (ra.PeriodicityType.id == 3)
    end
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

    assert_race_set_equal(expected_tab, @jresd_coll)
  end
## This triggered a
# Sequel::DatabaseError: SQLite3::SQLException: ambiguous column name: id
# because startswith field was not qualified
  def test_qualified_startswith_ambiguous_id

# this test only worked with sqlite.
# for DB with stronger type checks,   startswith(id,'1') makes a DB type error on ("Race"."id" LIKE '1%')
#    get "/Race?$filter=(PeriodicityType/id eq 3) AND (startswith(id,'1'))"
# --> just use a simpler condition on Race.id
  get "/Race?$filter=(PeriodicityType/id eq 3) AND (id NE 1)"
  # use brute force check(full scan...)
    expected_tab = Race.all.find_all do |ra|
      ( (ra.PeriodicityType.id == 3) and ( ra.id != 1 ) )
    end

    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)
  end

## This did trigger
# Sequel::DatabaseError: SQLite3::SQLException: no such function: startswith
# because startwith regexp was not accurate enough?
  def test_qualified_startswith
    get "/Race?$filter=startswith(PeriodicityType/description,'every')"
    # use brute force check(full scan...)
    expected_tab = Race.all.find_all do |ra|
       ( ra.PeriodicityType.description =~ /\Aevery/ )
    end
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

    # test content with result set
    assert_race_set_equal(expected_tab, @jresd_coll)
  end

  #same without quotes
  def test_qualified_startswith2
#    get "/Race?$filter=startswith(PeriodicityType/id,3)"
    get "/Race?$filter=(PeriodicityType/id mod 2) eq 1"
    # use brute force check(full scan...)
    expected_tab = Race.all.find_all do |ra|
       ( (ra.PeriodicityType.id % 2) == 1  )
    end
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

# test content with result set
    assert_race_set_equal(expected_tab, @jresd_coll)
  end

  def test_count_qualified_startswith
    get "/Race/$count?$filter=startswith(PeriodicityType/description,'every')"
    # use brute force check(full scan...)
    expected_tab = Race.all.find_all do |ra|
       ( ra.PeriodicityType.description =~ /\Aevery/ )
    end
    assert last_response.ok?
    assert_equal expected_tab.size.to_s, last_response.body
  end

  # Check filtering +ordering on related attributes
  def test_related_attr_filter_order
    get '/Race?$filter=(CrewType/size eq 1)&$orderby=PeriodicityType/description'
    # use brute force check(full scan...)
    expected_tab = Race.all.find_all do |pa|
      (pa.CrewType.size == 1)
    end.sort_by{ |pa| pa.PeriodicityType.description}
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

# test content with result set
    assert_race_set_equal(expected_tab, @jresd_coll)
# test ordering by PeriodicityType/description'

    assert_sequel_coll_ord_asc_by(@jresd_coll){|elmt| PeriodicityType[elmt[:periodicity]].description }

    # check reversed oder
    get '/Race?$filter=CrewType/size eq 1&$orderby=PeriodicityType/description desc'
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)
    assert_sequel_coll_ord_desc_by(@jresd_coll){|elmt| PeriodicityType[elmt[:periodicity]].description }

  end

# check the expand functionality
  def test_no_expand

# Per default expand should be off, but there should be
# the navigation attribute RaceType as deferred
    get '/Race'
    assert_last_resp_coll_parseable_ok('Race', Race.count)
    Race.each_with_index{|exp,idx|
      assert_not_expanded_entity(exp, @jresd_coll[idx], :rtype)
    }
  end

  def test_expand_to_one_attr
    get '/Race?$expand=rtype'
    assert_last_resp_coll_parseable_ok('Race', Race.count)
    Race.each_with_index{|exp,idx|
      assert_expanded_entity(exp, @jresd_coll[idx], :rtype){|expanded| "RaceType(#{expanded.id})"}
    }
  end

# check the expand+select functionality 
  def test_coll_select_and_expand_to_one_attr
    get '/Race?$expand=rtype&$select=id,rtype'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

    assert_sequel_coll_sel_set_equal(expected_tab, @jresd_coll, [:id, :rtype])

    Race.each_with_index{|exp,idx|
      expanded = exp.rtype
      proc = Proc.new{|expanded| "RaceType(#{expanded.id})"}
      assert_expanded_attrib(expanded, @jresd_coll[idx][:rtype], proc)
    }
  end
  
  def test_select_and_expand_to_one_attr_1
    get '/Race?$expand=rtype&$select=id,rtype'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

    # lets do brute force 
    expected = { :__metadata=>{:uri=>"http://example.org/Race(1)", :type=>"Race"},
                 :id=>1,
                 :rtype=> {:__metadata=>{:uri=>"http://example.org/RaceType(1)", :type=>"RaceType"},
                           :id=>1,
                           :description=>"Non stop round-the-world"}}
    assert_equal expected, @jresd_coll.first
    
  end
  
  # expand an attribute that is not selected --> not selected wins
  def test_select_and_expand_to_one_attr_2
    get '/Race?$expand=rtype&$select=id,name'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

    # lets do brute force
    expected = { :__metadata=>{:uri=>"http://example.org/Race(1)", :type=>"Race"},
                 :id=>1,
                 :name=>"Vendée Globe"
}
    assert_equal expected, @jresd_coll.first
    
  end  
  
    # select only nav_attrs
  def test_select_and_expand_to_one_attr_3
    get '/Race?$expand=rtype&$select=Edition,CrewType'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    # lets do brute force
    expected = { :__metadata=>{:uri=>"http://example.org/Race(1)", :type=>"Race"},
                 :CrewType=>{:__deferred=>{:uri=>"http://example.org/Race(1)/CrewType"}},
                 :Edition=>{"__deferred"=>{:uri=>"http://example.org/Race(1)/Edition"}}
                }
    assert_equal expected, @jresd_coll.first
    
  end  

   # select only nav_attrs
  def test_select_and_expand_to_one_attr_4
    get '/Race?$expand=rtype&$select=Edition,rtype,CrewType'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    # lets do brute force
    expected = { :__metadata=>{:uri=>"http://example.org/Race(1)", :type=>"Race"},
                 :CrewType=>{:__deferred=>{:uri=>"http://example.org/Race(1)/CrewType"}},
                 :Edition=>{"__deferred"=>{:uri=>"http://example.org/Race(1)/Edition"}},
                :rtype=>{:__metadata=>{:type=>"RaceType", :uri=>"http://example.org/RaceType(1)"},
   :description=>"Non stop round-the-world",
   :id=>1}
   }
    assert_equal expected, @jresd_coll.first
    
  end  

  # select=* 
  def test_select_star_and_expand_to_one_attr_
    get '/Race?$expand=rtype&$select=*'
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)

    assert_expanded_entity(Race[1], @jresd_coll.first, :rtype){|expanded| "RaceType(#{expanded.id})"}
    
  end
  
end


