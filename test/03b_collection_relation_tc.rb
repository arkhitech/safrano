
require_relative 'test_helper.rb'
require_relative './test_chinook_db.rb' # Chinook-DB testdata

module MathFloorCeilFuncImplementedTest 

  def test_floor_filter
    @filtstr =  'floor(Total) eq 2'
    gcollfilt {|inv| inv.Total.floor ==  2 }

    @filtstr =  'floor(Total) eq 1'
    gcollfilt {|inv| inv.Total.floor ==  1 }


  end
  
  def test_floor_ceil
    @filtstr =  'ceiling(Total) eq 2'
    gcollfilt {|inv| inv.Total.ceil ==  2 }

    @filtstr =  'ceiling(Total) eq 1'
    gcollfilt {|inv| inv.Total.ceil ==  1 }

  end
  
end

module MathFloorCeilFuncNotImplementedTest 

  def test_floor_filter
    @filtstr =  'floor(Total) eq 2'
    gcollfilt_not_implemented 'floor'
  end
  
  def test_floor_ceil
    @filtstr =  'ceiling(Total) eq 2'
    gcollfilt_not_implemented 'ceiling'
  end
  
end

class ChinookCollectionRelationTest < ChinookDBTest

  # minimal test our test-model/service
  def test_it_has_service
    get '/'
    assert last_response.ok?
  end

  def test_it_has_metadata
    get '/$metadata'
    assert last_response.ok?
  end

    # Check ordering on related attributes
  def test_related_attr_order
    get '/Customer?$orderby=salesrep/BirthDate'
    expected_tab = Customer.to_a.sort_by{ |cu| cu.salesrep.BirthDate}

    assert_last_resp_coll_parseable_ok('Customer', expected_tab.count)

# test ordering by salesrep/BirthDate'
    exp_order = expected_tab.map{|cu| cu.salesrep.BirthDate }

    resbd = @jresd_coll.map{|js| Employee[js[:SupportRepId]].BirthDate }

    y = resbd.dup.sort!

    assert_equal y, resbd
  end

  def test_nav_one_to_many
    get '/Customer(1)/invoices'
    expected_tab = Customer[1].invoices
    assert_last_resp_coll_parseable_ok('Invoice', expected_tab.count)

    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
  end

  def test_nav_one_to_many_filter
    get '/Customer(1)/invoices?$filter=Total eq 8.91'
    expected_tab = Customer[1].invoices_dataset.where(:Total => 8.91)

    assert_last_resp_coll_parseable_ok('Invoice', expected_tab.count)

    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
  end

  # the items of Invoice(411) whose Artist-Name (of album of track) is L.K.
  def test_nav_one_to_many_filter_deep_path
    get "/Invoice(411)/invoice_items?$filter=track/album/artist/Name eq 'Lenny Kravitz'"

    expected_tab = [InvoiceItem[2236], InvoiceItem[2237]]
    assert_last_resp_coll_parseable_ok('InvoiceItem', expected_tab.count)
    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
  end

  # the items of Invoice(411) whose Artist-Name (of album of track) is L.K.
  # oderderd by Track-Name
  def test_nav_one_to_many_filter_deep_path_order
    get "/Invoice(411)/invoice_items?$filter=track/album/artist/Name eq 'Lenny Kravitz'&$orderby=track/Name"

    expected_tab = [InvoiceItem[2236], InvoiceItem[2237]]
    assert_last_resp_coll_parseable_ok('InvoiceItem', expected_tab.count)
    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)

  end

  # same as prev. but order descending
  def test_nav_one_to_many_filter_deep_path_order_desc
    get "/Invoice(411)/invoice_items?$filter=track/album/artist/Name eq 'Lenny Kravitz'&$orderby=track/Name desc"

    expected_tab = [InvoiceItem[2237], InvoiceItem[2236]]
    assert_last_resp_coll_parseable_ok('InvoiceItem', expected_tab.count)

    assert_sequel_coll_values_equal(expected_tab, @jresd_coll)

  end

  ## expand nav attribut of target cardinality 1
  #def test_expand_to_one_attr
    #get '/Race(1)?$expand=rtype'
    #assert_last_resp_entity_parseable_ok('Race')
    #assert_expanded_entity(Race[1], @jresd, :rtype){|expanded| "RaceType(#{expanded.id})"}
  #end
  # minimaly test iso formating of date
  # theres probably some work remaining when it comes to the TZ
  
  # test iso8601 output of Time fields
  # both are valid 
  EXPT = [Time.new(2010,03,11,0,0,0,0).iso8601,    # --> 2010-03-11T00:00:00+00:00
          Time.utc(2010,03,11,0,0,0).iso8601  ]    # --> 2010-03-11T00:00:00Z
  def test_collection_date_type
    get "/Invoice?$filter=InvoiceId eq 98"
    
    assert_last_resp_coll_parseable_nocast_ok('Invoice', 1)

    assert EXPT.include?(@jrawd_coll.first[:InvoiceDate])

  end
  
  def test_rel_collection_get_date_type
    get "/Customer(1)/invoices?$filter=InvoiceId eq 98"
    
    assert_last_resp_coll_parseable_nocast_ok('Invoice', 1)

    assert EXPT.include?(@jrawd_coll.first[:InvoiceDate])

  end

  EXPT2 = [Time.new(2020,6,18,0,0,0,0).iso8601,    # --> 2020-06-18T00:00:00+00:00
          Time.utc(2020,06,18,0,0,0).iso8601  ]    # --> 2020-08-18T00:00:00Z        

  def test_create_on_navcol_date_type
    
    count = Customer[1].invoices.size
    
    @data = {CustomerId: 1,
             InvoiceDate: Time.new(2020,6,18,0,0,0,0),
             BillingAddress: 'Champs Elises',
             BillingCity: 'Paris',
             BillingState: '',
             BillingCountry: 'FR',
             BillingPostalCode: '00001',
             Total: 0.99 }
    pbody = @data.to_json

    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'

    post '/Customer(1)/invoices', pbody
    
    assert_last_resp_coll_parseable_nocast_created('Invoice', 1)
        
    assert EXPT2.include?(@jrawd_coll.first[:InvoiceDate])
    
    assert_equal count + 1, Customer[1].invoices.size
    
  end
  
end

# $filter math functions round, floor, ceil
module MathFilterTests

  # $filter datetime year (better covering is in botanics suite)
  def test_year_filter
    @filtstr = 'year(InvoiceDate) ge 2000'    
    gcollfilt {|inv| inv.InvoiceDate.year  >= 2000 }
  end  
  

  def test_round_filter
    @filtstr =  'round(Total) eq 8'
    gcollfilt {|inv| inv.Total.round ==  8 }
    
    @filtstr =  'round(Total) eq 9'
    gcollfilt {|inv| inv.Total.round ==  9 }
  end

# mixin adapter specific test modules 
  case Sequel::Model.db.adapter_scheme
  when :sqlite
    include MathFloorCeilFuncNotImplementedTest 
  else
    include MathFloorCeilFuncImplementedTest 
  end

# test error handling and reporting ... eg when ceil is used instead of ceiling
  def test_syntax_error_in_filter
    # OData defines func. ceiling, not ceil... should be reported as error
    @filtstr =  'ceil(Total) eq 9'
    gcollfilt_bad_request ['invalid', 'ceil', 'function']
  end
end
# run the tests on all invoices set
class ChinookCollectionMathFilterTest < ChinookDBTest
  def setup
    @collpath = 'Invoice'
    @etype = 'Invoice'
    @ecollbase = Invoice.all
  end
  include MathFilterTests

end

# same tests should pass for a limited top300  set
class ChinookLimited300CollectionMathFilterTest < ChinookDBTest
  def setup
    @collpath = 'Invoice'
    @etype = 'Invoice'
    @ecollbase = Invoice.all
    @toppar = 300
  end
  include MathFilterTests

end

# same tests should pass for a limited top1  set
class ChinookLimited1CollectionMathFilterTest < ChinookDBTest
  def setup
    @collpath = 'Invoice'
    @etype = 'Invoice'
    @ecollbase = Invoice.all
    @toppar = 1
  end
  include MathFilterTests

end


# same tests should pass on a navigated collection all invoices of Customer(2)
class ChinookNavCollectionMathFilterTest < ChinookDBTest
  def setup
    @collpath = 'Customer(2)/invoices'
    @etype = 'Invoice'
    @ecollbase = Customer[2].invoices
  end
  include MathFilterTests

end

