#!/usr/bin/env ruby

require_relative './test_saildb.rb' # Sail-DB testdata

class Entity_Create_TC < SailDBTest

  def  payload_cleanup_nava

    #remove nav attribs
    @data.delete('rtype')
    @data.delete('CrewType')
    @data.delete('PeriodicityType')
    @data.delete('Edition')
    @data
  end

  def setup
    @data = ( @data ? @data : get_a_valid_payload )
  end

  def  get_a_valid_payload
    get '/Race(12)'
    assert_last_resp_entity_parseable_ok('Race')

    @data = JSON.parse(last_response.body)['d']
    payload_cleanup_nava
    @data.delete('__metadata')
    @data.delete('id')
    @data
  end

# test access with a single PK id
  def test_simple_create
    idlist_old = Race.to_a.map{|r| r.id}

    @data['name'] = 'A copy of Race(12) '

    pbody = @data.to_json

    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'

    post '/Race', pbody

    assert_last_resp_coll_parseable_created('Race',1)
    idnew = @jresd_coll.first[:id]
# check the returned id is a new one
    assert_false idlist_old.include?(idnew)

# check the the uri of defered attributes
# Warning.. this is probably Version dependant (V2)
    assert_equal "http://example.org/Race(#{idnew})/rtype", @jresd_coll.first[:rtype][:__deferred][:uri]

#name was changed

    Race[idnew].reload
    assert_equal 'A copy of Race(12) ', Race[idnew].name
    assert_equal 'A copy of Race(12) ', @jresd_coll.first[:name]
# otherwise not changed
    assert_equal Race[12].type, Race[idnew].type
    assert_equal Race[12].periodicity, Race[idnew].periodicity
    assert_equal Race[12].crew_type, Race[idnew].crew_type

    assert_equal Race[12].type, @jresd_coll.first[:type]
     assert_equal Race[12].periodicity, @jresd_coll.first[:periodicity]
     assert_equal Race[12].crew_type, @jresd_coll.first[:crew_type]
  end

# test read + merge-update with a single PK id, BAD request
# (syntax error in JSON payload)
  def test_create_bad_request_syntax
    pbody = '{this aint be valid JSON '

    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'
    post '/Race', pbody

    # check for 400
    assert last_response.bad_request?

  end

# test create with a wrong column name
  def test_simple_create_unprocessable
    changed_data = @data.clone

    # provide an invalid column
    changed_data['name_XXX'] = 'Some other thing'
    pbody = changed_data.to_json
    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'
    post '/Race', pbody

    # check for 422
    assert last_response.unprocessable?

  end

end
