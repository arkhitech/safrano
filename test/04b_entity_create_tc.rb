#!/usr/bin/env ruby

require_relative './test_botanics_db.rb' # Botanics-DB testdata

class Entity_Create_1BO_TC < BotanicsDBTest

  # TODO; better design... Test will fail when we add nav. attr. in model
  # but dont add it here...
  def  payload_cleanup_nava
    #remove nav attribs
    @data.delete('breeder')
    @data.delete('parent')
    @data.delete('child')
    @data.delete('photo')
    @data.delete('institute')
    @data
  end

  def setup
    @jresd = nil
    @jresd_coll = nil
    @data = ( @data ? @data : get_a_valid_cultivar_create_payload )
  end

  def  get_a_valid_cultivar_create_payload
    get '/Cultivar(3)'
    assert_last_resp_entity_parseable_ok('Cultivar')

    @data = JSON.parse(last_response.body)['d']
    payload_cleanup_nava
    @data.delete('__metadata')
    @data.delete('id')
    @data
  end

# OData V4 ref § 11.4.2 collection nav link
#
# If the target URL for the collection is a navigation link, the new entity
# is automatically linked to the entity containing the navigation link.

# Note this test higly depends on the way the navigation relationship is defined
# For example for sail-db  Race(1)/Edition, the Edition navigation collection
# is linked automatically to Race(1) as per the Editions RaceId key
# In this case the key field of the to be created entity are not auto-incrementing (or otherway self-generating) but they have to be provided anyway by the calling client app
# TODO... what's the expected result in this case (POST Race(1)/Edition ???)?

# Thus this makes only sense for one to many rels where the related entity has
# his own independant (auto genrating/incrementing) key and the relation is made
# by a foreign key field on the new entity table.
# Example; the chinook Album/tracks relation...

# Furthermore even, in this case, we assume the client passes the correct
# FK value in the payload, so that on the end, we do just have to
# check consistency of passed FK
# if consistent --> creation works
#    otherwise error bad request ?

  def test_create_from_navigation_link_ok

    idlist_old = Cultivar.to_a.map{|c| c.id }

    @data['name'] = 'A copy of Red Delicious'

    pbody = @data.to_json

    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'

    post '/Breeder(1)/cultivar', pbody

    assert_last_resp_coll_parseable_created('Cultivar',1)
    newe = @jresd_coll.first
    idnew = newe[:id]

# check the returned id is a new one
    assert_false idlist_old.include?(idnew)

    assert_equal 'A copy of Red Delicious', Cultivar[idnew].name    
    assert_equal 1, Cultivar[idnew].breeder_id
    assert Breeder[1].cultivar.find{|c| c.id == idnew }

    assert_equal "#{uribase(last_request)}/Cultivar(#{idnew})", newe[:__metadata][:uri]

  end
end
