#!/usr/bin/env ruby

require_relative './test_botanics_db.rb' # Botanics-DB testdata

class Entity_Delete_1BO_TC < BotanicsDBTest


  def setup
    @jresd = nil
    @jresd_coll = nil
  end

  
  # test delete  entity
  def test_delete_entity
  
    # 1. get the entity and check content is not empty
    get '/Institute(5)'
    
    #2. check it has content
    assert_last_resp_entity_parseable_ok('Institute')
    assert_sequel_values_equal(Institute[5], @jresd)
    
    # 3. delete it
    delete  '/Institute(5)'
    
    # 4. check it's been deleted
    assert Institute.where(id: 5).all.empty?
    
  end

  # delete existing entity by nav
  # expected result:
  # 1. the navigation link is removed (ie. FK nulled on parent entity)
  # 2. the nav-entity
  
  # Note: we delete /Cultivar(22)/institute which is Institute(6)
  def test_delete_nav_entity  
   # 1. get the entity and check content is not empty
    get '/Cultivar(22)/institute'
    assert_last_resp_entity_parseable_ok('Institute')
    assert_sequel_values_equal(Institute[6], @jresd)
    
   
    # 3. delete it
    delete  '/Cultivar(22)/institute'
    
    # 4. check it's been deleted
    Cultivar[22].reload

    assert_nil Cultivar[22].inst_id
    assert Institute.where(id: 6).all.empty?
        
    # 5. profit
  end

  # same as above but call the delete directly on the entity
  # this should not work due to the existing FK and return an error
  # we try to delete Institute(2) which is linked to at least one Cultivar
  def test_delete_entity_FK_error
    # 1. get the entity and check content is not empty
    get '/Institute(2)'
    assert_last_resp_entity_parseable_ok('Institute')
    assert_sequel_values_equal(Institute[2], @jresd)
    
    # 2. delete it
    delete '/Institute(2)'
    assert last_response.bad_request?
  end

end
