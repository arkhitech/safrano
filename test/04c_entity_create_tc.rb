#!/usr/bin/env ruby

require_relative './test_chinook_db.rb' # Chinook-DB testdata

class Entity_Create_1_TC < ChinookDBTest

  def  payload_cleanup_nava

    #remove nav attribs
    @data.delete('album')
    @data
  end

  def setup
    @jresd = nil
    @jresd_coll = nil
    @data = ( @data ? @data : get_a_valid_track_create_payload )
  end

  def  get_a_valid_track_create_payload
    get '/Album(1)/tracks'
    assert_last_resp_coll_parseable_ok('Track', Album[1].tracks.size)

    @data = JSON.parse(last_response.body)['d']['results'].first
    payload_cleanup_nava
    @data.delete('__metadata')
    @data.delete('TrackId')
    @data
  end

# OData V4 ref § 11.4.2 collection nav link
#
# If the target URL for the collection is a navigation link, the new entity
# is automatically linked to the entity containing the navigation link.

# Note this test higly depends on the way the navigation relationship is defined
# For example for sail-db  Race(1)/Edition, the Edition navigation collection
# is linked automatically to Race(1) as per the Editions RaceId key
# In this case the key field of the to be created entity are not auto-incrementing (or otherway self-generating) but they have to be provided anyway by the calling client app
# TODO... what's the expected result in this case (POST Race(1)/Edition ???)?

# Thus this makes only sense for one to many rels where the related entity has
# his own independant (auto genrating/incrementing) key and the relation is made
# by a foreign key field on the new entity table.
# Example; the chinook Album/tracks relation...

# Furthermore even, in this case, we assume the client passes the correct
# FK value in the payload, so that on the end, we do just have to
# check consistency of passed FK
# if consistent --> creation works
#    otherwise error bad request ?

  def test_create_from_navigation_link_ok

    idlist_old = Track.to_a.map{|t| t.TrackId }

    @data['Name'] = 'A copy of first Track of Album(1) '

    pbody = @data.to_json

    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'

    post '/Album(1)/tracks', pbody

    assert_last_resp_coll_parseable_created('Track',1)
    idnew = @jresd_coll.first[:TrackId]

# check the returned id is a new one
    assert_false idlist_old.include?(idnew)

    assert_equal 'A copy of first Track of Album(1) ', Track[idnew].Name    
    assert_equal 1, Track[idnew].AlbumId
    assert Album[1].tracks.find{|t| t.TrackId == idnew }

  end

# TODO 
# conflicting information provided: POST Album(1)/tracks but with AlbumId=2 in payload
  #def test_create_from_navigation_link_bad_req

    #@data['AlbumId'] = 2

    #pbody = @data.to_json

    #header 'Accept', 'application/json'
    #header 'Content-Type', 'application/json'


    #post '/Album(1)/tracks', pbody
    #assert last_response.bad_request?
    
  #end
end
