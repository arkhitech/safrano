#!/usr/bin/env ruby

require 'test/unit'
require_relative '../lib/safrano/multipart.rb'


class Test_OData_Changeset_1 < TCWithoutDB
  CHGSET1_TXT = '{"__metadata":{"uri":"http://localhost:9494/Contacts(8)","type":"Contacts"},"FirstName":"zzzzzzzz"}'

  def setup
    # This testcase is for an OData batch changeset having only one change
    # (ie one part)
    @inpstr = File.open('test/multipart/odata_changeset_1_body.txt','r')
    @mimep = MIME::Media::Parser.new
    @boundary = 'batch_48f8-3aea-2f04'
    @changeset_boundary = 'changeset_fa99-0523-ad8a'
  end

  def teardown
    @inpstr.close
  end

  def test_parse_body
    assert_nothing_raised do
      @mime = @mimep.hook_multipart('multipart/mixed', @boundary)
      @mime = @mimep.parse_str(@inpstr)
    end

    assert_kind_of MIME::Content::Multipart::Base, @mime
    assert_equal @boundary, @mime.boundary

    assert_kind_of Array, @mime.content

    assert_equal 2, @mime.content.size


    firstpart = @mime.content.first
    lastpart = @mime.content.last.content


    assert_kind_of MIME::Content::Multipart::Base, firstpart
    assert_kind_of MIME::Content::Application::HttpReq, lastpart

    assert_equal 'Contacts(8)', lastpart.uri
    assert_equal 'GET', lastpart.http_method
    assert_equal 'application/json', lastpart.hd['accept']
    assert_equal '', lastpart.content

    assert_kind_of Array, firstpart.content
    assert_equal @changeset_boundary, firstpart.boundary
    assert_equal 1, firstpart.content.size

    chgsetpart1 = firstpart.content.first.content

    assert_equal 'Contacts(8)', chgsetpart1.uri
    assert_equal 'MERGE', chgsetpart1.http_method
    assert_equal 'application/json', chgsetpart1.hd['accept']
    assert_equal 'application/json', chgsetpart1.hd['content-type']
    assert_equal '99', chgsetpart1.hd['content-length']
    assert_equal CHGSET1_TXT , chgsetpart1.content

  end

end

class Test_OData_Changeset_5_Delete < TCWithoutDB

  def setup
    # This testcase is for an OData batch changeset with a DELETE
    @inpstr = File.open('test/multipart/odata_changeset_5_delete_body.txt','r')
    @mimep = MIME::Media::Parser.new
    @boundary = 'batch_8f22-6982-565e'
    @changeset_boundary = 'changeset_3a9f-0f42-a713'
  end

  def teardown
    @inpstr.close
  end

  def test_parse_body
    assert_nothing_raised do
      @mime = @mimep.hook_multipart('multipart/mixed', @boundary)
      @mime = @mimep.parse_str(@inpstr)
    end

    assert_kind_of MIME::Content::Multipart::Base, @mime
    assert_equal @boundary, @mime.boundary

    assert_kind_of Array, @mime.content

    assert_equal 7, @mime.content.size


    firstpart = @mime.content.first
    lastpart = @mime.content.last.content


    assert_kind_of MIME::Content::Multipart::Base, firstpart
    assert_kind_of MIME::Content::Application::HttpReq, lastpart

  end

end


class Test_OData_Changeset < TCWithoutDB

  CHGSET1_TXT = <<-BODY.gsub!("\n", "\r\n")
<AtomPub representation of a new Customer>
BODY

  CHGSET2_TXT = <<-BODY.gsub!("\n", "\r\n")
<JSON representation of Customer ALFKI>
BODY


  def setup
    @inpstr = File.open('test/multipart/odata_changeset_body.txt','r')
    @mimep = MIME::Media::Parser.new
    @boundary = 'batch_36522ad7-fc75-4b56-8c71-56071383e77b'
    @changeset_boundary = 'changeset_77162fcd-b8da-41ac-a9f8-9357efbbd621'
  end

  def teardown
    @inpstr.close
  end

  def test_parse_body
    assert_nothing_raised do
      @mime = @mimep.hook_multipart('multipart/mixed', @boundary)
      @mime = @mimep.parse_str(@inpstr)
    end

    assert_kind_of MIME::Content::Multipart::Base, @mime
    assert_equal @boundary, @mime.boundary

    assert_kind_of Array, @mime.content

    assert_equal 3, @mime.content.size

    firstpart = @mime.content.first.content
    secondpart = @mime.content[1]
    lastpart = @mime.content.last.content


    assert_kind_of MIME::Content::Application::HttpReq, firstpart
    assert_kind_of MIME::Content::Multipart::Base, secondpart
    assert_kind_of MIME::Content::Application::HttpReq, lastpart


    assert_equal "/service/Customers('ALFKI')", firstpart.uri
    assert_equal 'GET', firstpart.http_method
    assert_equal 'host', firstpart.hd['host']
    assert_equal '', firstpart.content

    assert_equal 'service/Products', lastpart.uri
    assert_equal 'GET', lastpart.http_method
    assert_equal 'host', lastpart.hd['host']
    assert_equal '', lastpart.content

    assert_kind_of Array, secondpart.content
    assert_equal @changeset_boundary, secondpart.boundary
    assert_equal 2, secondpart.content.size

    chgsetpart1 = secondpart.content.first.content
    chgsetpart2 = secondpart.content.last.content

    assert_equal '/service/Customers', chgsetpart1.uri
    assert_equal 'POST', chgsetpart1.http_method
    assert_equal 'application/atom+xml;type=entry', chgsetpart1.hd['content-type']
    assert_equal CHGSET1_TXT, chgsetpart1.content

    # check content-id
    assert_equal '99', secondpart.content.first.hd['content-id']

    assert_equal "/service/Customers('ALFKI')", chgsetpart2.uri
    assert_equal 'PUT', chgsetpart2.http_method
    assert_equal 'application/json', chgsetpart2.hd['content-type']
    assert_equal CHGSET2_TXT, chgsetpart2.content

  end
end

class Test_OData_Batch1 < TCWithoutDB

  LASTPARTBODYTXT = <<-BODY.gsub!("\n", "\r\n")
{"id": 666,
 "name": "TEST The Ocean Race",
 "type": 5,
 "periodicity": 3,
 "crew_type": 5
 }
BODY

  def setup
    @inpstr = File.open('test/multipart/odata_batch1_body.txt','r')
    @mimep = MIME::Media::Parser.new
    @boundary = 'batch_56071383e77b'
  end

  def teardown
    @inpstr.close
  end

  def test_parse_body
    assert_nothing_raised do
      @mimep.hook_multipart('multipart/mixed', @boundary)
      @mime = @mimep.parse_str(@inpstr)
    end

    assert_kind_of MIME::Content::Multipart::Base, @mime
    assert_equal @boundary, @mime.boundary

    assert_kind_of Array, @mime.content

    assert_equal 3, @mime.content.size

    firstpart = @mime.content.first.content
    secondpart = @mime.content[1].content
    lastpart = @mime.content.last.content

    @mime.content.each do |part|
      assert_kind_of MIME::Content::Application::Http, part
#      TODO we have the content-type of the part wich should be
#       'application/http' and the content-type of the http request body
#       at the moment we do not handle this cleanly yet
      assert_equal 'application/http', part.hd['content-type']
       assert_equal 'application/http', part.ct
    end

    assert_equal 'http://localhost:9595/Socius(1)', firstpart.uri
    assert_equal 'GET', firstpart.http_method
    assert_equal 'application/json;odata.metadata=minimal', firstpart.hd['accept']
    assert_equal '', firstpart.content


    assert_equal 'http://localhost:9595/Race(2)', secondpart.uri
    assert_equal 'GET', secondpart.http_method
    assert_equal 'application/json;odata.metadata=minimal', secondpart.hd['accept']
    assert_equal '', secondpart.content

    assert_equal 'http://localhost:9595/Race', lastpart.uri
    assert_equal 'POST', lastpart.http_method
    assert_equal 'application/json;odata.metadata=minimal', lastpart.hd['accept']
    assert_equal 'application/json', lastpart.hd['content-type']
    assert_equal LASTPARTBODYTXT, lastpart.content

  end
end

class Test_Nested < TCWithoutDB
  FIRSTTEXT = " ...body goes here ...\r\n"
  SECONDTEXT = " ... another body goes here ...\r\n"

  def setup
    @inpstr = File.open('test/multipart/nested.txt','r')
    @mimep = MIME::Media::Parser.new
  end

  def teardown
    @inpstr.close
  end

  def test_parse
    assert_nothing_raised do
      @mime = @mimep.parse_str(@inpstr)
    end

    assert_equal 'multipart/mixed; boundary="---- main boundary ----"', @mime.hd['content-type']
    assert_equal 'Recipient-List', @mime.hd['to']

    assert_equal 'multipart/mixed; boundary="---- main boundary ----"', @mime.ct

    assert_kind_of MIME::Content::Multipart::Base, @mime
    assert_equal '---- main boundary ----', @mime.boundary

    assert_kind_of Array, @mime.content

    assert_equal 2, @mime.content.size

    firstpart = @mime.content.first
    secondpart = @mime.content.last

    assert_kind_of MIME::Content::Text::Plain, firstpart
    assert_equal 'text/plain', firstpart.hd['content-type']

    assert_kind_of MIME::Content::Multipart::Base, secondpart
    assert_equal '---- next message ----', secondpart.boundary

    assert_kind_of Array, secondpart.content

    assert_equal 2, secondpart.content.size

    firsttext = secondpart.content.first
    secondtext = secondpart.content.last

    assert_equal FIRSTTEXT, firsttext.content
    assert_equal 'my opinion', firsttext.hd['subject']

    assert_equal SECONDTEXT, secondtext.content
    assert_equal 'my different opinion', secondtext.hd['subject']
  end
end

class Test_Prolog < TCWithoutDB

  FIRSTTEXT = "This is implicitly typed plain US-ASCII text.\r\nIt does NOT end with a linebreak."
  LASTTEXT = "This is explicitly typed plain US-ASCII text.\r\nIt DOES end with a linebreak.\r\n"

  def setup
    @inpstr = File.open('test/multipart/prolog.txt','r')
    @mimep = MIME::Media::Parser.new
  end

  def teardown
    @inpstr.close
  end

  def test_parse
    assert_nothing_raised do
      @mime = @mimep.parse_str(@inpstr)
    end

    assert_equal 'multipart/mixed; boundary="simple boundary"', @mime.hd['content-type']
    assert_equal 'Ned Freed <ned@innosoft.com>', @mime.hd['to']

    assert_equal 'multipart/mixed; boundary="simple boundary"', @mime.ct

    assert_kind_of MIME::Content::Multipart::Base, @mime
    assert_equal 'simple boundary', @mime.boundary

    assert_kind_of Array, @mime.content

    assert_equal 2, @mime.content.size

    firstpart = @mime.content.first
    lastpart = @mime.content.last
    assert_kind_of MIME::Content::Text::Plain, firstpart
    assert_kind_of MIME::Content::Text::Plain, lastpart

    assert_equal 'text/plain', firstpart.hd['content-type']
    assert_equal 'text/plain; charset=us-ascii', lastpart.hd['content-type']

    assert_equal FIRSTTEXT, firstpart.content
    assert_equal LASTTEXT, lastpart.content
  end

end

