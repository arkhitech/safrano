#!/usr/bin/env ruby

require 'test/unit'
require_relative '../lib/safrano/multipart.rb'


class Test_OData_UP_Changeset_1 < TCWithoutDB

  def setup
    # This testcase is for an OData batch changeset having only one change
    # (ie one part)
    @inpstr = File.open('test/multipart/odata_changeset_1_body.txt','r')
    @mimep = MIME::Media::Parser.new
    @boundary = 'batch_48f8-3aea-2f04'
    @changeset_boundary = 'changeset_fa99-0523-ad8a'
  end

  def teardown
    @inpstr.close
  end

  def test_unparse
    assert_nothing_raised do
      @mimep.hook_multipart('multipart/mixed', @boundary)
      @mime = @mimep.parse_str(@inpstr)
    end

    # we cant easily test idempotency of parse+unparse due to
    # small allowed changes like upper-lower case in header lines, or
    # optional spaces after : or ;

    #    exp = File.read('test/multipart/odata_changeset_1_body.txt')
    #    assert_equal exp, @mime.unparse(true)

    # However, unparse+re-parse should result in the same parsed result
    # and this is indirectly a good test of unparse
    assert_nothing_raised do
      @unparsed = @mime.unparse
    end

    @mimep2 = MIME::Media::Parser.new
    @mimep2.hook_multipart('multipart/mixed', @boundary)
    @mime2 = @mimep2.parse_string(@unparsed)

    # for this to work we have defined equality op in the multipart classes
    assert_equal @mime, @mime2

  end

end

class Test_OData_UP_Changeset < TCWithoutDB


  def setup
    @inpstr = File.open('test/multipart/odata_changeset_body.txt','r')
    @mimep = MIME::Media::Parser.new
    @boundary = 'batch_36522ad7-fc75-4b56-8c71-56071383e77b'
    @changeset_boundary = 'changeset_77162fcd-b8da-41ac-a9f8-9357efbbd621'
  end

  def teardown
    @inpstr.close
  end

  def test_unparse
    assert_nothing_raised do
      @mime = @mimep.hook_multipart('multipart/mixed', @boundary)
      @mime = @mimep.parse_str(@inpstr)
    end

    # However, unparse+re-parse should result in the same parsed result
    # and this is indirectly a good test of unparse
    assert_nothing_raised do
      @unparsed = @mime.unparse
    end

    @mimep2 = MIME::Media::Parser.new
    @mimep2.hook_multipart('multipart/mixed', @boundary)
    @mime2 = @mimep2.parse_string(@unparsed)

    # for this to work we have defined equality op in the multipart classes
    assert_equal @mime, @mime2

  end
end

