#!/usr/bin/env ruby

require_relative './test_saildb.rb' # Sail-DB testdata


class PrefixedWalker_TC <  SailDBTestII
  OSERV = ODataSailPrefixedApp.get_service_base

  def walker_status_context_check(st_exp, ctx_exp, path)
    ppath="#{TCSailPrefix}#{path}"
    walker = OData::Walker.new(OSERV, ppath)
    assert_equal(ctx_exp, walker.end_context,
                 "End context of path #{ppath} is not correct")
    assert_equal(st_exp, walker.status,
                 "End status of path #{ppath} is not correct")

  end

# check that a given path results in an expected error state
# TODO: check then end context as well (should be a error object
#   matching the type of error)
  def walker_error_status_check(path)
    ppath="#{TCSailPrefix}#{path}"
    walker = OData::Walker.new(OSERV, ppath)

    assert_equal(:error, walker.status,
                 "End status of path #{ppath} with known error is not :error")
  end

  def test_metadata
    walker_status_context_check( :end, OSERV.meta, '/$metadata')
    walker_status_context_check( :end, OSERV.meta, '/$metadata/')
  end

  def test_metadata_err
    walker_status_context_check( :error, nil, '/$metatata/')
  end

  def test_root
    walker_status_context_check( :end, OSERV, '/')
  end

  def test_top_level_collection_1
    walker_status_context_check( :end, Race, '/Race')
    walker_status_context_check( :end, Race, '/Race/')

    walker_status_context_check( :end, Person, '/Person')
    walker_status_context_check( :end, Person, '/Person/')
  end

# Check a collection with a renamed entity name (not equal the class name)
  def test_top_level_collection_renamed
    walker_status_context_check( :end, PeriodicityType, '/periodicity_type')
    walker_status_context_check( :end, PeriodicityType, '/periodicity_type/')
  end
end


class Prefixed_Entity_TC < PrefixedSailDBTest

# test access with a single PK id
  def test_race_model_single_pk
    get %{#{TCSailPrefix}/Race(1)}
    assert_last_resp_entity_parseable_ok('Race')

    # Race( 2) urlencoded space %20 should be ok
    get %{#{TCSailPrefix}/Race(%202)}
    assert_last_resp_entity_parseable_ok('Race')

    get %{#{TCSailPrefix}/Race(9999)}
    assert last_response.not_found?

    get %{#{TCSailPrefix}/Race(xyz99xxx)}
    assert last_response.bad_request?

  end

# test access with a multiple PK
  def test_ranking_model_multi_pk

    get "#{TCSailPrefix}/Ranking(race_id='1',race_num='8',boat_class='2',rank='1',exaequo='0')"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(8, @jresd[:crew])
# real spaces are forbidden in URI's. Ok this is handled by ruby URI parse
# somewhere
    assert_raise(URI::InvalidURIError){
      get "#{TCSailPrefix}/Ranking(race_id='1',  race_num='8',  boat_class='2', rank='1',exaequo='0')"
    }
# but URL-encoded spaces ie. %20 should work !
    get "#{TCSailPrefix}/Ranking(race_id='1',%20race_num='8',boat_class='2',%20rank='1',exaequo='0')"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(8, @jresd[:crew])
# should work without quotes
    get "#{TCSailPrefix}/Ranking(race_id=1,race_num=8,boat_class=2,rank=1,exaequo=0)"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(8, @jresd[:crew])
# check not found by providing a wrong exaequo
    get "#{TCSailPrefix}/Ranking(race_id='1',race_num='8',boat_class='2',rank='1',exaequo='1')"
    assert last_response.not_found?
    get "#{TCSailPrefix}/Ranking(race_id=1,race_num=8,boat_class=2,rank=1,exaequo=1)"
    assert last_response.not_found?


    get "#{TCSailPrefix}/Ranking(race_id='3',race_num='11',boat_class='1',rank='1',exaequo='0')"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(1, @jresd[:crew])

# check an ex-aequo record
    get "#{TCSailPrefix}/Ranking(race_id=1,race_num=6,boat_class=2,rank=3,exaequo=1)"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(28, @jresd[:crew])

    get "#{TCSailPrefix}/Ranking(race_id=1,race_num=6,boat_class=2,rank=3,exaequo=2)"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(15, @jresd[:crew])

# check not found
    get "#{TCSailPrefix}/Ranking(race_id='3',race_num='11',boat_class='1',rank='1',exaequo='1')"
    assert last_response.not_found?
  end

  # Check simple collection get
  def test_simple_coll_get
    get %{#{TCSailPrefix}/Race}
    expected_tab = Race.all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)
  end

  # Check a collection with a renamed entity name (not equal the class name)
  def test_simple_coll_renamed_get
    get %{#{TCSailPrefix}/periodicity_type}
    expected_tab = PeriodicityType.all
    assert_last_resp_coll_parseable_ok('PeriodicityType', expected_tab.count)
    assert_pertype_set_equal(expected_tab, @jresd_coll)
  end

  # Check startswith filter
  def test_startwith_filter_2
    get %{#{TCSailPrefix}/Race?$filter=startswith(name,'B')}
    expected_tab = Race.where(Sequel.like(:name, 'B%')).all
    assert_last_resp_coll_parseable_ok('Race', expected_tab.count)
    assert_race_set_equal(expected_tab, @jresd_coll)
  end
end
