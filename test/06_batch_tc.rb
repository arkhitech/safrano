#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require_relative './test_saildb.rb' # Sail-DB testdata

# with disabled batch (default)
class NOBatchTC < SailDBTest
  # per defaut, $batch is disabled
  def test_batch_not_implemented
    # $batch works only with POST , but if it's not enabled,
    # return 404 not found?
    get '/$batch'
    assert_equal(404, last_response.status)

    # not (yet) implemented
    post '/$batch', ''
    assert_equal(404, last_response.status)
  end
end



module OData::Batch::TestHelper

  AH = {
    'xyz-contextid-accept' => 'header',
    'Accept' => 'text/plain, */*;q=0.5',
    'Accept-Language' => 'en-US',
    'DataServiceVersion' => '2.0',
    'MaxDataServiceVersion' => '2.0',
    'xyz-cancel-on-close'=> 'true'    }
  
  BD = 'batch_d83d-823b-fb6a'

  def setup
    header 'MinDataServiceVersion', '2.0'
    header 'DataServiceVersion', '2.0'
    header 'MaxDataServiceVersion', '2.0'
  end

  # $batch works only with POST
  def test_batch_post_it
    get "#{self.class::TestPrefix}/$batch'"
    assert_equal(405, last_response.status)
  end


  def test_batch_1

    z = MIME::Content::Multipart::Base.new(BD)
    z.set_multipart_header

    z.content = ['Race/$count','Race?$skip=0&$top=3'].map{|uri|
      httpapp  = MIME::Content::Application::Http.new
      httpapp.content = MIME::Content::Application::HttpReq.new
      httpapp.content.http_method = 'GET'
      httpapp.content.uri = uri
      httpapp.content.hd = AH
      httpapp
    }


    z.hd.each{|k,v| header k, v }
    header 'Accept', OData::APPJSON

    post %{#{self.class::TestPrefix}/$batch}, z.unparse

    assert_equal(202, last_response.status)

# check that we get back a multipart response

    md = %r(multipart/mixed;\s*boundary=(\S+)).match(last_response.content_type)

    assert md
    @boundary = md[1]

# parse the returned multipart response and check the content

    mpresp = MIME::Media::Parser.new
    assert_nothing_raised do
      mpresp.hook_multipart(OData::MP_MIXED, @boundary)
      @mime = mpresp.parse_string(last_response.body)
    end
    assert_equal '200', @mime.content.first.content.status
    assert_equal OData::TEXTPLAIN_UTF8, @mime.content.first.content.hd['content-type']
    assert_equal Race.all.size.to_s, @mime.content.first.content.content

    assert_equal OData::APPJSON_UTF8, @mime.content.last.content.hd['content-type']

    # get the individual part data in a separate request
    # the response body should be same as for the multipart batch request (second part)
    get 'Race?$skip=0&$top=3'
    assert last_response.ok?
    assert_equal '200', @mime.content.last.content.status
    assert_equal last_response.body, @mime.content.last.content.content

  end
  
end


# with Enabled  $batch
class BatchTC < SailDBBatchTest
  TestPrefix = ''
  include  OData::Batch::TestHelper
end

# prefixed and with Enabled  $batch
class PrefixedBatchTC < PrefixedSailDBBatchTest
  TestPrefix = TCSailPrefix
  include  OData::Batch::TestHelper
  
end


# SAme as above with OData::CommonLogger enable
# with Enabled  $batch + logging
class BatchLoggingTC < SailDBBatchTest
  TestPrefix = ''
  include  OData::Batch::TestHelper
  def app
    Rack::OData::Builder.new do
      use Rack::ODataCommonLogger
      run ODataSailBatch.new
    end
  end
end

# prefixed and with Enabled  $batch +logging
class PrefixedBatchLoggingTC < PrefixedSailDBBatchTest
  TestPrefix = TCSailPrefix
  include  OData::Batch::TestHelper
  def app
    Rack::OData::Builder.new do
      use Rack::ODataCommonLogger
      run ODataSailPrefixedBatchApp.new
    end
  end
end