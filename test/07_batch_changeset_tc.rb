#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require_relative './test_saildb.rb' # Sail-DB testdata


# with Enabled  $batch
class BatchChangeSetTC < SailDBBatchTest

  def setup
    header 'MinDataServiceVersion', '2.0'
    header 'DataServiceVersion', '2.0'
    header 'MaxDataServiceVersion', '2.0'
  end


  def test_changeset_2

    # MVP testcase... just a simple merge-update in a changeset
    # the requested change is Race(14).type = 2 (changed from 1)
    inpstr = File.read('test/multipart/odata_changeset_2_body.txt')

    # check the data before

    assert_equal 1, Race[14].type

    header 'Accept', 'application/json'
    header 'Content-Type', 'multipart/mixed; boundary=batch_48f8-3aea-2f04'
    post '/$batch', inpstr
    assert_equal(202, last_response.status)

    # check that the change was made
    assert_equal 2, Race[14].type
  end
  def test_changeset_3

    # MVP testcase... just a two merge-updates in a changeset
    # the requested change are
    #    Race(14).crew_type = 2 (changed from 1)
    #    Race(14).periodicity = 3 (changed from 1)
    #    Race(15).crew_type = 10 (changed from 1)
    inpstr = File.read('test/multipart/odata_changeset_3_body.txt')

    # check the data before
    r14_type = Race[14].type
    r15_type = Race[15].type
    assert_equal 1, Race[14].crew_type
    assert_equal 1, Race[14].periodicity
    assert_equal 1, Race[15].crew_type

    header 'Accept', 'application/json'
    header 'Content-Type', 'multipart/mixed; boundary=batch_48f8-3aea-2f04'
    post '/$batch', inpstr
    assert_equal(202, last_response.status)

    # check that the change was made
    assert_equal 2, Race[14].crew_type
    assert_equal 3, Race[14].periodicity
    assert_equal 10, Race[15].crew_type

    # check that other fields are unchanged (MERGE)
    assert_equal r14_type, Race[14].type
    assert_equal r15_type, Race[15].type
  end

  def test_changeset_4_create

    # check the data before
    nbrace = Race.count

    # MVP testcase... just two creates

    inpstr = File.read('test/multipart/odata_changeset_4_create_body.txt')

    header 'Accept', 'application/json'
    header 'Content-Type', 'multipart/mixed; boundary=batch_48f8-3aea-2f04'
    post '/$batch', inpstr
    assert_equal(202, last_response.status)

    # we should have two more Race entities
    assert_equal nbrace+2, Race.count

    # check that we get back a multipart response

    md = %r(multipart/mixed;\s*boundary=(\S+)).match(last_response.content_type)

    assert md
    @boundary = md[1]

    # parse the returned multipart response and check the content

    mpresp = MIME::Media::Parser.new
    assert_nothing_raised do
      mpresp.hook_multipart('multipart/mixed', @boundary)
      @mime = mpresp.parse_string(last_response.body)
      @mimec = @mime.content
    end


    @btc0 = @mimec.first.content
    @changeset_response = @mimec[1].content
    @btc2 = @mimec[2].content

    @cset0 = @changeset_response[0].content
    @cset1 = @changeset_response[1].content

    # check status and content types of the parts
    # POST --> 201 json
    assert_equal '201', @cset0.status
    assert_equal 'application/json;charset=utf-8', @cset0.hd['content-type']

    # POST --> 201 json
    assert_equal '201', @cset1.status
    assert_equal 'application/json;charset=utf-8', @cset1.hd['content-type']

    # GET $count --> 200 text
    assert_equal '200', @btc0.status
    assert_equal 'text/plain;charset=utf-8', @btc0.hd['content-type']

    # GET $count --> 200 text
    assert_equal '200', @btc2.status
    assert_equal 'text/plain;charset=utf-8', @btc2.hd['content-type']

    # check that the first sub-request GET $count is correct
    # (issued before the changesed containing created)
    assert_equal nbrace.to_s, @btc0.content

    # we should have two more Race entities
    assert_equal nbrace+2, Race.count

    # check the content of the created entites
    # first POST with
    # {"name":"A copy of Race(12) with no content-ID", "type":1, "periodicity":3, "crew_type":1}
    assert_nothing_raised do
      @cset0_json = JSON.parse(@cset0.content)
      @j0 = @cset0_json['d']['results'].first
    end


    assert_equal @j0['name'], 'A copy of Race(12) with no content-ID'
    assert_equal @j0['type'], 1
    assert_equal @j0['periodicity'], 3
    assert_equal @j0['crew_type'], 1

    # second  POST with
    # {"name":"Another Race with no content-ID", "type":2, "periodicity":3, "crew_type":1}
    assert_nothing_raised do
      @cset1_json = JSON.parse(@cset1.content)
      @j1 = @cset1_json['d']['results'].first
    end
    assert_equal @j1['name'], 'Another Race with no content-ID'
    assert_equal @j1['type'], 2
    assert_equal @j1['periodicity'], 3
    assert_equal @j1['crew_type'], 1

    # check that the last sub-request GET $count is correct
    # (issued after the changesed containing created)
    assert_equal (nbrace+2).to_s, @btc2.content
  end

  def test_changeset_failed

    # innput request contains
    # * a changeset with
    # ** an invalid Race(14) MERGE update (wrong field names)
    # ** an valid Race(15) MERGE update
    # ** an valid Race(14) MERGE update
    # * a GET Race(14)
    inpstr = File.read('test/multipart/odata_changeset_fail_body.txt')

    # check the data before
    r14_type = Race[14].type
    r15_crew_type = Race[15].crew_type
    r15_type = Race[15].type

    header 'Accept', 'application/json'
    header 'Content-Type', 'multipart/mixed; boundary=batch_48f8-3aea-2f04'
    post '/$batch', inpstr
    assert_equal(202, last_response.status)

    # check that the change was NOT made
    assert_equal r15_crew_type, Race[15].crew_type

    # check that other fields are unchanged (MERGE)
    assert_equal r14_type, Race[14].type
    assert_equal r15_type, Race[15].type


    # check that we get back a multipart response

    md = %r(multipart/mixed;\s*boundary=(\S+)).match(last_response.content_type)

    assert md
    @boundary = md[1]

    # parse the returned multipart response and check the content

    mpresp = MIME::Media::Parser.new
    assert_nothing_raised do
      mpresp.hook_multipart('multipart/mixed', @boundary)
      @mime = mpresp.parse_string(last_response.body)
      @mimec = @mime.content
    end

    # changeset with an error that should cause failure
    @cset0 = @mimec.first

    # and then a simple GET Race(14) following the changeset
    @btc1 = @mimec[1].content


     # When a request within a change set fails, the change set response is not represented using the
     # multipart/mixed media type. Instead, a single response, using the application/http media type, is
     # returned that applies to all requests in the change set and MUST be a valid OData error response.
      assert_equal 'application/http', @cset0.hd['content-type']

      # this is just because safrano only supports json for now
      assert_equal 'application/json;charset=utf-8', @cset0.content.hd['content-type']

      # todo; more specific status values checks
      assert_not_equal '200', @cset0.content.hd['content-type']
      assert_not_equal '201', @cset0.content.hd['content-type']
      assert_not_equal '202', @cset0.content.hd['content-type']

    # check that the GET Race(14) outside of the changesed was executed ok
    assert_equal '200', @btc1.status
    assert_equal 'application/json;charset=utf-8', @btc1.hd['content-type']

    # check GET Race(14) result; especially that the third change of changeset
    # was not executed
    assert_nothing_raised do
      @btc1_json = JSON.parse(@btc1.content)
      @j1 = @btc1_json['d']
    end
    @btc1_json['d']['type'] = '1'
    @btc1_json['d']['crew_type'] = '1'
  end
end



