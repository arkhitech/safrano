BEGIN TRANSACTION;
 PRAGMA foreign_keys=OFF;
CREATE TABLE IF NOT EXISTS `photo` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	TEXT,
	`cultivar_id`	INTEGER,
	`content_type`	TEXT NOT NULL,
	FOREIGN KEY(`cultivar_id`) REFERENCES `cultivar`(`id`)
);
INSERT INTO `photo` (id,name,cultivar_id,content_type) VALUES (1,'Photo of Breeder WA Taylor',NULL,'image/png'),
 (2,'first Photo of Topaz ',5,'image/jpeg'),
 (3,'second Photo of Topaz',5,'image/jpeg'),
 (4,'third Photo of Topaz',5,'image/jpeg'),
 (5,'photo of Mr Baltet',NULL,'image/jpeg'),
 (6,'photo of Belle fille de Salins',19,'image/jpeg'),
 (7,'orphan photo',NULL,'image/jpeg'),
 (8,'photo of Mr James Grieve',NULL,'image/jpeg'),
 (9,'photo of Mr Diedrich Uhlhorn junior',NULL,'image/jpeg');
CREATE TABLE IF NOT EXISTS `parentage` (
	`cultivar_id`	INTEGER NOT NULL,
	`ptype_id`	INTEGER NOT NULL,
	`parent_id`	INTEGER NOT NULL,
	PRIMARY KEY(`cultivar_id`,`ptype_id`,`parent_id`),
	FOREIGN KEY(`ptype_id`) REFERENCES `par_type`(`id`)
);
INSERT INTO `parentage` (cultivar_id,ptype_id,parent_id) VALUES (3,3,4),
 (1,1,2),
 (1,2,3),
 (5,1,6),
 (5,2,7),
 (6,1,8),
 (6,2,9),
 (7,1,10),
 (7,2,9),
 (12,1,7),
 (12,2,11),
 (11,1,8),
 (11,2,13),
 (13,1,15),
 (13,2,14),
 (17,3,18);
CREATE TABLE IF NOT EXISTS `par_type` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`description`	TEXT
);
INSERT INTO `par_type` (id,description) VALUES (1,'mother'),
 (2,'father'),
 (3,'seedling');
CREATE TABLE IF NOT EXISTS `institute` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL
);
INSERT INTO `institute` (id,name) VALUES (1,'OVA Jork'),
 (2,'Versuchsstation Idaho (USA)'),
 (3,'Institut für Exp. Botanik, Prag'),
 (4,'Univ. Agric., Prag'),
 (5,'Dummy institute for delete testcase ID=5'),
 (6,'Dummy institute for delete testcase ID=6');
CREATE TABLE IF NOT EXISTS `cultivar` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL,
	`year`	INTEGER,
	`breeder_id`	INTEGER,
	`inst_id`	INTEGER,
	FOREIGN KEY(`breeder_id`) REFERENCES `breeder`(`id`),
	FOREIGN KEY(`inst_id`) REFERENCES `institute`(`id`)
);
INSERT INTO `cultivar` (id,name,year,breeder_id,inst_id) VALUES (1,'Gloster',1951,NULL,1),
 (2,'Weißer Winterglockenapfel',1865,NULL,NULL),
 (3,'Red Delicious',1870,1,2),
 (4,'Yellow Bellflower',1840,NULL,NULL),
 (5,'Topaz',1984,2,3),
 (6,'Rubin',1960,NULL,NULL),
 (7,'Vanda',NULL,NULL,NULL),
 (8,'Golden Delicious',NULL,NULL,NULL),
 (9,'Lord Lambourne',NULL,NULL,NULL),
 (10,'Jolana',NULL,NULL,NULL),
 (11,'Cripps Pink',NULL,NULL,NULL),
 (12,'Karneval','',NULL,NULL),
 (13,'Lady Williams',NULL,NULL,NULL),
 (14,'Granny Smith',NULL,NULL,NULL),
 (15,'Rokewood','',NULL,NULL),
 (16,'Laxton''s Herald',1906,3,NULL),
 (17,'Transparente de Croncels',1869,4,NULL),
 (18,'Antonovka',NULL,NULL,NULL),
 (19,'Belle fille de Salins',1900,5,NULL),
 (20,'James Grieve',1893,6,NULL),
 (21,'Berlepsch',1880,7,NULL) ,
 (22,'Pomme d''api',NULL,8,6) ,
 (23,'Köstliche aus Charneux',1800,NULL,NULL),
 (24,'Opal',NULL,2,NULL);
CREATE TABLE IF NOT EXISTS `breeder` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`first_name`	TEXT,
	`last_name`	TEXT,
	`photo_id`	INTEGER,
	FOREIGN KEY(`photo_id`) REFERENCES `photo`(`id`)
);
INSERT INTO `breeder` (id,first_name,last_name,photo_id) VALUES (1,'W.A.','Taylor',1),
(2,'Jaroslav','Túpy',NULL),
(3,'Thomas','Laxton',NULL),
(4,'Charles','Baltet',5),
(5,'Pierre','Baron',NULL),
(6,'James','Grieve',8),
(7,'Diedrich',' Uhlhorn junior',9),
(8,'Claudius Caecus','Appius',NULL);
COMMIT;
