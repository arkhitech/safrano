BEGIN TRANSACTION;
SET CONSTRAINTS ALL DEFERRED;
DROP TABLE IF EXISTS "par_type" CASCADE;
CREATE TABLE IF NOT EXISTS "par_type" (
  "id"  SERIAL NOT NULL PRIMARY KEY ,
  "description" TEXT
);
DROP TABLE IF EXISTS "photo" CASCADE;
CREATE TABLE IF NOT EXISTS "photo" (
	"id"	SERIAL NOT NULL PRIMARY KEY UNIQUE,
	"name"	TEXT,
  "cultivar_id" INTEGER DEFAULT NULL,
	"content_type"	TEXT NOT NULL
);
DROP TABLE IF EXISTS "parentage" CASCADE;
CREATE TABLE IF NOT EXISTS "parentage" (
  "cultivar_id" INTEGER NOT NULL,
  "ptype_id"  INTEGER NOT NULL,
  "parent_id" INTEGER NOT NULL,
  FOREIGN KEY("ptype_id") REFERENCES "par_type"("id"),
  PRIMARY KEY("cultivar_id","ptype_id","parent_id")
);
DROP TABLE IF EXISTS "breeder" CASCADE;
CREATE TABLE IF NOT EXISTS "breeder" (
  "id"  SERIAL NOT NULL PRIMARY KEY ,
  "first_name"  TEXT,
  "last_name" TEXT,
  "photo_id" INTEGER DEFAULT NULL,
  FOREIGN KEY("photo_id") REFERENCES "photo"("id") 
);
DROP TABLE IF EXISTS "institute" CASCADE;
CREATE TABLE IF NOT EXISTS "institute" (
  "id"  SERIAL NOT NULL PRIMARY KEY,
  "name"  TEXT NOT NULL
);
DROP TABLE IF EXISTS "cultivar" CASCADE;
CREATE TABLE IF NOT EXISTS "cultivar" (
  "id"  SERIAL NOT NULL PRIMARY KEY,
  "name"  TEXT NOT NULL,
  "year"  INTEGER,
  "breeder_id"  INTEGER,
  "inst_id" INTEGER,
  FOREIGN KEY("inst_id") REFERENCES "institute"("id"),
  FOREIGN KEY("breeder_id") REFERENCES "breeder"("id")
);
ALTER TABLE "photo" ADD FOREIGN KEY("cultivar_id") REFERENCES "cultivar"("id");

COMMIT;
