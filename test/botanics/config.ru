###########
# config.ru
#

#require 'rack/logger'

require_relative './dbmodel_run.rb'

puts "Safrano version #{Safrano::VERSION  }"

# docu for Builder --> https://www.tuicool.com/articles/yyIFri

final_odata_app = Rack::OData::Builder.new do
#  use Rack::ODataCommonLogger
#  use Rack::ShowExceptions
  run ODataBotanicsApp.new
end


Rack::Handler::Thin.run final_odata_app, :Port => 9494
