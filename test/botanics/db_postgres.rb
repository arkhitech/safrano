#!/usr/bin/env ruby

require_relative '../../lib/safrano.rb'
require 'sequel'
require_relative '../test_helper.rb'
require 'set'


refresh_postgresql_testdb('botanics')

DB[:botanics] = Sequel::Model.db = Sequel.postgres('ci_test',
                                                    host: 'postgres',
                                                    user: 'dm',
                                                    password: 'password')

at_exit { Sequel::Model.db.disconnect if Sequel::Model.db }
