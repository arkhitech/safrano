#!/usr/bin/env ruby

require_relative '../../lib/safrano.rb'
require 'sequel'
require_relative '../test_helper.rb'

refresh_sqlite_testdb('botanics')

dbrname = File.join(__dir__, 'botanics_test.db3')
DB[:botanics] = Sequel::Model.db = Sequel.sqlite(dbrname)
at_exit { Sequel::Model.db.disconnect if Sequel::Model.db }
