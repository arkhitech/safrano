#!/usr/bin/env ruby
#$LOAD_PATH << '../../lib/'
#require_relative '../../lib/safrano.rb'
require 'safrano'

require 'ruby-prof'
require 'thin'
  
require 'sequel'

  DB = {}

dbrname = File.join(__dir__, 'botanics_test.db3')
DB[:botanics] = Sequel::Model.db = Sequel.sqlite(dbrname)

PRF = nil
#PRF = RubyProf::Profile.new
#PRF.start
#PRF.pause

#DevLog = Logger.new(STDOUT)
#DB[:botanics] = Sequel::Model.db = Sequel.connect("sqlite://#{dbrname}", logger: DevLog)

at_exit {
 Sequel::Model.db.disconnect if Sequel::Model.db 
 if PRF
   result = PRF.stop
  # print a flat profile to text
printer = RubyProf::CallStackPrinter.new(result)
File.open('profile.html', 'w') do |f|
  printer.print(f)
end 

printer2 = RubyProf::FlatPrinter.new(result)
File.open('profile.flat', 'w') do |f2|
  printer2.print(f2)
end 
end
 }

require_relative './model.rb'
