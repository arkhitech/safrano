
class Photo < Sequel::Model(:photo)
end


class Cultivar < Sequel::Model(:cultivar)

    # Navigation attributes
    one_to_many :parent, :class => :Parentage, :key => :cultivar_id
    one_to_many :child, :class => :Parentage, :key => :parent_id
    one_to_many :photo, :class => :Photo, :key => :cultivar_id
    many_to_one :breeder, :key => :breeder_id
    many_to_one :institute, :class => :Institute, :key => :inst_id
end

class ParentageType < Sequel::Model(:par_type)
end

class Breeder < Sequel::Model(:breeder)
  one_to_many :cultivar, :key => :breeder_id
  many_to_one :photo, :key => :photo_id
end

class Institute < Sequel::Model(:institute)
end

class Parentage < Sequel::Model(:parentage)
  # A parentage is for a given cultivar; there can be multiple parentage records
  # (type: father or mother), or one (type seedling) or none
  #
    many_to_one :child, :class => :Cultivar, :key=>:cultivar_id
    many_to_one :parent, :class=> :Cultivar, :key=>:parent_id
    many_to_one :par_type, :class=>:ParentageType, :key => :ptype_id

end


class Photo < Sequel::Model(:photo)
end
### SERVER Part #############################

MediaRoot = File.absolute_path('media', File.dirname(__FILE__))
MediaFlatRoot = File.absolute_path('flat', MediaRoot)
MediaTreeRoot = File.absolute_path('tree', MediaRoot)
class ODataBotanicsApp < OData::ServerApp
  publish_service do

    title  'Cultivar Parentage OData API'
    name  'ParentageService'
    namespace  'ODataPar'
    server_url 'http://example.org'
    path_prefix '/'

    publish_media_model Photo do 
      slug :name
      use OData::Media::Static, :root => MediaFlatRoot
    end
    
    publish_model Cultivar do
      add_nav_prop_single :breeder
      add_nav_prop_single :institute
      add_nav_prop_collection :parent
      add_nav_prop_collection :child
      add_nav_prop_collection :photo
    end
    publish_model Breeder do 
      add_nav_prop_collection :cultivar 
      add_nav_prop_single :photo 
    end
    publish_model ParentageType
    publish_model Parentage, :ptree do
      add_nav_prop_single :parent
      add_nav_prop_single :child
      add_nav_prop_single :par_type
    end
    publish_model Institute
  end
end

