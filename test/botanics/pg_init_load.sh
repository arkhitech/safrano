#!/bin/sh

export PGPASSWORD=password
export POSTGRES_USER=dm
export PGOPTIONS='--client-min-messages=warning'

#dropdb -h "postgres" -U "$POSTGRES_USER" ci_test --if-exists
#createdb -h "postgres" -U "$POSTGRES_USER" -l 'C.UTF-8' -T template0 ci_test
psql -q -h "postgres" -U "$POSTGRES_USER" ci_test < botanics_v0_schema.sql > pg_init_load.log
psql -q -h "postgres" -U "$POSTGRES_USER" ci_test < botanics_v0_data.sql >> pg_init_load.log

