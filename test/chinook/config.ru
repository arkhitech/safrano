###########
# config.ru
#

$LOAD_PATH << '../../lib/'

require './model.rb'

# docu for Builder --> https://www.tuicool.com/articles/yyIFri

final_odata_app = Rack::OData::Builder.new do
  run ODataChinookApp.new
end

Rack::Handler::Thin.run final_odata_app, Port: 9797
#Rack::Handler::WEBrick.run final_odata_app, Port: 9797
