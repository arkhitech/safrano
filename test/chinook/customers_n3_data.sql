BEGIN TRANSACTION;
SET CONSTRAINTS ALL DEFERRED;
DROP TABLE IF EXISTS "Customer";
CREATE TABLE "Customer"
(
    "CustomerId" INT NOT NULL,
    "FirstName" VARCHAR(40) NOT NULL,
    "LastName" VARCHAR(20) NOT NULL,
    "Company" VARCHAR(80),
    "Address" VARCHAR(70),
    "City" VARCHAR(40),
    "State" VARCHAR(40),
    "Country" VARCHAR(40),
    "PostalCode" VARCHAR(10),
    "Phone" VARCHAR(24),
    "Fax" VARCHAR(24),
    "Email" VARCHAR(60) NOT NULL,
    "SupportRepId" INT,
    "TechSupportId" INT,
    CONSTRAINT "PK_Customer" PRIMARY KEY  ("CustomerId")
);

INSERT INTO "Customer" ("CustomerId","FirstName","LastName","Company","Address","City","State","Country","PostalCode","Phone","Fax","Email","SupportRepId","TechSupportId") VALUES (1,'Luís','Gonçalves','Embraer - Empresa Brasileira de Aeronáutica S.A.','Av. Brigadeiro Faria Lima, 2170','São José dos Campos','SP','Brazil','12227-000',NULL,'+55 (12) 3923-5566','luisg@embraer.com.br',3,10),
 (2,'Leonie','Köhler',NULL,'Theodor-Heuss-Straße 34','Stuttgart',NULL,'Germany','70174',NULL,NULL,'leonekohler@surfeu.de',5,10),
 (3,'François','Tremblay',NULL,'1498 rue Bélanger','Montréal','QC','Canada','H2G 1A7',NULL,NULL,'ftremblay@gmail.com',3,10),
 (4,'Bjørn','Hansen',NULL,'Ullevålsveien 14','Oslo',NULL,'Norway','0171',NULL,NULL,'bjorn.hansen@yahoo.no',4,12),
 (5,'František','Wichterlová','JetBrains s.r.o.','Klanova 9/506','Prague',NULL,'Czech Republic','14700',NULL,'+420 2 4172 5555','frantisekw@jetbrains.com',4,11),
 (6,'Helena','Holý',NULL,'Rilská 3174/6','Prague',NULL,'Czech Republic','14300',NULL,NULL,'hholy@gmail.com',5,11),
 (7,'Astrid','Gruber',NULL,'Rotenturmstraße 4, 1010 Innere Stadt','Vienne',NULL,'Austria','1010',NULL,NULL,'astrid.gruber@apple.at',5,11),
 (8,'Daan','Peeters',NULL,'Grétrystraat 63','Brussels',NULL,'Belgium','1000',NULL,NULL,'daan_peeters@apple.be',4,10),
 (9,'Kara','Nielsen',NULL,'Sønder Boulevard 51','Copenhagen',NULL,'Denmark','1720',NULL,NULL,'kara.nielsen@jubii.dk',4,11),
 (10,'Eduardo','Martins','Woodstock Discos','Rua Dr. Falcão Filho, 155','São Paulo','SP','Brazil','01007-010',NULL,'+55 (11) 3033-4564','eduardo@woodstock.com.br',4,10),
 (11,'Alexandre','Rocha','Banco do Brasil S.A.','Av. Paulista, 2022','São Paulo','SP','Brazil','01310-200',NULL,'+55 (11) 3055-8131','alero@uol.com.br',5,12),
 (12,'Roberto','Almeida','Riotur','Praça Pio X, 119','Rio de Janeiro','RJ','Brazil','20040-020',NULL,'+55 (21) 2271-7070','roberto.almeida@riotur.gov.br',3,12),
 (13,'Fernanda','Ramos',NULL,'Qe 7 Bloco G','Brasília','DF','Brazil','71020-677',NULL,'+55 (61) 3363-7855','fernadaramos4@uol.com.br',4,12),
 (14,'Mark','Philips','Telus','8210 111 ST NW','Edmonton','AB','Canada','T6G 2C7',NULL,'+1 (780) 434-5565','mphilips12@shaw.ca',5,10),
 (15,'Jennifer','Peterson','Rogers Canada','700 W Pender Street','Vancouver','BC','Canada','V6C 1G8',NULL,'+1 (604) 688-8756','jenniferp@rogers.ca',3,10),
 (16,'Frank','Harris','Google Inc.','1600 Amphitheatre Parkway','Mountain View','CA','USA','94043-1351',NULL,'+1 (650) 253-0000','fharris@google.com',4,10),
 (17,'Jack','Smith','Microsoft Corporation','1 Microsoft Way','Redmond','WA','USA','98052-8300',NULL,'+1 (425) 882-8081','jacksmith@microsoft.com',5,10),
 (18,'Michelle','Brooks',NULL,'627 Broadway','New York','NY','USA','10012-2612',NULL,'+1 (212) 221-4679','michelleb@aol.com',3,10),
 (19,'Tim','Goyer','Apple Inc.','1 Infinite Loop','Cupertino','CA','USA','95014',NULL,'+1 (408) 996-1011','tgoyer@apple.com',3,10),
 (20,'Dan','Miller',NULL,'541 Del Medio Avenue','Mountain View','CA','USA','94040-111',NULL,NULL,'dmiller@comcast.com',4,10),
 (21,'Kathy','Chase',NULL,'801 W 4th Street','Reno','NV','USA','89503',NULL,NULL,'kachase@hotmail.com',5,11),
 (22,'Heather','Leacock',NULL,'120 S Orange Ave','Orlando','FL','USA','32801',NULL,NULL,'hleacock@gmail.com',4,11),
 (23,'John','Gordon',NULL,'69 Salem Street','Boston','MA','USA','2113',NULL,NULL,'johngordon22@yahoo.com',4,11),
 (24,'Frank','Ralston',NULL,'162 E Superior Street','Chicago','IL','USA','60611',NULL,NULL,'fralston@gmail.com',3,11),
 (25,'Victor','Stevens',NULL,'319 N. Frances Street','Madison','WI','USA','53703',NULL,NULL,'vstevens@yahoo.com',5,11),
 (26,'Richard','Cunningham',NULL,'2211 W Berry Street','Fort Worth','TX','USA','76110',NULL,NULL,'ricunningham@hotmail.com',4,11),
 (27,'Patrick','Gray',NULL,'1033 N Park Ave','Tucson','AZ','USA','85719',NULL,NULL,'patrick.gray@aol.com',4,11),
 (28,'Julia','Barnett',NULL,'302 S 700 E','Salt Lake City','UT','USA','84102',NULL,NULL,'jubarnett@gmail.com',5,11),
 (29,'Robert','Brown',NULL,'796 Dundas Street West','Toronto','ON','Canada','M6J 1V1',NULL,NULL,'robbrown@shaw.ca',3,11),
 (30,'Edward','Francis',NULL,'230 Elgin Street','Ottawa','ON','Canada','K2P 1L7',NULL,NULL,'edfrancis@yachoo.ca',3,11),
 (31,'Martha','Silk',NULL,'194A Chain Lake Drive','Halifax','NS','Canada','B3S 1C5',NULL,NULL,'marthasilk@gmail.com',5,11),
 (32,'Aaron','Mitchell',NULL,'696 Osborne Street','Winnipeg','MB','Canada','R3L 2B9',NULL,NULL,'aaronmitchell@yahoo.ca',4,11),
 (33,'Ellie','Sullivan',NULL,'5112 48 Street','Yellowknife','NT','Canada','X1A 1N6',NULL,NULL,'ellie.sullivan@shaw.ca',3,11),
 (34,'João','Fernandes',NULL,'Rua da Assunção 53','Lisbon',NULL,'Portugal',NULL,NULL,NULL,'jfernandes@yahoo.pt',4,11),
 (35,'Madalena','Sampaio',NULL,'Rua dos Campeões Europeus de Viena, 4350','Porto',NULL,'Portugal',NULL,NULL,NULL,'masampaio@sapo.pt',4,11),
 (36,'Hannah','Schneider',NULL,'Tauentzienstraße 8','Berlin',NULL,'Germany','10789',NULL,NULL,'hannah.schneider@yahoo.de',5,11),
 (37,'Fynn','Zimmermann',NULL,'Berger Straße 10','Frankfurt',NULL,'Germany','60316',NULL,NULL,'fzimmermann@yahoo.de',3,11),
 (38,'Niklas','Schröder',NULL,'Barbarossastraße 19','Berlin',NULL,'Germany','10779',NULL,NULL,'nschroder@surfeu.de',3,11),
 (39,'Camille','Bernard',NULL,'4, Rue Milton','Paris',NULL,'France','75009',NULL,NULL,'camille.bernard@yahoo.fr',4,11),
 (40,'Dominique','Lefebvre',NULL,'8, Rue Hanovre','Paris',NULL,'France','75002',NULL,NULL,'dominiquelefebvre@gmail.com',4,11),
 (41,'Marc','Dubois',NULL,'11, Place Bellecour','Lyon',NULL,'France','69002',NULL,NULL,'marc.dubois@hotmail.com',5,11),
 (42,'Wyatt','Girard',NULL,'9, Place Louis Barthou','Bordeaux',NULL,'France','33000',NULL,NULL,'wyatt.girard@yahoo.fr',3,11),
 (43,'Isabelle','Mercier',NULL,'68, Rue Jouvence','Dijon',NULL,'France','21000',NULL,NULL,'isabelle_mercier@apple.fr',3,11),
 (44,'Terhi','Hämäläinen',NULL,'Porthaninkatu 9','Helsinki',NULL,'Finland','00530',NULL,NULL,'terhi.hamalainen@apple.fi',3,11),
 (45,'Ladislav','Kovács',NULL,'Erzsébet krt. 58.','Budapest',NULL,'Hungary','H-1073',NULL,NULL,'ladislav_kovacs@apple.hu',3,11),
 (46,'Hugh','O''Reilly',NULL,'3 Chatham Street','Dublin','Dublin','Ireland',NULL,NULL,NULL,'hughoreilly@apple.ie',3,11),
 (47,'Lucas','Mancini',NULL,'Via Degli Scipioni, 43','Rome','RM','Italy','00192',NULL,NULL,'lucas.mancini@yahoo.it',5,11),
 (48,'Johannes','Van der Berg',NULL,'Lijnbaansgracht 120bg','Amsterdam','VV','Netherlands','1016',NULL,NULL,'johavanderberg@yahoo.nl',5,11),
 (49,'Stanisław','Wójcik',NULL,'Ordynacka 10','Warsaw',NULL,'Poland','00-358',NULL,NULL,'stanisław.wójcik@wp.pl',4,11),
 (50,'Enrique','Muñoz',NULL,'C/ San Bernardo 85','Madrid',NULL,'Spain','28015',NULL,NULL,'enrique_munoz@yahoo.es',5,11),
 (51,'Joakim','Johansson',NULL,'Celsiusg. 9','Stockholm',NULL,'Sweden','11230',NULL,NULL,'joakim.johansson@yahoo.se',5,11),
 (52,'Emma','Jones',NULL,'202 Hoxton Street','London',NULL,'United Kingdom','N1 5LH',NULL,NULL,'emma_jones@hotmail.com',3,11),
 (53,'Phil','Hughes',NULL,'113 Lupus St','London',NULL,'United Kingdom','SW1V 3EN',NULL,NULL,'phil.hughes@gmail.com',3,11),
 (54,'Steve','Murray',NULL,'110 Raeburn Pl','Edinburgh ',NULL,'United Kingdom','EH4 1HH',NULL,NULL,'steve.murray@yahoo.uk',5,11),
 (55,'Mark','Taylor',NULL,'421 Bourke Street','Sidney','NSW','Australia','2010',NULL,NULL,'mark.taylor@yahoo.au',4,11),
 (56,'Diego','Gutiérrez',NULL,'307 Macacha Güemes','Buenos Aires',NULL,'Argentina','1106',NULL,NULL,'diego.gutierrez@yahoo.ar',4,11),
 (57,'Luis','Rojas',NULL,'Calle Lira, 198','Santiago',NULL,'Chile',NULL,NULL,NULL,'luisrojas@yahoo.cl',5,11),
 (58,'Manoj','Pareek',NULL,'12,Community Centre','Delhi',NULL,'India','110017',NULL,NULL,'manoj.pareek@rediff.com',3,11),
 (59,'Puja','Srivastava',NULL,'3,Raj Bhavan Road','Bangalore',NULL,'India','560001',NULL,NULL,'puja_srivastava@yahoo.in',3,11),
 (60,'Robin','Malkovitch',NULL,NULL,NULL,NULL,'India',NULL,NULL,NULL,'robin@malkovitch.in',3,11);

/*******************************************************************************
   Create Primary Key Unique Indexes
********************************************************************************/

/*******************************************************************************
   Create Foreign Keys
********************************************************************************/
ALTER TABLE "Album" ADD CONSTRAINT "FK_AlbumArtistId"
    FOREIGN KEY ("ArtistId") REFERENCES "Artist" ("ArtistId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_AlbumArtistId" ON "Album" ("ArtistId");

ALTER TABLE "Customer" ADD CONSTRAINT "FK_CustomerSupportRepId"
    FOREIGN KEY ("SupportRepId") REFERENCES "Employee" ("EmployeeId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_CustomerSupportRepId" ON "Customer" ("SupportRepId");

ALTER TABLE "Customer" ADD CONSTRAINT "FK_CustomerTechSupportId"
    FOREIGN KEY ("TechSupportId") REFERENCES "Employee" ("EmployeeId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_CustomerTechSupportId" ON "Customer" ("TechSupportId");

ALTER TABLE "Employee" ADD CONSTRAINT "FK_EmployeeReportsTo"
    FOREIGN KEY ("ReportsTo") REFERENCES "Employee" ("EmployeeId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_EmployeeReportsTo" ON "Employee" ("ReportsTo");

ALTER TABLE "Invoice" ADD CONSTRAINT "FK_InvoiceCustomerId"
    FOREIGN KEY ("CustomerId") REFERENCES "Customer" ("CustomerId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_InvoiceCustomerId" ON "Invoice" ("CustomerId");

ALTER TABLE "InvoiceLine" ADD CONSTRAINT "FK_InvoiceLineInvoiceId"
    FOREIGN KEY ("InvoiceId") REFERENCES "Invoice" ("InvoiceId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_InvoiceLineInvoiceId" ON "InvoiceLine" ("InvoiceId");

ALTER TABLE "InvoiceLine" ADD CONSTRAINT "FK_InvoiceLineTrackId"
    FOREIGN KEY ("TrackId") REFERENCES "Track" ("TrackId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_InvoiceLineTrackId" ON "InvoiceLine" ("TrackId");

ALTER TABLE "PlaylistTrack" ADD CONSTRAINT "FK_PlaylistTrackPlaylistId"
    FOREIGN KEY ("PlaylistId") REFERENCES "Playlist" ("PlaylistId") ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE "PlaylistTrack" ADD CONSTRAINT "FK_PlaylistTrackTrackId"
    FOREIGN KEY ("TrackId") REFERENCES "Track" ("TrackId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_PlaylistTrackTrackId" ON "PlaylistTrack" ("TrackId");

ALTER TABLE "Track" ADD CONSTRAINT "FK_TrackAlbumId"
    FOREIGN KEY ("AlbumId") REFERENCES "Album" ("AlbumId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_TrackAlbumId" ON "Track" ("AlbumId");

ALTER TABLE "Track" ADD CONSTRAINT "FK_TrackGenreId"
    FOREIGN KEY ("GenreId") REFERENCES "Genre" ("GenreId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_TrackGenreId" ON "Track" ("GenreId");

ALTER TABLE "Track" ADD CONSTRAINT "FK_TrackMediaTypeId"
    FOREIGN KEY ("MediaTypeId") REFERENCES "MediaType" ("MediaTypeId") ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX "IFK_TrackMediaTypeId" ON "Track" ("MediaTypeId");


COMMIT;
