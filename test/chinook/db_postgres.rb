#!/usr/bin/env ruby

require_relative '../../lib/safrano.rb'
require_relative '../test_helper.rb'
require 'set'


refresh_postgresql_testdb('chinook')

Sequel::Model.db = Sequel.postgres('ci_test', host: 'postgres',
                                              user: 'dm',
                                              password: 'password')

begin
  DB[:chinook] =  Sequel::Model.db
rescue NameError
  DB = {}
  DB[:chinook] =  Sequel::Model.db
end

at_exit { Sequel::Model.db.disconnect if Sequel::Model.db }


