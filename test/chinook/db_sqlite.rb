#!/usr/bin/env ruby

require_relative '../../lib/safrano.rb'
require_relative '../test_helper.rb'

refresh_sqlite_testdb('chinook')

dbrname = File.join(__dir__, 'chinookn3_test.db3')
DB[:chinook] = Sequel::Model.db = Sequel.sqlite(dbrname)
at_exit { Sequel::Model.db.disconnect if Sequel::Model.db }



