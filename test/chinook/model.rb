#!/usr/bin/env ruby

require_relative './db.rb'

if Sequel::Model.db.adapter_scheme == :sqlite
# sqlite uses plurilised table names
  class Customer < Sequel::Model(:customers) ; end

  class Employee < Sequel::Model(:employees) ; end

  class Artist < Sequel::Model(:artists) ; end

  class Album < Sequel::Model(:albums)
    many_to_one :artist, key: :ArtistId
  end

  class Track  < Sequel::Model(:tracks)
    many_to_one :album, key:  :AlbumId
  end
  class PlaylistTrack < Sequel::Model(:playlist_track) ; end

  class Playlist < Sequel::Model(:playlists) ; end

  class InvoiceItem < Sequel::Model(:invoice_items)
      many_to_one :track, :class => Track, :key => :TrackId
  end
  class Invoice < Sequel::Model(:invoices)
    many_to_one :customer,  :key => :CustomerId
    one_to_many :invoice_items, :key => :InvoiceId
  end
elsif Sequel::Model.db.adapter_scheme == :postgres
  # Postgres-chinook uses singular table names
  class Customer < Sequel::Model(:Customer) ; end

  class Employee < Sequel::Model(:Employee) ; end

  class Artist < Sequel::Model(:Artist) ; end

  class Album < Sequel::Model(:Album)
    many_to_one :artist, key: :ArtistId
  end

  class Track  < Sequel::Model(:Track)
    many_to_one :album, key:  :AlbumId
  end
  class PlaylistTrack < Sequel::Model(:PlaylistTrack) ;end

  class Playlist < Sequel::Model(:Playlist) ; end

  class InvoiceItem < Sequel::Model(:InvoiceLine)
      many_to_one :track, :class => Track, :key => :TrackId
  end
  class Invoice < Sequel::Model(:Invoice)
    many_to_one :customer,  :key => :CustomerId
    one_to_many :invoice_items, :key => :InvoiceId
  end
end
class Track
  many_to_many :playlists, :class => Playlist, :left_key => :TrackId,
                :right_key => :playlistId,
                :join_table => :playlist_track
end

class Album
  one_to_many :tracks, key: :AlbumId
end

class Customer
  many_to_one :salesrep, :class => Employee, :key => :SupportRepId
  many_to_one :techsupport, :class => Employee, :key => :TechSupportId
  one_to_many :invoices,  :key => :CustomerId
end

class Employee
#  bug in Sequel ? this results in Stack level too deep error
#  many_to_one :ReportsTo, :class => Employee, :key => :ReportsTo

# this works
  many_to_one :repto, :class => Employee, :key => :ReportsTo
end

# set a default timezone otherwise Sequel uses Time.parse to load DATETIME columns, wich would pull the server
# timezone in case the column does not have a TZ
Sequel.default_timezone=:utc

### ODATA SERVER Part #############################

class ODataChinookApp < OData::ServerApp
  publish_service do

    title  'Chinook OData API'
    name  'ChinookService'
    namespace  'ODataChinook'
    server_url 'http://example.org'
    path_prefix '/'

    publish_model Album do
      add_nav_prop_collection :tracks
      add_nav_prop_single :artist
    end
    publish_model Artist
    publish_model Track do add_nav_prop_single :album end

    publish_model InvoiceItem do
      add_nav_prop_single :track
    end

    # This allows to ask for  /Employee(1)/repto
    publish_model Employee do add_nav_prop_single :repto end

    publish_model Customer do
      # This allows to ask for  /Customer(1)/salesrep
      add_nav_prop_single :salesrep

      # This allows to ask for  /Customer(1)/techsupport
      add_nav_prop_single :techsupport

    # This allows to ask for  /Customer(1)/invoices
      add_nav_prop_collection :invoices
    end

    publish_model Invoice do
      add_nav_prop_collection :invoice_items
      add_nav_prop_single :customer
    end

  end
end


#TODO: Fix API
class ODataChinookBatch < OData::ServerApp
  set_servicebase(ODataChinookApp.get_service_base.dup)
  enable_batch
end

class ODataChinookPrefixedApp < OData::ServerApp
  set_servicebase(ODataChinookApp.get_service_base.dup)
  path_prefix '/chinook.svc'
end
