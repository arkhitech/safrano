require 'json'
require 'time'

# client parsing functionality to ease testing

module OData
  module XJSON
    # symbolise keys and cast/parse values back to the proper ruby type;
    # proper type meaning the one that Sequel chooses when loading data from the DB
    # apply recursively to nested navigation attributes sub-structures
    def self.cast_values_in(resd)
      resd.symbolize_keys!

      if (defered = resd[:__deferred])
        defered.symbolize_keys!
      elsif meta = resd[:__metadata]
        meta.symbolize_keys!

        typ = meta[:type].constantize

        typ.db_schema.each{|f,props|
          case props[:type]
            when :datetime
              resd[f] = Time.parse(resd[f]) if resd[f]
            when :decimal
              resd[f] = BigDecimal(resd[f])
            when :float
              resd[f] = Float(resd[f])
          end
        }

        typ.nav_entity_attribs.each_key{|nattr|
          cast_values_in(resd[nattr.to_sym]) if resd[nattr.to_sym]
        } if typ.nav_entity_attribs

        typ.nav_collection_attribs.each_key{|ncattr|
          if (resd_attr = resd[ncattr.to_sym])
            if (defered = resd_attr['__deferred'])
              defered.symbolize_keys! if defered
            else
              resd_attr.symbolize_keys!  #  'results' --> :results
              resd_attr[:results].each{|enty|     cast_values_in(enty)}
            end
          end
        }   if typ.nav_collection_attribs
      end
      resd
    end

    # symbolise attrib names (h-keys)
    # apply recursively to nested navigation attributes sub-structures
    def self.symbolize_attribs_in(resd)
      resd.symbolize_keys!

      if (defered = resd[:__deferred])
        defered.symbolize_keys!
      elsif meta = resd[:__metadata]
        meta.symbolize_keys!

        typ = meta[:type].constantize

        typ.nav_entity_attribs.each_key{|nattr|
          symbolize_attribs_in(resd[nattr.to_sym])
        } if typ.nav_entity_attribs

        typ.nav_collection_attribs.each_key{|ncattr|
          if (defered = resd[ncattr.to_sym]['__deferred'])
            defered.symbolize_keys!
          else
            resd[ncattr.to_sym].each{|enty|     symbolize_attribs_in(enty)}
          end
        }   if typ.nav_collection_attribs

      end
      resd
    end

    def self.parse_one_nocast(*args)

      resh = JSON.parse(*args)
      symbolize_attribs_in(resh['d'])

    end

    def self.parse_one(*args)

      resh = JSON.parse(*args)
      cast_values_in(resh['d'])

    end

    def self.v1_parse_coll(*args)

      resh = JSON.parse(*args)

      resh['d'].map!{|currd|    cast_values_in(currd)   }

      return resh['d']
    end

    def self.parse_coll(*args)

      resh = JSON.parse(*args)

      resh['d']['results'].map!{|currd|    cast_values_in(currd)   }

      return resh['d']
    end
    
    def self.v1_parse_coll_nocast(*args)

      resh = JSON.parse(*args)

      resh['d'].map!{|currd|    symbolize_attribs_in(currd)   }

      return resh['d']
    end

    def self.parse_coll_nocast(*args)

      resh = JSON.parse(*args)

      resh['d']['results'].map!{|currd|    symbolize_attribs_in(currd)   }

      return resh['d']
    end    
  end
end
