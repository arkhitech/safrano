
--batch_48f8-3aea-2f04
Content-Type: multipart/mixed; boundary=changeset_fa99-0523-ad8a

--changeset_fa99-0523-ad8a
Content-Type: application/http
Content-Transfer-Encoding: binary
Content-ID: 1

POST Race HTTP/1.1
Accept: application/json
Content-Type: application/json

{"name":"A copy of Race(12) with content-ID 1", "type":1, "periodicity":3, "crew_type":1}


--changeset_fa99-0523-ad8a
Content-Type: application/http
Content-Transfer-Encoding: binary
Content-ID: 2

POST Race HTTP/1.1
Accept: application/json
Content-Type: application/json

{"name":"A copy of Race(12) with content-ID 2", "type":1, "periodicity":3, "crew_type":1}

--changeset_fa99-0523-ad8a
Content-Type: application/http
Content-Transfer-Encoding: binary

GET $2 HTTP/1.1
Accept: application/json

--changeset_fa99-0523-ad8a
Content-Type: application/http
Content-Transfer-Encoding: binary

GET $1/rtype/description HTTP/1.1
Accept: application/json

--changeset_fa99-0523-ad8a
Content-Type: application/http
Content-Transfer-Encoding: binary

GET $1/id/$value HTTP/1.1
Accept: application/json

--changeset_fa99-0523-ad8a--

--batch_48f8-3aea-2f04--
