require_relative './client.rb'

module TM_Model_Compare_Helper

  def assert_sequel_values_equal(expected, resulth)
    #type
    assert_equal(expected.model.to_s, resulth[:__metadata][:type])

    # values
    expected.each{|k,v| assert_equal v, resulth[k] }
  end

  def assert_sequel_media_values_equal(expmedia, resulth, ressource_version='1', &proc)
    #type
    assert_equal(expmedia.model.to_s, resulth[:__metadata][:type])

    expmedia.each{|k,v| 
      case k
        when :content_type
           # media related values --> metadata
        assert_equal v,  resulth[:__metadata][k]
        when :media_src # obsolete here, will be removed from table/model
      else
    # non-media values
        assert_equal v, resulth[k] 
      end
      }
    # media src
     assert_equal "#{uribase(last_request)}/#{proc.call(expmedia)}/$value?version=#{ressource_version}",
                  resulth[:__metadata][:media_src]
    # edit media
    assert_equal "#{uribase(last_request)}/#{proc.call(expmedia)}/$value",
                  resulth[:__metadata][:edit_media]
  end

  # helper method to transform expected collection and returned json collection
  # into comparable set objects
  def make_comparable_coll_sets(expcoll, rescollh)
    exptype = expcoll.first.model.to_s
    exptab = expcoll.map{|exp|   {type: exptype,   values: exp.values }   }
    fields = expcoll.first.model.db_schema.keys

    restab = rescollh.map{|resh|
        pruned = {}
        fields.each{|f| pruned[f] = resh[f] }
        
      {type: resh[:__metadata][:type],
      values: pruned}
    }
    [exptab.to_set, restab.to_set]
  end
  
  # compare collections using Set (unordered) and compare type and values
  def assert_sequel_coll_values_equal(expcoll, rescollh)
    # we compare Set's of {:type=>... 
    #                      :values=>{} } hashes
    if expcoll.empty?
      assert_empty rescollh
    else
      expset, result_set = make_comparable_coll_sets(expcoll, rescollh)
      assert_equal expset, result_set
    end
  end

  # same as assert_sequel_coll_values_equal but does check that
  # result set is *included* or equal to the provided expected set
  # thus we are able to test limited result sets without knowing anything
  # about ordering 
  def assert_sequel_coll_set_included(expcoll, rescollh)
    # we compare Set's of {:type=>... 
    #                      :values=>{} } hashes
    if expcoll.empty?
      assert_empty rescollh
    else
      expset, result_set = make_comparable_coll_sets(expcoll, rescollh)
      assert result_set.subset?(expset)
    end
  
  end
  # compare collections content regardless of ordering (use Set)
  def assert_sequel_coll_set_equal(expcoll, rescoll)
    if expcoll.empty?
      assert_empty rescoll
    else
      # remove metadata and nav attribs from rescoll in order to be able
      # to compare just the direct values
      fields = expcoll.first.model.db_schema.keys
      prunedres = rescoll.map{ |res|
        pruned = {}
        fields.each{|f| pruned[f] = res[f] }
        pruned
      }
      assert_equal expcoll.map{|o| o.values}.to_set, prunedres.to_set
    end
  end

  # compare collections content regardless of ordering (use Set) with selected field
  def assert_sequel_coll_sel_set_equal(expcoll, rescoll, sel)
    if expcoll.empty?
      assert_empty rescoll
    else
      # 0 cleanup not relevant __metadata
      rescoll.each{|res| res.delete(:__metadata)}
      # 1. check that we have the expected data
      # remove nav attribs from rescoll in order to be able
      # to compare just the direct values of selected fields
      klass = expcoll.first.class
      selprops = klass.columns.map(&:to_s) & sel
      
      prunedres = rescoll.map{ |res|
        pruned = {}
        selprops.each{|f| pruned[f] = res[f] }
        pruned
      }
      prunedexp = expcoll.map{ |exp|
        pruned = {}
        selprops.each{|f| pruned[f] = exp.values[f] }
        pruned
      }
      assert_equal prunedexp.to_set, prunedres.to_set
      
      
      # 2. check that we dont have additional unwanted data
      sel_set = sel.to_set
      rescoll.each{|res|  
        fields = res.keys.to_set
        assert_equal sel_set, fields, "Exported fields are not ok : #{fields.to_a.join(' ')}"
      }
    end
  end


  def  assert_sequel_coll_ord_desc_by(y, &proc)
    y[0...-1].each_with_index{|elmt, idx|
      assert((proc.call(elmt) <=> proc.call(y[idx+1])) >= 0,
      "idx #{idx} #{proc.call(elmt).inspect} is not larger as #{proc.call(y[idx+1]).inspect}")  #descending
    }
  end

  def  assert_sequel_coll_ord_asc_by(y, &proc)
    y[0...-1].each_with_index{|elmt, idx|
      assert((proc.call(elmt) <=> proc.call(y[idx+1])) <= 0,
      "idx #{idx} #{proc.call(elmt).inspect} is not smaller as #{proc.call(y[idx+1]).inspect}")  #ascending
    }
  end
end

class ODataModelTestCase < Test::Unit::TestCase
  include Rack::Test::Methods
  include TM_Model_Compare_Helper

  def uribase(last_request)
    e = last_request.env
    uribase = %Q(#{e['rack.url_scheme']}://#{e['HTTP_HOST']})
  end

  # Reuseable get (some collection) with filter/order and asserts expected result
  # assumes the following input is set : 
  
  # @collpath   : collection ressource path, eg: Race or Customer(1)/invoices
  # @etype  : the expected collection type(class name)
  # @filtstr : the value of $filter parameter
  # @adpar: optionally the value of $orderby parameter (or nil)
  # @toppar optionally the value of $top parameter (or nil)
  # @ecollbase: the expected unfiltered collection corresponding to @collpath
  #  method proc &filt: a filtering proc when applied to @ecollbase yields the final 
  #        expected result
  # OR alternatively to @ecollbase + filt
  #  method argument ecollfinal the final expected filtered collection
  
  # Usage; 
  #  gcollfilt Invoice.where{ val > 3 }.all  OR 
  #  gcollfilt {|i| i.val > 3  } 
  def gcollfilt( ecollfinal = nil, &filt)
    getcoll
    expected_tab = if block_given?
      @ecollbase.select{|row| filt.call(row) } 
    else
      ecollfinal
    end
    # expected result can be limited by @toppar if provided
    if @toppar
      exp_count =  [expected_tab.count, @toppar.to_i].min 
      assert_last_resp_coll_parseable_ok(@etype , exp_count)
      # we check that the limited result is *contained* in the expected 
      # *unlimited* collection set 
      assert_sequel_coll_set_included(expected_tab, @jresd_coll)
    else
      assert_last_resp_coll_parseable_ok(@etype , expected_tab.count)
      assert_sequel_coll_values_equal(expected_tab, @jresd_coll)
    end
  end
  
  def parse_error_response
    @jrawd = JSON.parse last_response.body
    @jmessage = @jrawd['odata.error']['message']
  end
  
  # check that it's a bad request and that the err.msg contains the expected keywords
  def assert_bad_request(keywords)
    assert last_response.bad_request?
    parse_error_response
    assert @jmessage, "Returned message is nil"
    assert @jmessage.kind_of?(String), "Returned message is not a String but a #{@jmessage.class.to_s}"
    keywords.each{|kw|
      assert @jmessage.include?(kw), "Keyword '#{kw}' not in message : //#{@jmessage}//"
    }
  end
  
  def gcollfilt_not_implemented(funcname)
    gcollfilt_bad_request([funcname, 'not implemented'])
  end
  
  def gcollfilt_bad_request(keywords)
    getcoll
    assert_bad_request(keywords)
  end
  
  # helper func for gcollfilt and gcollfilt_not_implemented
  def getcoll
    partab = []
    fpar = @filtstr ? "$filter=#{URI.encode_www_form_component @filtstr}" : nil
    partab << fpar if fpar
    apar =  @adpar ? "$orderby=#{URI.encode_www_form_component @adpar}" : nil
    partab << apar if apar
    tpar =  @toppar ? "$top=#{URI.encode_www_form_component @toppar}" : nil
    partab << tpar if tpar
    # we expect at least one entry in partab
    get "#{@collpath}?#{partab.join('&')}"
  end
  
  # reusable check on last response + json-parse
  # it + check

  def assert_last_resp_entity_parseable_ok(type_name)
    assert last_response.ok?

    @jresd = OData::XJSON.parse_one last_response.body

    assert_equal(type_name, @jresd[:__metadata][:type])

  end

  # same as previous but keep values unchanged (not casted)
  def assert_last_resp_enty_ok_no_cast(type_name=nil)
    assert last_response.ok?

    @jrawd = OData::XJSON.parse_one_nocast last_response.body

    # check type name on request only
    assert_equal(type_name, @jrawd[:__metadata][:type]) if type_name

  end
  # same as previous but expect an empty entity
  def assert_last_resp_empty_entity_ok(type_name=nil)
    assert last_response.ok?

    @jrawd = JSON.parse last_response.body

    # check emptyness
    assert_equal({}, @jrawd['d'])

  end
  def assert_last_resp_empty_coll_parseable_ok( version = 2)
    assert_last_resp_coll_parseable(nil, 0, version )
  end


  def assert_last_resp_coll_parseable(type_name, exp_size, version = 2, &proc)
    @resh = nil

    if (version == 1)
      @jresd = OData::XJSON.v1_parse_coll last_response.body
      @jresd_coll = @jresd
    else
      @jresd = OData::XJSON.parse_coll last_response.body
      @jresd_coll = @jresd['results']
    end

    # just check that we end up with a collection
    # and check the size of it
    assert_equal(exp_size, @jresd_coll.size)

    # check the returned type
    if exp_size > 0
      assert_equal(type_name, @jresd_coll.first[:__metadata][:type])
      assert_equal(type_name, @jresd_coll.last[:__metadata][:type])
    end
  end

  def assert_last_resp_coll_parseable_nocast(type_name, exp_size, version = 2, &proc)
    @resh = nil

    if (version == 1)
      @jrawd = OData::XJSON.v1_parse_coll_nocast last_response.body
      @jrawd_coll = @rawd
    else
      @jrawd = OData::XJSON.parse_coll_nocast last_response.body
      @jrawd_coll = @jrawd['results']
    end

    # just check that we end up with a collection
    # and check the size of it
    assert_equal(exp_size, @jrawd_coll.size)

    # check the returned type
    if exp_size > 0
      assert_equal(type_name, @jrawd_coll.first[:__metadata][:type])
      assert_equal(type_name, @jrawd_coll.last[:__metadata][:type])
    end
  end

  def assert_last_resp_links_parseable(type_name, exp_size, version=2)
    @resh = nil

    if (version == 1)
      @jresd = OData::XJSON.v1_parse_coll last_response.body
      @jresd_links = @jresd
    else
      @jresd = OData::XJSON.parse_coll last_response.body
      @jresd_links = @jresd['results']
    end


    # just check that we end up with a collection
    # and check the size of it
    assert_equal(exp_size, @jresd_links.size)

  end

  def assert_last_resp_onelink_parseable
    @jresd_links = OData::XJSON.parse_one last_response.body
    assert @jresd_links.has_key? :uri
  end

  # this is implicitely v2
  def assert_last_resp_coll_parseable_ok(type_name, exp_size, version=2)
    assert last_response.ok?
    assert_last_resp_coll_parseable(type_name, exp_size, version)
  end
  # this is implicitely v2
  def assert_last_resp_coll_parseable_nocast_ok(type_name, exp_size, version=2)
    assert last_response.ok?
    assert_last_resp_coll_parseable_nocast(type_name, exp_size, version)
  end
  # list of $links
  def assert_last_resp_links_parseable_ok(type_name, exp_size, version=2)
    assert last_response.ok?
    assert_last_resp_links_parseable(type_name, exp_size, version)
  end

  # single $links  for x_to_one navigation
  def assert_last_resp_onelink_parseable_ok
    assert last_response.ok?
    assert_last_resp_onelink_parseable
  end

  def assert_last_resp_coll_parseable_created(type_name, exp_size)
    assert last_response.created?
    assert_last_resp_coll_parseable(type_name, exp_size)
  end

  def assert_last_resp_coll_parseable_nocast_created(type_name, exp_size)
    assert last_response.created?
    assert_last_resp_coll_parseable_nocast(type_name, exp_size)
  end

  def assert_last_resp_coll_parseable_success(type_name, exp_size)
    assert last_response.successful?
    assert_last_resp_coll_parseable(type_name, exp_size)
  end



  # $expand checks
  def assert_deferred_attrib(jresd, attr)
    assert (enty_uri = jresd[:__metadata][:uri])
    assert_kind_of Hash, jresd[attr]
    assert_kind_of Hash, jresd[attr][:__deferred]
    assert_equal "#{enty_uri}/#{attr}",
                  jresd[attr][:__deferred][:uri]
  end

  def assert_not_expanded_entity(expected, jresd, attr)

    assert_sequel_values_equal(expected, jresd)
    assert_deferred_attrib(jresd, attr)

  end

  def assert_metadata_uri(entity, jresd, &proc)
      assert_equal "#{uribase(last_request)}/#{proc.call(entity)}",
                  jresd[:__metadata][:uri]
  end

  def assert_expanded_attrib(expanded, jresd_attr, proc)
    assert_kind_of Hash, jresd_attr
    assert_kind_of Hash, jresd_attr[:__metadata]

    assert_metadata_uri(expanded, jresd_attr, &proc)
                  
    assert_sequel_values_equal(expanded, jresd_attr)
  end

  def assert_expanded_attrib_mult(expcoll, jresd_attr_coll, proc)
    expcoll.each_with_index{|exp, idx|
      assert (jresd_attr = jresd_attr_coll[idx])
      assert_expanded_attrib(exp, jresd_attr, proc)
    }

  end

  # need to be called with a block that returns the Odata address (uri) of
  # the expected navigated attribute.
  def assert_expanded_entity(expected, jresd, attr, &proc)

    assert_sequel_values_equal(expected, jresd)

    expanded = expected.send(attr)
    assert_expanded_attrib(expanded, jresd[attr], proc)
  end

  # need to be called with a block that returns the Odata address (uri) of
  # the expected navigated attribute.
  def assert_expanded_entity_mult(expected, jresd, attr, &proc)

    assert_sequel_values_equal(expected, jresd)

    expanded = expected.send(attr)
    assert_kind_of Array, jresd[attr][:results]  # Warning, v2 only

    assert_expanded_attrib_mult(expanded, jresd[attr][:results], proc)  # Warning, v2 only
  end

end
