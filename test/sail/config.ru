###########
# config.ru
#

# $LOAD_PATH << '../../lib/'

require_relative './dbmodel_run.rb'

# docu for Builder --> https://www.tuicool.com/articles/yyIFri

final_odata_app = Rack::OData::Builder.new do
  run ODataSailApp.new
end

Rack::Handler::Thin.run final_odata_app, Port: 9595
#Rack::Handler::WEBrick.run final_odata_app, Port: 9595
