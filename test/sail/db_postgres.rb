#!/usr/bin/env ruby

require_relative '../../lib/safrano.rb'
require 'sequel'
require_relative '../test_helper.rb'

refresh_postgresql_testdb('sail')

DB[:sail] = Sequel::Model.db = Sequel.postgres('ci_test',
                                                host: 'postgres',
                                                user: 'dm',
                                                password: 'password')

at_exit { Sequel::Model.db.disconnect if Sequel::Model.db }
