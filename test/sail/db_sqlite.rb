#!/usr/bin/env ruby

require_relative '../../lib/safrano.rb'
require 'sequel'
require_relative '../test_helper.rb'

refresh_sqlite_testdb('sail')

dbrname = File.join(__dir__, 'sail_test.db3')
DB[:sail] = Sequel::Model.db = Sequel.sqlite(dbrname)

at_exit { Sequel::Model.db.disconnect if Sequel::Model.db }
