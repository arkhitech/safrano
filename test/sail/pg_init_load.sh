#!/bin/sh

export PGPASSWORD=password
export POSTGRES_USER=dm

dropdb -h "postgres" -U "$POSTGRES_USER" ci_test --if-exists
createdb -h "postgres" -U "$POSTGRES_USER" -l 'C.UTF-8' -T template0 ci_test
psql -q -h "postgres" -U "$POSTGRES_USER" ci_test < sail_pgdump_v0.sql > pg_init_load.log


