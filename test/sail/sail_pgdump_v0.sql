--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Raspbian 11.5-1+deb10u1)
-- Dumped by pg_dump version 11.5 (Raspbian 11.5-1+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: BoatClass; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."BoatClass" (
    id integer NOT NULL,
    name text
);


ALTER TABLE public."BoatClass" OWNER TO dm;

--
-- Name: BoatClass_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public."BoatClass_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."BoatClass_id_seq" OWNER TO dm;

--
-- Name: BoatClass_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public."BoatClass_id_seq" OWNED BY public."BoatClass".id;


--
-- Name: Crew; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."Crew" (
    id integer NOT NULL,
    type integer
);


ALTER TABLE public."Crew" OWNER TO dm;

--
-- Name: CrewMember; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."CrewMember" (
    crew_id integer DEFAULT 1 NOT NULL,
    person_id integer DEFAULT 1 NOT NULL
);


ALTER TABLE public."CrewMember" OWNER TO dm;

--
-- Name: CrewType; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."CrewType" (
    id integer NOT NULL,
    size integer,
    description text
);


ALTER TABLE public."CrewType" OWNER TO dm;

--
-- Name: Crew_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public."Crew_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Crew_id_seq" OWNER TO dm;

--
-- Name: Crew_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public."Crew_id_seq" OWNED BY public."Crew".id;


--
-- Name: Edition; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."Edition" (
    race_id integer NOT NULL,
    num integer NOT NULL,
    year integer NOT NULL,
    start timestamp,
    organizer text
);


ALTER TABLE public."Edition" OWNER TO dm;

--
-- Name: Issue; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."Issue" (
    race_id integer NOT NULL,
    race_num integer NOT NULL,
    boat_class integer NOT NULL,
    participants integer,
    abandonments integer
);


ALTER TABLE public."Issue" OWNER TO dm;

--
-- Name: PeriodicityType; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."PeriodicityType" (
    id integer NOT NULL,
    description text,
    period integer
);


ALTER TABLE public."PeriodicityType" OWNER TO dm;

--
-- Name: PeriodicityType_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public."PeriodicityType_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PeriodicityType_id_seq" OWNER TO dm;

--
-- Name: PeriodicityType_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public."PeriodicityType_id_seq" OWNED BY public."PeriodicityType".id;


--
-- Name: Person; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."Person" (
    id integer NOT NULL,
    first_name text,
    last_name text
);


ALTER TABLE public."Person" OWNER TO dm;

--
-- Name: Person_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public."Person_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Person_id_seq" OWNER TO dm;

--
-- Name: Person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public."Person_id_seq" OWNED BY public."Person".id;


--
-- Name: Race; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."Race" (
    id integer NOT NULL,
    name text,
    type integer,
    periodicity integer,
    crew_type integer
);


ALTER TABLE public."Race" OWNER TO dm;

--
-- Name: RaceType; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."RaceType" (
    id integer NOT NULL,
    description text
);


ALTER TABLE public."RaceType" OWNER TO dm;

--
-- Name: RaceType_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public."RaceType_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RaceType_id_seq" OWNER TO dm;

--
-- Name: RaceType_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public."RaceType_id_seq" OWNED BY public."RaceType".id;


--
-- Name: Race_id_seq; Type: SEQUENCE; Schema: public; Owner: dm
--

CREATE SEQUENCE public."Race_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Race_id_seq" OWNER TO dm;

--
-- Name: Race_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dm
--

ALTER SEQUENCE public."Race_id_seq" OWNED BY public."Race".id;


--
-- Name: Ranking; Type: TABLE; Schema: public; Owner: dm
--

CREATE TABLE public."Ranking" (
    race_id integer NOT NULL,
    race_num integer NOT NULL,
    boat_class integer NOT NULL,
    rank integer NOT NULL,
    exaequo integer NOT NULL,
    crew integer NOT NULL,
    arrival timestamp
);


ALTER TABLE public."Ranking" OWNER TO dm;

--
-- Name: BoatClass id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."BoatClass" ALTER COLUMN id SET DEFAULT nextval('public."BoatClass_id_seq"'::regclass);


--
-- Name: Crew id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Crew" ALTER COLUMN id SET DEFAULT nextval('public."Crew_id_seq"'::regclass);


--
-- Name: PeriodicityType id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."PeriodicityType" ALTER COLUMN id SET DEFAULT nextval('public."PeriodicityType_id_seq"'::regclass);


--
-- Name: Person id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Person" ALTER COLUMN id SET DEFAULT nextval('public."Person_id_seq"'::regclass);


--
-- Name: Race id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Race" ALTER COLUMN id SET DEFAULT nextval('public."Race_id_seq"'::regclass);


--
-- Name: RaceType id; Type: DEFAULT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."RaceType" ALTER COLUMN id SET DEFAULT nextval('public."RaceType_id_seq"'::regclass);


--
-- Data for Name: BoatClass; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."BoatClass" (id, name) FROM stdin;
1	Ultime 
2	IMOCA
3	Figaro Bénéteau 1
4	Figaro Bénéteau 2
5	Figaro Bénéteau 3
6	Class40
7	Multi50
8	  __test_trim__
\.


--
-- Data for Name: Crew; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."Crew" (id, type) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
13	1
14	1
15	1
16	1
17	1
18	1
19	1
20	1
21	1
22	1
23	1
24	1
25	1
26	1
27	1
28	1
29	1
30	1
31	1
32	1
33	1
34	2
35	2
36	2
37	1
38	2
39	2
40	1
41	1
42	2
43	1
44	1
45	1
46	1
47	1
\.


--
-- Data for Name: CrewMember; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."CrewMember" (crew_id, person_id) FROM stdin;
1	1
2	2
3	3
4	4
5	5
6	6
7	7
8	8
9	9
10	10
11	11
12	12
13	13
14	14
15	15
20	20
21	21
22	22
23	23
24	24
25	25
26	26
27	27
28	28
29	29
30	30
31	31
32	32
33	33
16	16
34	2
34	14
35	26
35	27
36	10
36	12
37	34
38	34
38	5
39	10
39	11
40	35
41	36
17	17
18	18
19	19
42	37
42	38
43	39
44	40
45	41
46	42
47	43
\.


--
-- Data for Name: CrewType; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."CrewType" (id, size, description) FROM stdin;
1	1	Single 
2	2	Two handed
5	10	Up to 10
10	\N	Free size
\.


--
-- Data for Name: Edition; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."Edition" (race_id, num, year, start, organizer) FROM stdin;
3	11	2018	2018-11-04T14:00:00+01:00	\N
1	8	2016	2016-11-06T13:02:00+01:00	Saem Vendée
5	11	2005	2005-08-05T12:00:00+01:00	\N
2	3	2014	2014-12-31T13:00:00+01:00	\N
2	2	2010	2010-12-31T12:00:00+01:00	\N
4	13	2017	2017-11-05T13:35:00+01:00	\N
1	7	2012	2012-11-10T13:02:00+01:00	Saem Vendée
1	6	2008	2008-11-09T13:02:00+01:00	Saem Vendée
1	5	2004	2004-11-07T12:01:05+01:00	Saem Vendée
3	10	2014	2014-11-02T14:00:00+01:00	\N
2	1	2007	2007-11-11T12:00:00+01:00	\N
1	4	2000	2000-11-09T13:00:00+01:00	Sail Com Philippe Jeantot
\.


--
-- Data for Name: Issue; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."Issue" (race_id, race_num, boat_class, participants, abandonments) FROM stdin;
1	8	2	\N	\N
1	7	2	\N	\N
1	6	2	\N	\N
1	5	2	\N	\N
5	11	4	\N	\N
2	2	2	\N	\N
2	3	2	\N	\N
3	10	1	\N	\N
3	10	2	\N	\N
3	11	1	\N	\N
3	11	2	\N	\N
4	13	1	\N	\N
4	13	2	\N	\N
3	10	6	\N	\N
3	10	7	\N	\N
3	11	6	\N	\N
3	11	7	\N	\N
\.


--
-- Data for Name: PeriodicityType; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."PeriodicityType" (id, description, period) FROM stdin;
1	Yearly	1
2	every two years	2
3	every four years	4
99		\N
\.


--
-- Data for Name: Person; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."Person" (id, first_name, last_name) FROM stdin;
1	Francis	Joyon
2	Jean 	Le Cam
3	Romain	Pilliard
4	François	Gabard
5	Thomas	Coville
6	Alex	Thomson
7	Paul	Meilhat
8	Armel	Le Cléac’h
9	Jérémie	Beyou
10	Jean-Pierre	Dick
11	Yann	Elies
12	Loïck	Peyron
13	Damian	Foxall
14	Bernard 	Stamm
15	Vincent	Riou
16	Michel	Desjoyeaux
17	Mike	Golding
18	Bertrand	De Broc
19	Tanguy	De Lamotte
20	Samantha	Davies
21	Louis	Burton
22	Arnaud	Boissières
23	Kito	de Pavant
24	Conrad	Colman
25	Nándor	Fa
26	Guillermo	Altadill
27	José	Muñoz
28	Marc	Guillemot
29	Isabelle	Joschke
30	Sébastien	Josse
31	Boris	Herrmann
32	Raphaël	Dinelli
33	Gildas	Morvan
34	Jean-Luc	Nelias
35	Brian	Thompson
36	Dominique	Wavre
37	Iker	Martinez
38	Xabi	Fernandez
39	Yann	Guichard
40	Lionel 	Lemonchois
41	Sidney	Gavignet
42	Armel	Tripon
43	Alessandro	Di Benedetto
\.


--
-- Data for Name: Race; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."Race" (id, name, type, periodicity, crew_type) FROM stdin;
1	Vendée Globe	1	3	1
2	Barcelona World Race	1	3	2
3	Route du Rhum	2	3	1
4	Transat Jacques Vabres	2	2	2
5	Solitaire du Figaro	3	1	1
6	Brand new Race without Edition yet (testing)	1	2	1
7	The Race	1	99	10
8	Oryx Quest	1	99	10
9	Transat AG2R	4	2	2
10	The Ocean Race	5	3	5
11	The Fastnet Race	3	2	1
12	Test-Race record for test update	1	3	1
13	Test-Race 'record' with quotes	1	3	1
14	Test-Race record for changeset	1	1	1
15	Test-Race record for changeset	1	1	1
16	Armel Le Cléac’hs race for testing	1	1	1
\.


--
-- Data for Name: RaceType; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."RaceType" (id, description) FROM stdin;
1	Non stop round-the-world
2	Trans-Atlantic Non Stop
3	Regata
4	Trans-Atlantic One Stop
5	Multi leg round-the-world
\.


--
-- Data for Name: Ranking; Type: TABLE DATA; Schema: public; Owner: dm
--

COPY public."Ranking" (race_id, race_num, boat_class, rank, exaequo, crew, arrival) FROM stdin;
3	11	1	1	0	1	2018-11-12T04:21:47+01:00
3	11	1	2	0	4	2018-11-12T04:28:55+01:00
3	11	1	3	0	5	2018-11-20T21:41:36+01:00
3	11	2	1	0	7	2018-11-17T01:23:18+01:00
1	8	2	1	0	8	2017-01-19T16:37:46+01:00
1	8	2	2	0	6	2017-01-20T08:37:15+01:00
1	8	2	3	0	9	2017-01-23T19:40:40+01:00
1	8	2	4	0	10	2017-01-25T14:47:45+01:00
1	8	2	5	0	11	2017-01-25T16:13:09+01:00
1	8	2	6	0	2	2017-01-25T17:43:54+01:00
3	11	2	2	0	11	2018-11-17T03:38:30+01:00
3	11	2	3	0	6	2018-11-17T13:10:58+01:00
5	11	4	1	0	9	1900-01-01T01:01:01+01:00
5	11	4	2	0	16	1900-01-01T01:01:01+01:00
5	11	4	3	0	23	1900-01-01T01:01:01+01:00
5	11	4	4	0	33	1900-01-01T01:01:01+01:00
5	11	4	5	0	11	1900-01-01T01:01:01+01:00
2	3	2	1	0	34	2015-03-25T18:50:25+01:00
2	3	2	2	0	35	2015-03-31T00:47:00+01:00
2	2	2	1	0	36	2011-04-04T10:20:36+01:00
4	13	1	1	0	38	2017-11-13T11:42:27+01:00
4	13	2	1	0	39	2017-11-18T21:11:46+01:00
1	6	2	1	0	16	2009-02-01T16:11:08+01:00
1	6	2	2	0	8	2009-02-06T22:41:35+01:00
1	6	2	3	1	28	2009-02-12T16:21:36+01:00
1	6	2	3	2	15	1900-01-01T01:01:01+01:00
1	6	2	4	0	20	2009-02-12T17:41:01+01:00
1	7	2	5	0	2	2013-02-06T13:14:58+01:00
1	7	2	4	0	10	2013-02-04T16:05:40+01:00
1	7	2	3	0	6	2013-01-30T08:25:43+01:00
1	7	2	2	0	8	2013-01-27T18:35:52+01:00
1	7	2	1	0	4	2013-01-27T15:18:40+01:00
1	6	2	5	0	28	2009-02-16T09:31:55+01:00
1	5	2	1	0	15	2005-02-02T22:49:00+01:00
1	5	2	2	0	2	2005-02-03T05:21:13+01:00
1	5	2	3	0	17	2005-02-04T03:16:18+01:00
1	5	2	4	0	41	2005-02-08T05:14:25+01:00
1	5	2	5	0	30	2005-02-08T12:03:15+01:00
1	5	2	6	0	10	2005-02-13T15:50:43+01:00
2	2	2	2	0	42	2011-04-05T09:17:35+01:00
3	10	1	1	0	12	2014-11-10T05:08:32+01:00
3	10	1	2	0	43	2014-11-10T19:18:46+01:00
3	10	1	3	0	30	2014-11-11T04:47:09+01:00
3	10	1	4	0	44	2014-11-11T07:44:50+01:00
3	10	1	5	0	45	2014-11-11T09:15:24+01:00
3	10	1	6	0	1	2014-11-11T18:42:04+01:00
3	10	2	1	0	4	2014-11-14T18:38:55+01:00
3	10	2	2	0	9	2014-11-15T02:11:18+01:00
3	10	2	3	0	28	2014-11-15T15:59:20+01:00
3	10	2	4	0	46	2014-11-16T04:04:04+01:00
3	10	2	5	0	21	2014-11-16T15:33:44+01:00
3	10	2	6	0	47	2014-11-19T01:04:30+01:00
\.


--
-- Name: BoatClass_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dm
--

SELECT pg_catalog.setval('public."BoatClass_id_seq"', 8, true);


--
-- Name: Crew_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dm
--

SELECT pg_catalog.setval('public."Crew_id_seq"', 1, false);


--
-- Name: PeriodicityType_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dm
--

SELECT pg_catalog.setval('public."PeriodicityType_id_seq"', 1, false);


--
-- Name: Person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dm
--

SELECT pg_catalog.setval('public."Person_id_seq"', 43, true);


--
-- Name: RaceType_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dm
--

SELECT pg_catalog.setval('public."RaceType_id_seq"', 5, true);


--
-- Name: Race_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dm
--

SELECT pg_catalog.setval('public."Race_id_seq"', 16, true);


--
-- Name: BoatClass BoatClass_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."BoatClass"
    ADD CONSTRAINT "BoatClass_pkey" PRIMARY KEY (id);


--
-- Name: CrewMember CrewMember_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."CrewMember"
    ADD CONSTRAINT "CrewMember_pkey" PRIMARY KEY (crew_id, person_id);


--
-- Name: CrewType CrewType_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."CrewType"
    ADD CONSTRAINT "CrewType_pkey" PRIMARY KEY (id);


--
-- Name: Crew Crew_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Crew"
    ADD CONSTRAINT "Crew_pkey" PRIMARY KEY (id);


--
-- Name: Edition Edition_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Edition"
    ADD CONSTRAINT "Edition_pkey" PRIMARY KEY (race_id, num);


--
-- Name: Issue Issue_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT "Issue_pkey" PRIMARY KEY (race_id, race_num, boat_class);


--
-- Name: PeriodicityType PeriodicityType_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."PeriodicityType"
    ADD CONSTRAINT "PeriodicityType_pkey" PRIMARY KEY (id);


--
-- Name: Person Person_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Person"
    ADD CONSTRAINT "Person_pkey" PRIMARY KEY (id);


--
-- Name: RaceType RaceType_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."RaceType"
    ADD CONSTRAINT "RaceType_pkey" PRIMARY KEY (id);


--
-- Name: Race Race_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Race"
    ADD CONSTRAINT "Race_pkey" PRIMARY KEY (id);


--
-- Name: Ranking Ranking_pkey; Type: CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Ranking"
    ADD CONSTRAINT "Ranking_pkey" PRIMARY KEY (race_id, race_num, boat_class, rank, exaequo);


--
-- Name: idx_racetype; Type: INDEX; Schema: public; Owner: dm
--

CREATE INDEX idx_racetype ON public."Race" USING btree (type);


--
-- Name: CrewMember CrewMember_crew_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."CrewMember"
    ADD CONSTRAINT "CrewMember_crew_id_fkey" FOREIGN KEY (crew_id) REFERENCES public."Crew"(id);


--
-- Name: CrewMember CrewMember_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."CrewMember"
    ADD CONSTRAINT "CrewMember_person_id_fkey" FOREIGN KEY (person_id) REFERENCES public."Person"(id);


--
-- Name: Crew Crew_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Crew"
    ADD CONSTRAINT "Crew_type_fkey" FOREIGN KEY (type) REFERENCES public."CrewType"(id);


--
-- Name: Edition Edition_race_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Edition"
    ADD CONSTRAINT "Edition_race_id_fkey" FOREIGN KEY (race_id) REFERENCES public."Race"(id);


--
-- Name: Issue Issue_boat_class_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT "Issue_boat_class_fkey" FOREIGN KEY (boat_class) REFERENCES public."BoatClass"(id);


--
-- Name: Issue Issue_race_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT "Issue_race_id_fkey" FOREIGN KEY (race_id, race_num) REFERENCES public."Edition"(race_id, num);


--
-- Name: Race Race_crew_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Race"
    ADD CONSTRAINT "Race_crew_type_fkey" FOREIGN KEY (crew_type) REFERENCES public."CrewType"(id);


--
-- Name: Race Race_periodicity_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Race"
    ADD CONSTRAINT "Race_periodicity_fkey" FOREIGN KEY (periodicity) REFERENCES public."PeriodicityType"(id);


--
-- Name: Race Race_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Race"
    ADD CONSTRAINT "Race_type_fkey" FOREIGN KEY (type) REFERENCES public."RaceType"(id);


--
-- Name: Ranking Ranking_boat_class_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Ranking"
    ADD CONSTRAINT "Ranking_boat_class_fkey" FOREIGN KEY (boat_class) REFERENCES public."BoatClass"(id);


--
-- Name: Ranking Ranking_race_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dm
--

ALTER TABLE ONLY public."Ranking"
    ADD CONSTRAINT "Ranking_race_id_fkey" FOREIGN KEY (race_id, race_num, boat_class) REFERENCES public."Issue"(race_id, race_num, boat_class);


--
-- PostgreSQL database dump complete
--

