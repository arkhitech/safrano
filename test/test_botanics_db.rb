#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require 'test/unit'
require 'rack/test'
require 'json'
require 'rexml/document'
require 'pp'
require 'uri'
require 'set'

require_relative 'rack_test_monkey.rb'
require_relative '../lib/safrano.rb'
require_relative './botanics/dbmodel_test.rb' # for testdata

# base class for tests that require the botanics DB to be loaded,
# but are not Rack-Tests
class BotanicsDBTestII <  Test::Unit::TestCase
  def self.startup
    Sequel::Model.db = DB[:botanics]
  end
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('BotanicsDBTestII')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end
end

class BotanicsDBTest < ODataModelTestCase
  # Warning: dont forget to call super if you redefine startup in subclasses !
  def self.startup
    Sequel::Model.db = DB[:botanics]
  end
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('BotanicsDBTest')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end

  # we will use V2 for testing
  def setup
    header 'MaxDataServiceVersion', '2.0'
  end

  def app
    ODataBotanicsApp.new
  end

end

