#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require 'test/unit'
require 'rack/test'
require 'json'

require_relative './chinook/model.rb' # for testdata
require_relative './odata_model_asserts.rb'


# base class for tests that require the chinook DB to be loaded,
# but are not Rack-Tests
class ChinookDBTestII <  Test::Unit::TestCase
  include TM_Model_Compare_Helper
  def self.startup
    Sequel::Model.db = DB[:chinook]
  end
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('ChinookDBTestII')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end
end

class ChinookDBTest < ODataModelTestCase

  def self.startup
    Sequel::Model.db = DB[:chinook]
  end
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new('ChinookDBTest')
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end

  # we will use V2 for testing
  def setup
    header 'MaxDataServiceVersion', '2.0'
  end

  def app
    ODataChinookApp.new
  end

end

