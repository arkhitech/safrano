
require 'test/unit'

class Class
  def descendants
    ObjectSpace.each_object(::Class).select {|klass| klass < self }
  end
end

ENV['RACK_ENV'] = 'test'


def link_db(type)
  case type.to_sym
    when :sqlite, :postgres
      Dir.chdir('test') do
        %W(sail botanics chinook).each{|testdb|
          Dir.chdir(testdb) do FileUtils.ln_sf("db_#{type}.rb", 'db.rb') end
          puts "Linked db_#{type}.rb to db.rb in test-dir #{testdb}"
        }
      end
    else
      raise 'Unsupported DB type'
  end

end

# refresh/rebuild postgresql test-db's
def refresh_postgresql_testdb(name)
  Dir.chdir("test/#{name}") do
    print "Refresh Postgresql #{name} test-DB..."
    if system('./pg_init_load.sh')
      puts 'done'
    else
      raise "'could not refresh postgresql #{name} test-db !'"
    end
  end
  refresh_media_testdata(name)
end

# refresh/rebuild sqlite test-db's
def refresh_sqlite_testdb(name)

  dbname = case name
    when 'chinook'
      'chinookn3'
    else
      name
    end
  # create the botanics and sail db3 from sqlite dump file because its more flexibel
  # when schema changes are needed
  # TODO: generalise this to the other test-db's

  Dir.chdir("test/#{name}") do
    print "Refresh Sqlite #{name} test-DB..."
    begin
      if (name == 'botanics')
        FileUtils.rm 'botanics_v0.db3' if File.exists? 'botanics_v0.db3'
        system('sqlite3 botanics_v0.db3 < botanics_v0.db3.sql ')
      end
      if (name == 'sail')
        FileUtils.rm 'sail_v0.db3' if File.exists? 'sail_v0.db3'
        system('sqlite3 sail_v0.db3 < sail_db3_schema.sql  && sqlite3 sail_v0.db3 < sail_db3_data.sql')
      end
      FileUtils.cp("#{dbname}_v0.db3", "#{dbname}_test.db3")
      puts 'done'
    rescue
      raise "Could not refresh sqlite #{name} db !"
    end
  end

  refresh_media_testdata(name)

end

def   refresh_media_testdata(name)
  begin
    case name 
      when 'botanics'
        print "Refreshing  #{name} media test-data..."
        Dir.chdir("test/#{name}") do
          FileUtils.rm_rf 'media'
          FileUtils.cp_r 'media_v0', 'media'
        end
        puts 'done'
      else
    end
  rescue 
    raise "Could not refresh  #{name} media test-data !"
  end
end

class TCWithoutDB < Test::Unit::TestCase

  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end

end
